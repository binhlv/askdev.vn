<?php get_header();
	$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
    $vbegy_sidebar = rwmb_meta('vbegy_sidebar','select',$post->ID);
    $vbegy_google = rwmb_meta('vbegy_google',"textarea",$post->ID);
    $video_id = rwmb_meta('vbegy_video_post_id',"select",$post->ID);
    $video_type = rwmb_meta('vbegy_video_post_type',"text",$post->ID);
    $vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
    if ($video_type == 'youtube') {
    	$type = "http://www.youtube.com/embed/".$video_id;
    }else if ($video_type == 'vimeo') {
    	$type = "http://player.vimeo.com/video/".$video_id;
    }else if ($video_type == 'daily') {
    	$type = "http://www.dailymotion.com/swf/video/".$video_id;
    }
	if ( have_posts() ) : while ( have_posts() ) : the_post();?>
		<article <?php post_class('post single-post');?> id="post-<?php the_ID();?>">
			<div class="post-inner">
				<div class="post-img<?php if ($vbegy_what_post == "image" && !has_post_thumbnail()) {echo " post-img-0";}else if ($vbegy_what_post == "video") {echo " video_embed";}if ($vbegy_sidebar == "full") {echo " post-img-12";}else {echo " post-img-9";}?>">
					<?php include (get_template_directory() . '/includes/head.php');?>
				</div>
	        	<h2 class="post-title"><?php the_title()?></h2>
				<?php $posts_meta = vpanel_options("post_meta");
				if ($posts_meta == 1) {?>
					<div class="post-meta">
					    <span class="meta-author"><i class="icon-user"></i><?php the_author_posts_link();?></span>
					    <span class="meta-date"><i class="icon-time"></i><?php the_time('F j , Y');?></span>
					    <span class="meta-comment"><i class="icon-comments-alt"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
					</div>
				<?php }?>
				<div class="post-content">
					<?php the_content();?>
				</div>
			</div><!-- End post-inner -->
		</article><!-- End article.post -->
		<?php $post_comments = vpanel_options("post_comments");
		if ($post_comments == 1) {
			comments_template();
		}
	endwhile; endif;
get_footer();?>