<?php if (is_home()) {
    $home_page_sidebar = vpanel_options("sidebar_home");
    if ($home_page_sidebar == "default") {
        dynamic_sidebar('sidebar_default');
    }else {
        dynamic_sidebar(sanitize_title($home_page_sidebar));
    }
}else if (is_author()) {
	$author_page_sidebar = vpanel_options("author_sidebar");
	if ($author_page_sidebar == "default") {
	    dynamic_sidebar('sidebar_default');
	}else {
	    dynamic_sidebar(sanitize_title($author_page_sidebar));
	}
}else if (is_category()) {
	$category_id = get_query_var('cat');
	$cat_page_sidebar = vpanel_options("cat_sidebar".$category_id);
	if ($cat_page_sidebar == "default") {
	    dynamic_sidebar('sidebar_default');
	}else {
	    dynamic_sidebar(sanitize_title($cat_page_sidebar));
	}
}else if (is_tax("question-category")) {
	$tax_id = get_term_by('slug',get_query_var('term'),"question-category");
	$tax_id = $tax_id->term_id;
	$cat_page_sidebar = vpanel_options("cat_sidebar".$tax_id);
	if ($cat_page_sidebar == "default") {
	    dynamic_sidebar('sidebar_default');
	}else {
	    dynamic_sidebar(sanitize_title($cat_page_sidebar));
	}
}else if(is_single() or is_page()) {
    $vbegy_what_sidebar = rwmb_meta('vbegy_what_sidebar','select',$post->ID);
    if (isset($vbegy_what_sidebar) && $vbegy_what_sidebar != "default" && $vbegy_what_sidebar != "") {
	    dynamic_sidebar(sanitize_title($vbegy_what_sidebar));
    }else {
        dynamic_sidebar('sidebar_default');
    }
}else  {
    $else_sidebar = vpanel_options("else_sidebar");
    if ($else_sidebar == "default") {
        dynamic_sidebar('sidebar_default');
    }else {
        dynamic_sidebar(sanitize_title($else_sidebar));
    }
}
?>