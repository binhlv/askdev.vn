<?php /* Template Name: Questions bump */
get_header();
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array("paged" => $paged,"post_type" => "question","posts_per_page" => get_option("posts_per_page"),"orderby" => "meta_value_num","meta_key" => "question_points","meta_query" => array(array('type' => 'numeric',"key" => "question_points","value" => 0,"compare" => ">=")));
	function ask_filter_where($where = '') {
		$where .= " AND comment_count = 0";
		return $where;
	}
	add_filter('posts_where', 'ask_filter_where');
	query_posts($args);
	global $vbegy_sidebar;
	$vbegy_sidebar = vpanel_options("sidebar_layout");
	get_template_part("loop-question");
	vpanel_pagination();
	remove_filter( 'posts_where', 'filter_where' );
get_footer();?>