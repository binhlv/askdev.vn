<?php /* Template name: User Posts */
global $user_ID;
if(empty($_GET['u'])){
	wp_redirect(get_bloginfo("url"));
}
$user_login = get_userdata($_GET['u']);
if(empty($user_login)){
	wp_redirect(get_bloginfo("url"));
}
$owner = false;
if($user_ID == $user_login->ID){
	$owner = true;
}
get_header();
	include (get_template_directory() . '/includes/author-head.php');
	$paged = intval(get_query_var('paged'));
	wp_reset_query();
	$args = array('post_type' => 'post','paged' => $paged,'author' => $user_login->ID);
	query_posts($args);
	$blog_style = vpanel_options("home_display");
	$posts_meta = vpanel_options("post_meta");
	if (have_posts() ) : while (have_posts() ) : the_post();
	    $vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
	    $vbegy_sidebar = rwmb_meta('vbegy_sidebar','select',$post->ID);
	    $vbegy_google = rwmb_meta('vbegy_google',"textarea",$post->ID);
	    $video_id = rwmb_meta('vbegy_video_post_id',"select",$post->ID);
	    $video_type = rwmb_meta('vbegy_video_post_type',"text",$post->ID);
	    $vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
	    if ($video_type == 'youtube') {
	    	$type = "http://www.youtube.com/embed/".$video_id;
	    }else if ($video_type == 'vimeo') {
	    	$type = "http://player.vimeo.com/video/".$video_id;
	    }else if ($video_type == 'daily') {
	    	$type = "http://www.dailymotion.com/swf/video/".$video_id;
	    }?>
		<article <?php post_class('post  clearfix '.($blog_style == "blog_2"?"blog_2":"").(is_sticky()?" sticky_post":""));?>>
			<div class="post-inner">
				<?php if ($blog_style != "blog_2") {?>
					<div class="post-img<?php if ($vbegy_what_post == "image" && !has_post_thumbnail()) {echo " post-img-0";}else if ($vbegy_what_post == "video") {echo " video_embed";}if ($vbegy_sidebar == "full") {echo " post-img-12";}else {echo " post-img-9";}?>">
						<?php if (has_post_thumbnail() && $vbegy_what_post == "image") {?><a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php }
							include (get_template_directory() . '/includes/head.php');?>
						<?php if (has_post_thumbnail()) {?></a><?php }?>
					</div>
		        <?php }?>
		        <h2 class="post-title">
		        	<?php if ($vbegy_what_post == "google") {?>
			        	<span class="post-type"><i class="icon-map-marker"></i></span>
		        	<?php }else if ($vbegy_what_post == "video") {?>
		        		<span class="post-type"><i class="icon-play-circle"></i></span>
		        	<?php }else if ($vbegy_what_post == "slideshow") {?>
		        		<span class="post-type"><i class="icon-film"></i></span>
		        	<?php }else {
		        		if (has_post_thumbnail()) {?>
		    	        	<span class="post-type"><i class="icon-picture"></i></span>
		        		<?php }else {?>
		    	        	<span class="post-type"><i class="icon-file-alt"></i></span>
		        		<?php }
		        	}?>
		        	<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title();?></a>
		        </h2>
		        <?php
		        if ($blog_style == "blog_2") {?>
			        <div class="post-img<?php if ($vbegy_what_post == "image" && !has_post_thumbnail()) {echo " post-img-0";}else if ($vbegy_what_post == "video") {echo " video_embed";}if ($vbegy_sidebar == "full") {echo " post-img-12";}else {echo " post-img-9";}?>">
			        	<?php if (has_post_thumbnail() && $vbegy_what_post == "image") {?><a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php }
			        		include (get_template_directory() . '/includes/head.php');?>
			        	<?php if (has_post_thumbnail()) {?></a><?php }?>
			        </div>
		        <?php }
		        if ($posts_meta == 1) {?>
		        	<div class="post-meta">
		        	    <span class="meta-author"><i class="icon-user"></i><?php the_author_posts_link();?></span>
		        	    <span class="meta-date"><i class="icon-time"></i><?php the_time('F j , Y');?></span>
		        	    <?php if (!is_page()) {?>
			        	    <span class="meta-categories"><i class="icon-suitcase"></i><?php the_category(' , ');?></span>
		        	    <?php }?>
		        	    <span class="meta-comment"><i class="icon-comments-alt"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
		        	</div>
		        <?php }?>
		        <div class="post-content">
		            <p><?php if ($blog_style == "blog_2") {excerpt(20);}else {excerpt(40);}?></p>
		            <a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark" class="post-read-more button color small"><?php _e("Continue reading","vbegy");?></a>
		        </div><!-- End post-content -->
		    </div><!-- End post-inner -->
		</article><!-- End article.post -->
	<?php endwhile;else :
		echo "<div class='page-content page-content-user'><p class='no-item'>".__("No posts yet .","vbegy")."</p></div>";
	endif;
	vpanel_pagination();
get_footer();?>