<?php /* Template Name: Recent question  */
get_header();
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array("paged" => $paged,"post_type" => "question","posts_per_page" => get_option("posts_per_page"));
	query_posts($args);
	global $vbegy_sidebar;
	$vbegy_sidebar = vpanel_options("sidebar_layout");
	get_template_part("loop-question");
	vpanel_pagination();
get_footer();?>