<?php
global $blog_style,$vbegy_sidebar,$authordata;
if (have_posts() ) : while (have_posts() ) : the_post();
	include ("question.php");
endwhile;else :?>
	<div class="error_404">
		<div>
			<h2><?php _e("404","vbegy")?></h2>
			<h3><?php _e("Page not Found","vbegy")?></h3>
		</div>
		<div class="clearfix"></div><br>
		<a href="<?php echo esc_url(home_url('/'));?>" class="button large color margin_0"><?php _e("Home Page","vbegy")?></a>
	</div>
<?php endif;?>