<?php
ob_start();
/* ask_question_shortcode */
add_shortcode('ask_question', 'ask_question_shortcode');
function ask_question_shortcode($atts, $content = null) {
	global $posted;
	$out = '';
	$ask_question_no_register = vpanel_options("ask_question_no_register");
	if (!is_user_logged_in() && $ask_question_no_register != 1) {
		$out .= '<div class="form-style form-style-3"><div class="note_error"><strong>'.__("You must login to ask question .","vbegy").'</strong></div>'.do_shortcode("[ask_login]").'<ul class="login-links login-links-r"><li><a href="#">'.__("Register","vbegy").'</a></li></ul></div>';
	}else {
		$question_points_active = vpanel_options("question_points_active");
		$question_points = vpanel_options("question_points");
		$points = get_user_meta(get_current_user_id(),"points",true);
		$attachment_question = vpanel_options("attachment_question");
		
		if ($question_points_active == 0 || $points >= $question_points) {
			$out .= '<div class="ask-question"><div class="form-style form-style-3 question-submit">
				<div class="ask_question">
					<div '.(!is_user_logged_in()?"class='if_no_login'":"").'>';?>
						<script type="text/javascript">
							jQuery(function () {
								var question_poll = jQuery(".question_poll:checked").length;
								if (question_poll == 1) {
									jQuery(".poll_options").slideDown(500);
								}else {
									jQuery(".poll_options").slideUp(500);
								}
								
								jQuery(".question_poll").click(function () {
									var question_poll_c = jQuery(".question_poll:checked").length;
									if (question_poll_c == 1) {
										jQuery(".poll_options").slideDown(500);
									}else {
										jQuery(".poll_options").slideUp(500);
									}
								});
			
								whatsubmit = false;
								jQuery(".add_qu_a").live("click" , function() {
									var question_poll_submit = jQuery(".question_poll:checked").length;
									jQuery("#title,#comment,#category").css("border","1px solid #CCC");
									var ask_submit = jQuery(".ask").length;
									if (jQuery("#title").val() == "" ) {
										jQuery("#title").css("border","1px solid red");
										jQuery(".new-question-form").find('.note_error').slideDown(300).html('<strong><?php _e("You must type in all required fields!","vbegy");?></strong>').delay(1000).slideUp(300);
										whatsubmit = false;
									}else if (question_poll_submit == 1 && ask_submit < 2) {
										jQuery(".new-question-form").find('.note_error').slideDown(300).html('<strong><?php _e("You must book at least two options to vote!","vbegy");?></strong>').delay(1000).slideUp(300);
										whatsubmit = false;
									}else if (question_poll_submit == 1 && ask_submit == 2 && jQuery(".ask").val() == "") {
										jQuery(".ask").css('border','1px solid red');
										whatsubmit = false;
										jQuery(".new-question-form").find('.note_error').slideDown(300).html('<strong><?php _e("You must book at least two options to vote!","vbegy");?></strong>').delay(1000).slideUp(300);
									}else if (jQuery("#title").val() == "" && jQuery("#category").val() == "-1") {
										jQuery(".new-question-form").find('.note_error').slideDown(300).html('<strong><?php _e("You must type in all required fields!","vbegy");?></strong>').delay(1000).slideUp(300);
										jQuery("#title,#comment,#category").css("border","1px solid red");
										whatsubmit = false;
									}else if (jQuery("#category").val() == "" || jQuery("#category").val() == "-1") {
										jQuery(".new-question-form").find('.note_error').slideDown(300).html('<strong><?php _e("Category must be selected!","vbegy");?></strong>').delay(1000).slideUp(300);
										jQuery("#category").css("border","1px solid red");
										whatsubmit = false;
									}else if (jQuery("#comment").val() == "" ) {
										jQuery(".new-question-form").find('.note_error').slideDown(300).html('<strong><?php _e("You must type in all required fields!","vbegy");?></strong>').delay(1000).slideUp(300);
										jQuery("#comment").css("border","1px solid red");
										whatsubmit = false;
									}else {
										whatsubmit = true;
									}
									return whatsubmit;
								});
							});
						</script><?php
						do_action('new_question');
						$rand_q = rand(1,1000);
						$out .= '
						<form class="new-question-form" method="post" enctype="multipart/form-data">
							<div class="note_error display"></div>
							<div class="form-inputs clearfix">';
								if (!is_user_logged_in() && $ask_question_no_register == 1) {
									$out .= '<p>
										<label for="question-username-'.$rand_q.'" class="required">'.__("Username","vbegy").'<span>*</span></label>
										<input name="username" id="question-username-'.$rand_q.'" class="question-username" type="text" value="'.(isset($posted['username'])?$posted['title']:'').'">
										<span class="form-description">'.__("Please type your username .","vbegy").'</span>
									</p>
									
									<p>
										<label for="question-email-'.$rand_q.'" class="required">'.__("E-Mail","vbegy").'<span>*</span></label>
										<input name="email" id="question-email-'.$rand_q.'" class="question-email" type="text" value="'.(isset($posted['email'])?$posted['title']:'').'">
										<span class="form-description">'.__("Please type your E-Mail .","vbegy").'</span>
									</p>';
								}
								$out .= '<p>
									<label for="question-title-'.$rand_q.'" class="required">'.__("Question Title","vbegy").'<span>*</span></label>
									<input name="title" id="question-title-'.$rand_q.'" class="question-title" type="text" value="'.(isset($posted['title'])?$posted['title']:'').'">
									<span class="form-description">'.__("Please choose an appropriate title for the question to answer it even easier .","vbegy").'</span>
								</p>
								
								<p>
									<label for="question_tags-'.$rand_q.'">'.__("Tags","vbegy").'</label>
									<input type="text" class="input question_tags" name="question_tags" id="question_tags-'.$rand_q.'" value="'.(isset($posted['question_tags'])?$posted['question_tags']:'').'" data-seperator=",">
									<span class="form-description">'.__("Please choose  suitable Keywords Ex : ","vbegy").'<span class="color">'.__("question , poll","vbegy").'</span> .</span>
								</p>
								
								<p>
									<label for="category-'.$rand_q.'" class="required">'.__("Category","vbegy").'<span>*</span></label>
									<span class="styled-select">'.
										wp_dropdown_categories(array("echo" => "0","show_option_none" => __("Select a Category","vbegy"),'taxonomy' => 'question-category', 'hide_empty' => 0,'depth' => 0,'id' => 'category-'.$rand_q.'','name' => 'category',  'hierarchical' => true,'selected' => (int)$posted['category']))
									.'</span>
									<span class="form-description">'.__("Please choose the appropriate section so easily search for your question .","vbegy").'</span>
								</p>
								
								<p class="question_poll_p">
									<label for="question_poll-'.$rand_q.'">'.__("Poll","vbegy").'</label>
									<input type="checkbox" id="question_poll-'.$rand_q.'" class="question_poll" value="1" name="question_poll" '.(isset($posted['question_poll']) && $posted['question_poll'] == 1?"checked='checked'":"").'>
									<span class="question_poll">'.__("This question is a poll ?","vbegy").'</span>
									<span class="poll-description">'.__("If you want to be doing a poll click here .","vbegy").'</span>
								</p>
								
								<div class="clearfix"></div>
								<div class="poll_options">
									<p class="form-submit add_poll">
										<button type="button" class="button color small submit add_poll_button"><i class="icon-plus"></i>'.__("Add Field","vbegy").'</button>
									</p>
									<ul class="question_poll_item">';
										if (isset($_POST['ask']) && is_array($_POST['ask'])) {
											foreach($_POST['ask'] as $ask) {
												if (stripslashes($ask['title']) != "") {
													$out .= '<li id="poll_li_'.(int)$ask['id'].'">
														<div class="poll-li">
															<p><input id="ask['.(int)$ask['id'].'][title]" class="ask" name="ask['.(int)$ask['id'].'][title]" value="'.stripslashes($ask['title']).'" type="text"></p>
															<input id="ask['.(int)$ask['id'].'][value]" name="ask['.(int)$ask['id'].'][value]" value="" type="hidden">
															<input id="ask['.(int)$ask['id'].'][id]" name="ask['.(int)$ask['id'].'][id]" value="'.(int)$ask['id'].'" type="hidden">
															<div class="del-poll-li"><i class="icon-remove"></i></div>
															<div class="move-poll-li"><i class="icon-fullscreen"></i></div>
														</div>
													</li>';
												}
											}
										}else {
											$out .= '<li id="poll_li_1">
												<div class="poll-li">
													<p><input id="ask[1][title]" class="ask" name="ask[1][title]" value="" type="text"></p>
													<input id="ask[1][value]" name="ask[1][value]" value="" type="hidden">
													<input id="ask[1][id]" name="ask[1][id]" value="1" type="hidden">
													<div class="del-poll-li"><i class="icon-remove"></i></div>
													<div class="move-poll-li"><i class="icon-fullscreen"></i></div>
												</div>
											</li>';
										}
									$out .= '</ul>
									<script> var nextli = '.(isset($_POST['ask']) && is_array($_POST['ask'])?count($_POST['ask'])+1:"2").';</script>
									<div class="clearfix"></div>
								</div>';
								
								if ($attachment_question == 1) {
									$out .= '<label for="attachment-'.$rand_q.'">'.__("Attachment","vbegy").'</label>
									<div class="fileinputs">
										<input type="file" class="file" name="attachment" id="attachment-'.$rand_q.'">
										<div class="fakefile">
											<button type="button" class="button small margin_0">'.__("Select file","vbegy").'</button>
											<span><i class="icon-arrow-up"></i>'.__("Browse","vbegy").'</span>
										</div>
									</div>';
								}
								
							$out .= '
							</div>
							<div>
								<p>
									<label for="question-details-'.$rand_q.'" class="required">'.__("Details","vbegy").'<span>*</span></label>
									<textarea name="comment" id="question-details-'.$rand_q.'" class="question-textarea" aria-required="true" cols="58" rows="8">'.(isset($posted['comment'])?$posted['comment']:"").'</textarea>
									<span class="form-description">'.__("Type the description thoroughly and in detail .","vbegy").'</span>
								</p>
							</div>
							
							<div class="form-inputs clearfix">';
								if (vpanel_options("video_desc_active") == 1) {
									$out .= '
									<p class="question_poll_p">
										<label for="video_description-'.$rand_q.'">'.__("Video description","vbegy").'</label>
										<input type="checkbox" id="video_description-'.$rand_q.'" class="video_description_input" name="video_description" value="1" '.(isset($posted['video_description']) && $posted['video_description'] == 1?"checked='checked'":"").'>
										<span class="question_poll">'.__("Do you need a video to description the problem better ?","vbegy").'</span>
									</p>
									
									<div class="video_description" '.(isset($posted['video_description']) && $posted['video_description'] == 1?"style='display:block;'":"").'>
										<p>
											<label for="video_type-'.$rand_q.'">'.__("Video type","vbegy").'</label>
											<span class="styled-select">
												<select id="video_type-'.$rand_q.'" name="video_type">
													<option value="youtube" '.(isset($posted['video_type']) && $posted['video_type'] == "youtube"?' selected="selected"':'').'>Youtube</option>
													<option value="vimeo" '.(isset($posted['video_type']) && $posted['video_type'] == "vimeo"?' selected="selected"':'').'>Vimeo</option>
													<option value="daily" '.(isset($posted['video_type']) && $posted['video_type'] == "daily"?' selected="selected"':'').'>Dialymotion</option>
												</select>
											</span>
											<span class="form-description">'.__("Choose from here the video type .","vbegy").'</span>
										</p>
										
										<p>
											<label for="video_id-'.$rand_q.'">'.__("Video ID","vbegy").'</label>
											<input name="video_id" id="video_id-'.$rand_q.'" class="video_id" type="text" value="'.(isset($posted['video_id'])?$posted['video_id']:'').'">
											<span class="form-description">'.__("Put here the video id : http://www.youtube.com/watch?v=sdUUx5FdySs EX : 'sdUUx5FdySs' .","vbegy").'</span>
										</p>
									</div>';
								}
							$out .= '
							<p class="question_poll_p">
								<label for="remember_answer-'.$rand_q.'">'.__("Notified","vbegy").'</label>
								<input type="checkbox" id="remember_answer-'.$rand_q.'" name="remember_answer" value="1" '.(isset($posted['remember_answer']) && $posted['remember_answer'] == 1?"checked='checked'":"").'>
								<span class="question_poll">'.__("Notified by e-mail at incoming answers .","vbegy").'</span>
							</p>';
							$the_captcha = vpanel_options("the_captcha");
							$captcha_style = vpanel_options("captcha_style");
							$captcha_question = vpanel_options("captcha_question");
							$captcha_answer = vpanel_options("captcha_answer");
							$show_captcha_answer = vpanel_options("show_captcha_answer");
							if ($the_captcha == 1) {
								if ($captcha_style == "question_answer") {
									$out .= "
									<p class='ask_captcha_p'>
										<label for='ask_captcha-'.$rand_q.'' class='required'>".__("Captcha","vbegy")."<span>*</span></label>
										<input size='10' id='ask_captcha-'.$rand_q.'' name='ask_captcha' class='ask_captcha' value='' type='text'>
										<span class='question_poll ask_captcha_span'>".$captcha_question.($show_captcha_answer == 1?" ( ".$captcha_answer." )":"")."</span>
									</p>";
								}else {
									$out .= "
									<p class='ask_captcha_p'>
										<label for='ask_captcha_".$rand_q."' class='required'>".__("Captcha","vbegy")."<span>*</span></label>
										<input size='10' id='ask_captcha_".$rand_q."' name='ask_captcha' class='ask_captcha' value='' type='text'><img class='ask_captcha_img' src='".get_template_directory_uri()."/captcha/create_image.php' alt='".__("Captcha","vbegy")."' title='".__("Click here to update the captcha","vbegy")."' onclick=";$out .='"javascript:ask_get_captcha';$out .="('".get_template_directory_uri()."/captcha/create_image.php', 'ask_captcha_img_".$rand_q."');";$out .='"';$out .=" id='ask_captcha_img_".$rand_q."'>
										<span class='question_poll ask_captcha_span'>".__("Click on image to update the captcha .","vbegy")."</span>
									</p>";
								}
							}
							$out .= '</div>
							<p class="form-submit">
								<input type="submit" value="'.__("Publish Your Question","vbegy").'" class="button color small submit add_qu publish-question">
							</p>
						
						</form>
					</div>
				</div>
			</div></div>';
		}else {
			$out .= sprintf(__("Sorry do not have the minimum points Please do answer questions, even gaining points ( The minimum points = %s ) .","vbegy"),$question_points);
		}
	}
	return $out;
}
/* edit_question_shortcode */
add_shortcode('edit_question', 'edit_question_shortcode');
function edit_question_shortcode($atts, $content = null) {
	global $posted;
	$out = '';
	if (!is_user_logged_in()) {
		$out .= '<div class="form-style form-style-3"><div class="note_error"><strong>'.__("You must login to ask question .","vbegy").'</strong></div>'.do_shortcode("[ask_login]").'<ul class="login-links login-links-r"><li><a href="#">'.__("Register","vbegy").'</a></li></ul></div>';
	}else {
		$get_question = (int)$_GET["q"];
		$get_post_q = get_post($get_question);
		$q_tag = "";
		if ($terms = wp_get_object_terms( $get_question, 'question_tags' )) :
			$terms_array = array();
			foreach ($terms as $term) :
				$terms_array[] = $term->name;
				$q_tag = implode(' , ', $terms_array);
			endforeach;
		endif;
		$question_category = wp_get_post_terms($get_question,'question-category',array("fields" => "ids"));
		if (isset($question_category) && is_array($question_category)) {
			$question_category = $question_category[0];
		}
		$out .= '<div class="ask-question"><div class="form-style form-style-3 question-submit">
			<div class="ask_question">
				<div '.(!is_user_logged_in()?"class='if_no_login'":"").'>';
					do_action('edit_question');
					$rand_e = rand(1,1000);
					$out .= '
					<form class="new-question-form" method="post" enctype="multipart/form-data">
						<div class="note_error display"></div>
						<div class="form-inputs clearfix">
							<p>
								<label for="question-title-'.$rand_e.'" class="required">'.__("Question Title","vbegy").'<span>*</span></label>
								<input name="title" id="question-title-'.$rand_e.'" class="question-title" type="text" value="'.(isset($posted['title'])?$posted['title']:$get_post_q->post_title).'">
								<span class="form-description">'.__("Please choose an appropriate title for the question to answer it even easier .","vbegy").'</span>
							</p>
							
							<p>
								<label for="question_tags-'.$rand_e.'">'.__("Tags","vbegy").'</label>
								<input type="text" class="input question_tags" name="question_tags" id="question_tags-'.$rand_e.'" value="'.(isset($posted['question_tags'])?$posted['question_tags']:$q_tag).'" data-seperator=",">
								<span class="form-description">'.__("Please choose  suitable Keywords Ex : ","vbegy").'<span class="color">'.__("question , poll","vbegy").'</span> .</span>
							</p>
							
							<p>
								<label for="category-'.$rand_e.'" class="required">'.__("Category","vbegy").'<span>*</span></label>
								<span class="styled-select">'.
									wp_dropdown_categories(array("echo" => "0","show_option_none" => __("Select a Category","vbegy"),'taxonomy' => 'question-category', 'hide_empty' => 0,'depth' => 0,'id' => 'category-'.$rand_e,'name' => 'category',  'hierarchical' => true,'selected' => (isset($posted['category'])?(int)$posted['category']:$question_category)))
								.'</span>
								<span class="form-description">'.__("Please choose the appropriate section so easily search for your question .","vbegy").'</span>
							</p>
							
							<p class="question_poll_p">
								<label for="question_poll-'.$rand_e.'">'.__("Poll","vbegy").'</label>
								<input type="checkbox" id="question_poll-'.$rand_e.'" class="question_poll" value="1" name="question_poll" '.(isset($posted['question_poll']) && $posted['question_poll'] == 1 || get_post_meta($get_question,"question_poll",true) == 1?"checked='checked'":"").'>
								<span class="question_poll">'.__("This question is a poll ?","vbegy").'</span>
								<span class="poll-description">'.__("If you want to be doing a poll click here .","vbegy").'</span>
							</p>
							
							<div class="clearfix"></div>
							<div class="poll_options">
								<p class="form-submit add_poll">
									<button type="button" class="button color small submit add_poll_button"><i class="icon-plus"></i>'.__("Add Field","vbegy").'</button>
								</p>
								<ul class="question_poll_item">';
									if (isset($_POST['ask']) && is_array($_POST['ask'])) {
										$q_ask = $_POST['ask'];
									}else {
										$q_ask = get_post_meta($get_question,"ask");
										if (isset($q_ask[0]) && is_array($q_ask[0])) {
											$q_ask = $q_ask[0];
										}
									}
									if (isset($q_ask) && is_array($q_ask)) {
										foreach($q_ask as $ask) {
											if (stripslashes($ask['title']) != "") {
												$out .= '<li id="poll_li_'.(int)$ask['id'].'">
													<div class="poll-li">
														<p><input id="ask['.(int)$ask['id'].'][title]" class="ask" name="ask['.(int)$ask['id'].'][title]" value="'.stripslashes($ask['title']).'" type="text"></p>
														<input id="ask['.(int)$ask['id'].'][value]" name="ask['.(int)$ask['id'].'][value]" value="" type="hidden">
														<input id="ask['.(int)$ask['id'].'][id]" name="ask['.(int)$ask['id'].'][id]" value="'.(int)$ask['id'].'" type="hidden">
														<div class="del-poll-li"><i class="icon-remove"></i></div>
														<div class="move-poll-li"><i class="icon-fullscreen"></i></div>
													</div>
												</li>';
											}
										}
									}else {
										$out .= '<li id="poll_li_1">
											<div class="poll-li">
												<p><input id="ask[1][title]" class="ask" name="ask[1][title]" value="" type="text"></p>
												<input id="ask[1][value]" name="ask[1][value]" value="" type="hidden">
												<input id="ask[1][id]" name="ask[1][id]" value="1" type="hidden">
												<div class="del-poll-li"><i class="icon-remove"></i></div>
												<div class="move-poll-li"><i class="icon-fullscreen"></i></div>
											</div>
										</li>';
									}
								$out .= '</ul>
								<script> var nextli = '.(isset($_POST['ask']) && is_array($_POST['ask'])?count($_POST['ask'])+1:"2").';</script>
								<div class="clearfix"></div>
							</div>
							
						</div>
						<div>
							<p>
								<label for="question-details-'.$rand_e.'" class="required">'.__("Details","vbegy").'<span>*</span></label>
								<textarea name="comment" id="question-details-'.$rand_e.'" class="question-textarea" aria-required="true" cols="58" rows="8">'.(isset($posted['comment'])?$posted['comment']:$get_post_q->post_content).'</textarea>
								<span class="form-description">'.__("Type the description thoroughly and in detail .","vbegy").'</span>
							</p>
						</div>
						
						<div class="form-inputs clearfix">';
							$q_video_description = get_post_meta($get_question,"video_description",true);
							$q_video_type = get_post_meta($get_question,"video_type",true);
							$q_video_id = get_post_meta($get_question,"video_id",true);
							
							if (vpanel_options("video_desc_active") == 1) {
								$out .= '
								<p class="question_poll_p">
									<label for="video_description-'.$rand_e.'">'.__("Video description","vbegy").'</label>
									<input type="checkbox" id="video_description-'.$rand_e.'" class="video_description_input" name="video_description" value="1" '.(isset($posted['video_description']) && $posted['video_description'] == 1 || $q_video_description == 1?"checked='checked'":"").'>
									<span class="question_poll">'.__("Do you need a video to description the problem better ?","vbegy").'</span>
								</p>
								
								<div class="video_description" '.(isset($posted['video_description']) && $posted['video_description'] == 1 || $q_video_description == 1?"style='display:block;'":"").'>
									<p>
										<label for="video_type-'.$rand_e.'">'.__("Video type","vbegy").'</label>
										<span class="styled-select">
											<select id="video_type-'.$rand_e.'" class="video_type" name="video_type">
												<option value="youtube" '.(isset($posted['video_type']) && $posted['video_type'] == "youtube" || $q_video_type == "youtube"?' selected="selected"':'').'>Youtube</option>
												<option value="vimeo" '.(isset($posted['video_type']) && $posted['video_type'] == "vimeo" || $q_video_type == "vimeo"?' selected="selected"':'').'>Vimeo</option>
												<option value="daily" '.(isset($posted['video_type']) && $posted['video_type'] == "daily" || $q_video_type == "daily"?' selected="selected"':'').'>Dialymotion</option>
											</select>
										</span>
										<span class="form-description">'.__("Choose from here the video type .","vbegy").'</span>
									</p>
									
									<p>
										<label for="video_id-'.$rand_e.'">'.__("Video ID","vbegy").'</label>
										<input name="video_id" id="video_id-'.$rand_e.'" class="video_id" type="text" value="'.(isset($posted['video_id'])?$posted['video_id']:$q_video_id).'">
										<span class="form-description">'.__("Put here the video id : http://www.youtube.com/watch?v=sdUUx5FdySs EX : 'sdUUx5FdySs' .","vbegy").'</span>
									</p>
								</div>';
							}
						$q_remember_answer = get_post_meta($get_question,"remember_answer",true);
						$out .= '
						<p class="question_poll_p">
							<label for="remember_answer-'.$rand_e.'">'.__("Notified","vbegy").'</label>
							<input type="checkbox" id="remember_answer-'.$rand_e.'" class="remember_answer" name="remember_answer" value="1" '.(isset($posted['remember_answer']) && $posted['remember_answer'] == 1 || $q_remember_answer == 1?"checked='checked'":"").'>
							<span class="question_poll">'.__("Notified by e-mail at incoming answers .","vbegy").'</span>
						</p>
						</div>
						<p class="form-submit">
							<input type="hidden" name="ID" value="'.$get_question.'">
							<input type="submit" value="'.__("Edit Your Question","vbegy").'" class="button color small submit add_qu publish-question">
						</p>
					
					</form>
				</div>
			</div>
		</div></div>';
	}
	return $out;
}
/* is_user_logged_in_data */
function is_user_logged_in_data () {
	$out = '';
	if (is_user_logged_in()) {
		$user_login = get_userdata(get_current_user_id());
		$you_avatar = get_the_author_meta('you_avatar',$user_login->ID);
		$url = get_the_author_meta('url',$user_login->ID);
		$country = get_the_author_meta('country',$user_login->ID);
		$twitter = get_the_author_meta('twitter',$user_login->ID);
		$facebook = get_the_author_meta('facebook',$user_login->ID);
		$google = get_the_author_meta('google',$user_login->ID);
		$linkedin = get_the_author_meta('linkedin',$user_login->ID);
		$follow_email = get_the_author_meta('follow_email',$user_login->ID);
		$out .= '<div class="row">
			<div class="col-md-8">
				<div class="is-login-left user-profile-img">
					<a original-title="'.$user_login->display_name.'" class="tooltip-n" href="'.get_author_posts_url($user_login->ID).'">';
						if ($you_avatar) {
							$you_avatar_img = get_aq_resize_url(esc_attr($you_avatar),"full",79,79);
							$out .= "<img alt='".$user_login->display_name."' src='".$you_avatar_img."'>";
						}else {
							$out .= get_avatar(get_the_author_meta('user_email',$user_login->ID),'65','');
						}
					$out .= '</a>
				</div>
				<div class="is-login-right">
					<h2>'.__("Welcome","vbegy").' '.$user_login->display_name.'</h2>
					<p>'.$user_login->description.'</p>';
					if ($facebook || $twitter || $linkedin || $google || $follow_email) {
						if ($facebook) {
						$out .= '<a href="'.$facebook.'" original-title="'.__("Facebook","vbegy").'" class="tooltip-n">
							<span class="icon_i">
								<span class="icon_square" icon_size="30" span_bg="#3b5997">
									<i class="social_icon-facebook"></i>
								</span>
							</span>
						</a>';
						}
						if ($twitter) {
						$out .= '<a href="'.$twitter.'" original-title="'.__("Twitter","vbegy").'" class="tooltip-n">
							<span class="icon_i">
								<span class="icon_square" icon_size="30" span_bg="#00baf0">
									<i class="social_icon-twitter"></i>
								</span>
							</span>
						</a>';
						}
						if ($linkedin) {
						$out .= '<a href="'.$linkedin.'" original-title="'.__("Linkedin","vbegy").'" class="tooltip-n">
							<span class="icon_i">
								<span class="icon_square" icon_size="30" span_bg="#006599">
									<i class="social_icon-linkedin"></i>
								</span>
							</span>
						</a>';
						}
						if ($google) {
						$out .= '<a href="'.$google.'" original-title="'.__("Google plus","vbegy").'" class="tooltip-n">
							<span class="icon_i">
								<span class="icon_square" icon_size="30" span_bg="#c43c2c">
									<i class="social_icon-gplus"></i>
								</span>
							</span>
						</a>';
						}
						if ($follow_email) {
						$out .= '<a href="'.$follow_email.'" original-title="'.__("Email","vbegy").'" class="tooltip-n">
							<span class="icon_i">
								<span class="icon_square" icon_size="30" span_bg="#000">
									<i class="social_icon-email"></i>
								</span>
							</span>
						</a>';
						}
					}
				$out .= '</div>
			</div>
			<div class="col-md-4">
				<h2>'.__("Quick Links","vbegy").'</h2>
				<div class="row">
					<div class="col-md-6">
						<ul class="user_quick_links">
							<li><a href="'.get_author_posts_url($user_login->ID).'"><i class="icon-home"></i>'.__("Profile page","vbegy").'</a></li>
							<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('question_user_page'))).'"><i class="icon-question-sign"></i>'.__("Questions","vbegy").'</a></li>
							<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('answer_user_page'))).'"><i class="icon-comment"></i>'.__("Answers","vbegy").'</a></li>
							<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('favorite_user_page'))).'"><i class="icon-star"></i>'.__("Favorite Questions","vbegy").'</a></li>
							<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('i_follow_user_page'))).'"><i class="icon-user-md"></i>'.__("Authors I Follow","vbegy").'</a></li>
							<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('followers_user_page'))).'"><i class="icon-user"></i>'.__("Followers","vbegy").'</a></li>
						</ul>
					</div>
					<div class="col-md-6">
						<ul class="user_quick_links">
							<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('point_user_page'))).'"><i class="icon-heart"></i>'.__("Points","vbegy").'</a></li>
							<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('follow_question_page'))).'"><i class="icon-question-sign"></i>'.__("Follow question","vbegy").'</a></li>';
							$out .= '<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('follow_answer_page'))).'"><i class="icon-comment"></i>'.__("Follow answer","vbegy").'</a></li>';
							$out .= '<li><a href="'.add_query_arg("u", esc_attr($user_login->ID),get_page_link(vpanel_options('post_user_page'))).'"><i class="icon-file-alt"></i>'.__("Posts","vbegy").'</a></li>
							<li><a href="'.get_page_link(vpanel_options('user_edit_profile_page')).'"><i class="icon-pencil"></i>'.__("Edit profile","vbegy").'</a></li>
							<li><a href="'.wp_logout_url(home_url()).'"><i class="icon-signout"></i>'.__("Logout","vbegy").'</a></li>
						</ul>
					</div>
				</div>
			</div><!-- End col-md-4 -->
		</div><!-- End row -->';
	}else {
		$out .= '<div class="form-style form-style-3">
			'.do_shortcode("[ask_login]").'
		</div>';
	}
	return $out;
}
/* Login shortcode */
function ask_login ($atts, $content = null) {
	global $user_identity,$user_ID;
	$a = shortcode_atts( array(
	    'forget' => 'asd',
	), $atts );
	$out = '';
	if (is_user_logged_in()) :
		$user_login = get_userdata(get_current_user_id());
		$out .= is_user_logged_in_data();
	else:
	$out .= '<div class="ask_form inputs">
		<form class="login-form ask_login" action="'.home_url('/').'" method="post">
			<div class="ask_error"></div>
			
			<div class="form-inputs clearfix">
				<p class="login-text">
					<input class="required-item" type="text" value="'.__("Username","vbegy").'" onfocus="if (this.value == \''.__("Username","vbegy").'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.__("Username","vbegy").'\';}" name="log">
					<i class="icon-user"></i>
				</p>
				<p class="login-password">
					<input class="required-item" type="password" value="'.__("Password","vbegy").'" onfocus="if (this.value == \''.__("Password","vbegy").'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.__("Password","vbegy").'\';}" name="pwd">
					<i class="icon-lock"></i>
					'.(isset($a["forget"]) && $a["forget"] == "false"?'':'<a href="#">'.__("Forget","vbegy").'</a>').'
				</p>
			</div>
			<p class="form-submit login-submit">
				<div class="loader_2"></div>
				<input type="submit" value="'.__("Log in","vbegy").'" class="button color small login-submit submit sidebar_submit">
			</p>
			<div class="rememberme">
				<label><input type="checkbox"input name="rememberme" checked="checked"> '.__("Remember Me","vbegy").'</label>
			</div>
			
			<input type="hidden" name="redirect_to" value="'.strip_tags( 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']).'">
			<input type="hidden" name="login_nonce" value="'.wp_create_nonce("ask-login-action").'">
			<input type="hidden" name="ajax_url" value="'.admin_url('admin-ajax.php').'">
			<div class="errorlogin"></div>
		</form>
	</div>';
	endif;
	return $out;
}
function ask_login_shortcode() {
	add_shortcode("ask_login","ask_login");
}
add_action("init","ask_login_shortcode");
add_filter("the_content","do_shortcode");
add_filter("widget_text","do_shortcode");
function ask_login_jquery() {
	if ( isset( $_REQUEST['redirect_to'] ) ) $redirect_to = $_REQUEST['redirect_to']; else $redirect_to = admin_url();
	if ( is_ssl() && force_ssl_login() && !force_ssl_admin() && ( 0 !== strpos($redirect_to, 'https') ) && ( 0 === strpos($redirect_to, 'http') ) )$secure_cookie = false; else $secure_cookie = '';
	$user = wp_signon('', $secure_cookie);
	// Check the username
	if ( !$_POST['log'] ) :
		$user = new WP_Error();
		$user->add('empty_username', __('<strong>Error :&nbsp;</strong>please insert your name .','vbegy'));
	elseif ( !$_POST['pwd'] ) :
		$user = new WP_Error();
		$user->add('empty_username', __('<strong>Error :&nbsp;</strong>please insert your password .','vbegy'));
	endif;
	if (ask_is_ajax()) :
		// Result
		$result = array();
		if ( !is_wp_error($user) ) :
			$result['success'] = 1;
			$result['redirect'] = $redirect_to;
		else :
			$result['success'] = 0;
			foreach ($user->errors as $error) {
				$result['error'] = $error[0];
				break;
			}
		endif;
		echo json_encode($result);
		die();
	else :
		if ( !is_wp_error($user) ) :
			wp_redirect($redirect_to);
			exit;
		endif;
	endif;
	return $user;
}
if (!function_exists('ask_is_ajax')) {
	function ask_is_ajax() {
		if (defined('DOING_AJAX')) return true;
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') return true;
		return false;
	}
}
function ask_login_process() {
	global $ask_login_errors;
	if (isset($_POST['login-form']) && $_POST['login-form']) :
		$ask_login_errors = ask_login_jquery();
	endif;
}
add_action('init','ask_login_process');
function ask_ajax_login_process() {
	check_ajax_referer( 'ask-login-action', 'security' );
	ask_login_jquery();
	die();
}
add_action('wp_ajax_ask_ajax_login_process','ask_ajax_login_process');
add_action('wp_ajax_nopriv_ask_ajax_login_process','ask_ajax_login_process');
/* Signup shortcode */
add_shortcode('ask_signup', 'ask_signup_shortcode');
function ask_signup_shortcode($atts, $content = null) {
	global $user_identity,$posted;
	$a = shortcode_atts( array(
	    'dark_button' => '',
	), $atts );
	$out = '';
	if (is_user_logged_in()) {
		$user_login = get_userdata(get_current_user_id());
		$out .= is_user_logged_in_data();
	}else {
		if (!is_user_logged_in()) {
		$rand_w = rand(1,1000);
		$out .= '
		<form method="post" class="signup_form ask_form" enctype="multipart/form-data">';
			do_action('ask_signup');
			$out .= '<div class="ask_error"></div>
				<div class="form-inputs clearfix">
					<p>
						<label for="user_name_'.$rand_w.'" class="required">'.__("Username","vbegy").'<span>*</span></label>
						<input type="text" class="required-item" name="user_name" id="user_name_'.$rand_w.'" value="'.(isset($posted["user_name"])?$posted["user_name"]:"").'">
					</p>
					<p>
						<label for="email_'.$rand_w.'" class="required">'.__("E-Mail","vbegy").'<span>*</span></label>
						<input type="email" class="required-item" name="email" id="email_'.$rand_w.'" value="'.(isset($posted["email"])?$posted["email"]:"").'">
					</p>
					<p>
						<label for="pass1_'.$rand_w.'" class="required">'.__("Password","vbegy").'<span>*</span></label>
						<input type="password" class="required-item" name="pass1" id="pass1_'.$rand_w.'" autocomplete="off">
					</p>
					<p>
						<label for="pass2_'.$rand_w.'" class="required">'.__("Confirm Password","vbegy").'<span>*</span></label>
						<input type="password" class="required-item" name="pass2" id="pass2_'.$rand_w.'" autocomplete="off">
					</p>';
					$profile_picture = vpanel_options("profile_picture");
					$profile_picture_required = vpanel_options("profile_picture_required");
					if ($profile_picture == 1) {
						$out .= '<label '.($profile_picture_required == 1?'class="required"':'').' for="attachment_'.$rand_w.'">'.__('Profile Picture','vbegy').($profile_picture_required == 1?'<span>*</span>':'').'</label>
						<div class="fileinputs">
							<input type="file" name="you_avatar" id="attachment_'.$rand_w.'">
							<div class="fakefile">
								<button type="button" class="small margin_0">'.__('Select file','vbegy').'</button>
								<span><i class="icon-arrow-up"></i>'.__('Browse','vbegy').'</span>
							</div>
						</div>';
					}
					$the_captcha = vpanel_options("the_captcha");
					$captcha_style = vpanel_options("captcha_style");
					$captcha_question = vpanel_options("captcha_question");
					$captcha_answer = vpanel_options("captcha_answer");
					$show_captcha_answer = vpanel_options("show_captcha_answer");
					if ($the_captcha == 1) {
						if ($captcha_style == "question_answer") {
							$out .= "
							<p class='ask_captcha_p'>
								<label for='ask_captcha' class='required'>".__("Captcha","vbegy")."<span>*</span></label>
								<input size='10' id='ask_captcha' name='ask_captcha' class='ask_captcha' value='' type='text'>
								<span class='question_poll ask_captcha_span'>".$captcha_question.($show_captcha_answer == 1?" ( ".$captcha_answer." )":"")."</span>
							</p>";
						}else {
							$rand = rand(0000,9999);
							$out .= "
							<p class='ask_captcha_p'>
								<label for='ask_captcha_".$rand."' class='required'>".__("Captcha","vbegy")."<span>*</span></label>
								<input size='10' id='ask_captcha_".$rand."' name='ask_captcha' class='ask_captcha' value='' type='text'><img class='ask_captcha_img' src='".get_template_directory_uri()."/captcha/create_image.php' alt='".__("Captcha","vbegy")."' title='".__("Click here to update the captcha","vbegy")."' onclick=";$out .='"javascript:ask_get_captcha';$out .="('".get_template_directory_uri()."/captcha/create_image.php', 'ask_captcha_img_".$rand."');";$out .='"';$out .=" id='ask_captcha_img_".$rand."'>
								<span class='question_poll ask_captcha_span'>".__("Click on image to update the captcha .","vbegy")."</span>
							</p>";
						}
					}
				$out .= '</div>
				<p class="form-submit">
					<input type="submit" name="register" value="'.__("Signup","vbegy").'" class="button color '.(isset($a["dark_button"]) && $a["dark_button"] == "dark_button"?"dark_button":"").' small submit">
				</p>
		</form>';
		}
	}
	return $out;
}
function ask_signup_process() {
	global $posted;
	$errors = new WP_Error();
	if (isset($_POST['register']) && $_POST['register']) :
		// Process signup form
		$posted = array(
			'user_name'   => esc_html($_POST['user_name']),
			'email'       => esc_html($_POST['email']),
			'pass1'       => esc_html($_POST['pass1']),
			'pass2'       => esc_html($_POST['pass2']),
			'ask_captcha' => esc_html($_POST['ask_captcha']),
		);
		$posted = array_map('stripslashes', $posted);
		$posted['username'] = sanitize_user((isset($posted['username'])?$posted['username']:""));
		// Validation
		if ( empty($posted['user_name']) ) $errors->add('required-user_name',__("Please enter your name.","vbegy"));
		if ( empty($posted['email']) ) $errors->add('required-email',__("Please enter your email.","vbegy"));
		if ( empty($posted['pass1']) ) $errors->add('required-pass1',__("Please enter your password.","vbegy"));
		if ( empty($posted['pass2']) ) $errors->add('required-pass2',__("Please rewrite password.","vbegy"));
		if ( $posted['pass1']!==$posted['pass2'] ) $errors->add('required-pass1',__("Password does not match.","vbegy"));
		$the_captcha = vpanel_options("the_captcha");
		$captcha_style = vpanel_options("captcha_style");
		$captcha_question = vpanel_options("captcha_question");
		$captcha_answer = vpanel_options("captcha_answer");
		$show_captcha_answer = vpanel_options("show_captcha_answer");
		if ($the_captcha == 1) {
			if (empty($posted["ask_captcha"])) {
				$errors->add('required-captcha', __("There are required fields ( captcha ).","vbegy"));
			}
			if ($captcha_style == "question_answer") {
				if ($captcha_answer != $posted["ask_captcha"]) {
					$errors->add('required-captcha-error', __('The captcha is incorrect, please try again.','vbegy'));
				}
			}else {
				if ($_SESSION["security_code"] != $posted["ask_captcha"]) {
					$errors->add('required-captcha-error', __('The captcha is incorrect, please try again.','vbegy'));
				}
			}
		}
		$profile_picture = vpanel_options("profile_picture");
		$profile_picture_required = vpanel_options("profile_picture_required");
		if(isset($_FILES['you_avatar']) && !empty($_FILES['you_avatar']['name'])) :
			require_once(ABSPATH . "wp-admin" . '/includes/file.php');					
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$you_avatar = wp_handle_upload($_FILES['you_avatar'], array('test_form'=>false), current_time('mysql'));
			if ( isset($you_avatar['error']) ) :
				$errors->add('upload-error',  __('Error in upload the image : ','vbegy') . $you_avatar['error'] );
				return $errors;
			endif;
		else:
			if ($profile_picture_required == 1) {
				$errors->add('required-captcha', __("There are required fields ( Profile Picture ).","vbegy"));
			}
		endif;
		// Check the username
		if ( !validate_username( $posted['user_name'] ) || strtolower($posted['user_name'])=='admin' ) :
			$errors->add('required-user_name',__("Wrong name.","vbegy"));
		elseif ( username_exists( $posted['user_name'] ) ) :
			$errors->add('required-user_name',__("This account is already registered.","vbegy"));
		endif;
		// Check the e-mail address
		if ( !is_email( $posted['email'] ) ) :
			$errors->add('required-email',__("Please write correctly email.","vbegy"));
		elseif ( email_exists( $posted['email'] ) ) :
			$errors->add('required-email',__("This account is already registered.","vbegy"));
		endif;
		if ( $errors->get_error_code() ) return $errors;
		if ( !$errors->get_error_code() ) :
			do_action('register_post', $posted['user_name'], $posted['email'], $errors);
			$errors = apply_filters( 'registration_errors', $errors, $posted['user_name'], $posted['email'] );
			// if there are no errors, let's create the user account
			if ( !$errors->get_error_code() ) :
				$user_id = wp_create_user( $posted['user_name'], $posted['pass1'], $posted['email'] );
				if ( !$user_id ) :
					$errors->add('error', sprintf(__('<strong>Error</strong>: Sorry can not register please contact the webmaster ','vbegy'), get_option('admin_email')));
				else :
					if ($you_avatar) :
						update_user_meta($user_id,"you_avatar",$you_avatar["url"]);
					endif;
					$confirm_email = vpanel_options("confirm_email");
					if ($confirm_email == 1) {
						$activation = get_role("activation");
						if (!isset($activation)) {
							add_role("activation","activation",array('read' => false));
						}
						wp_update_user( array ('ID' => $user_id, 'role' => 'activation') ) ;
						$rand_a = rand(1,1000000000000);
						update_user_meta($user_id,"activation",$rand_a);
						$user_data = get_user_by("id",$user_id);
						$post_mail = "
						".__("Hi there","vbegy")."<br />
						
						".__("This is the link to activate your membership","vbegy")."<br />
						
						<a href=".add_query_arg(array("u" => $user_id,"activate" => $rand_a),esc_url(home_url('/'))).">".__("Activate","vbegy")."</a><br />
						
						";
						sendEmail(get_bloginfo("admin_email"),get_bloginfo('name'),esc_html($user_data->user_email),esc_html($user_data->display_name),__("Hi there","vbegy"),$post_mail);
					}else {
						wp_update_user( array ('ID' => $user_id, 'role' => 'subscriber') ) ;
					}
					wp_new_user_notification( $user_id, $posted['pass1'] );
					$secure_cookie = is_ssl() ? true : false;
					wp_set_auth_cookie($user_id, true, $secure_cookie);
					wp_safe_redirect(get_bloginfo(url));
					exit;
				endif;
			endif;
		endif;
	endif;
	return;
}
function ask_signup() {
	if ($_POST) :
		$return = ask_signup_process();
		if (is_wp_error($return) ) :
			echo '<div class="ask_error"><strong><p>'.__("Error","vbegy").' :&nbsp;</strong>'.wptexturize(str_replace('<strong>'.__("Error","vbegy").'</strong>: ', '', $return->get_error_message())).'</p></div>';
   		endif;
	endif;
}
add_action('ask_signup', 'ask_signup');
/* Lostpassword shortcode */
add_shortcode('ask_lost_pass', 'ask_lost_pass');
function ask_lost_pass($atts, $content = null) {
	global $user_identity;
	$a = shortcode_atts( array(
	    'dark_button' => '',
	), $atts );
	$out = '';
	if (is_user_logged_in()) :
		$user_login = get_userdata(get_current_user_id());
		$out .= is_user_logged_in_data();
	else:
		do_action('ask_lost_password');
		$rand_w = rand(1,1000);
		$out .= '
		<form method="post" class="ask-lost-password ask_form" action="">
			<div class="ask_error"></div>
			<div class="form-inputs clearfix">
				<p>
					<label for="user_name_'.$rand_w.'" class="required">'.__("Username","vbegy").'<span>*</span></label>
					<input type="text" class="required-item" name="user_name" id="user_name_'.$rand_w.'">
				</p>
				<p>
					<label for="user_mail_'.$rand_w.'" class="required">'.__("E-Mail","vbegy").'<span>*</span></label>
					<input type="email" class="required-item" name="user_mail" id="user_mail_'.$rand_w.'">
				</p>
			</div>
			<p class="form-submit">
				<input type="submit" value="'.__("Reset","vbegy").'" class="button color '.(isset($a["dark_button"]) && $a["dark_button"] == "dark_button"?"dark_button":"").' small submit">
			</p>
		</form>';
	endif;
	return $out;
}
function ask_process_lost_pass() {
	global $posted, $wpdb;
	$errors = new WP_Error();
	$posted = array(
		'user_name' => esc_html($_POST['user_name']),
		'user_mail' => esc_html($_POST['user_mail']),
	);
	$posted = array_map('stripslashes', $posted);
	if ( empty($posted['user_name']) ) $errors->add('required-user_name',__("Please enter your name.","vbegy"));
	if ( empty($posted['user_mail']) ) $errors->add('required-user_mail',__("Please enter your email.","vbegy"));
	if (!username_exists($posted['user_name']) && !email_exists($posted['user_mail'])) {
		$errors->add('required-two',__("Name or Email is not correct.","vbegy"));
	}
	$get_user_by_mail = get_user_by('email',$posted['user_mail']);
	$get_user_by_name = get_user_by('login',$posted['user_name']);
	if ($get_user_by_name->ID != $get_user_by_mail->ID) {
		$errors->add('required-two',__("Name or Email is not correct.","vbegy"));
	}
	if ( $errors->get_error_code() ) return $errors;
	$pw 	= ask_generate_random();
	$pwdb	= md5(trim($pw));
	$wpdb->query("UPDATE ".$wpdb->users." SET user_pass = '".$pwdb."' WHERE ID = ".$get_user_by_name->ID);
	$headers = 'From: '.get_bloginfo("name").' <'.get_bloginfo("admin_email").'>' . "\r\n";
	$send_text = '';
	$send_text .= "\r\n ".__("You are :","vbegy")." ".$posted['user_name'];
	$send_text .= "\r\n ".__("The New Password :")." ".$pw;
	wp_mail($posted['user_mail'],__("Your password","vbegy"),$send_text,$headers);
	return;
}
function ask_lost_pass_word() {
	if ($_POST) :
		$return = ask_process_lost_pass();
		if ( is_wp_error($return) ) :
   			echo '<div class="ask_error"><strong>'.__("Error","vbegy").' :&nbsp;'.$return->get_error_message().'</strong></div>';
   		else :
   			echo '<div class="ask_done"><strong>'.__("A reset password will be sent to this email address","vbegy").'</strong></div>';
   		endif;
	endif;
}
add_action('ask_lost_password', 'ask_lost_pass_word');
/* Generate random code */
function ask_generate_random($length = 6, $letters = '1234567890qwertyuiopasdfghjklzxcvbnm') {
	$s = '';
	$lettersLength = strlen($letters)-1;
	for($i = 0 ; $i < $length ; $i++) {
		$s .= $letters[rand(0,$lettersLength)];
	}
	return $s;
}
/* hex2rgb */
function hex2rgb ($hex) {
   $hex = str_replace("#","",$hex);
   if (strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   }else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return $rgb;
}
/* ask_edit_profile_shortcode */
add_shortcode('ask_edit_profile', 'ask_edit_profile_shortcode');
function ask_edit_profile_shortcode($atts, $content = null) {
	global $user_identity,$posted,$public_display;
	$out = '';
	if (!is_user_logged_in()) {
		$out .= '<div class="note_error"><strong>'.__("Please login to edit profile .","vbegy").'</strong></div>
		<div class="form-style form-style-3">
			'.do_shortcode("[ask_login]").'
		</div>';
	}else {
		do_action('ask_edit_profile_form');
		$out .= '<form class="edit-profile-form vpanel_form" method="post" enctype="multipart/form-data">';
		
			$user_info = get_userdata(get_current_user_id());
			$you_avatar = get_the_author_meta('you_avatar',$user_info->ID);
			$url = get_the_author_meta('url',$user_info->ID);
			$country = get_the_author_meta('country',$user_info->ID);
			$twitter = get_the_author_meta('twitter',$user_info->ID);
			$facebook = get_the_author_meta('facebook',$user_info->ID);
			$google = get_the_author_meta('google',$user_info->ID);
			$linkedin = get_the_author_meta('linkedin',$user_info->ID);
			$follow_email = get_the_author_meta('follow_email',$user_info->ID);
			
			$out .= '
			
			<div class="form-inputs clearfix">
				<p>
					<label>'.__("First Name","vbegy").'</label>
					<input name="first_name" id="first_name" type="text" value="'.$user_info->first_name.'">
				</p>
				<p>
					<label>'.__("Last Name","vbegy").'</label>
					<input name="last_name" id="last_name" type="text" value="'.$user_info->last_name.'">
				</p>
				<p>
					<label for="email" class="required">'.__("E-Mail","vbegy").'<span>*</span></label>
					<input name="email" id="email" type="email" value="'.$user_info->user_email.'">
				</p>
				<p>
					<label>'.__("Website","vbegy").'</label>
					<input name="url" id="url" type="text" value="'.$url.'">
				</p>
				<p>
					<label for="newpassword" class="required">'.__("Password","vbegy").'<span>*</span></label>
					<input name="pass1" id="newpassword" type="password" value="">
				</p>
				<p>
					<label for="newpassword2" class="required">'.__("Confirm Password","vbegy").'<span>*</span></label>
					<input name="pass2" id="newpassword2" type="password" value="">
				</p>
				<p>
					<label for="country">'.__("Country","vbegy").'</label>
					<input name="country" id="country" type="text" value="'.$country.'">
				</p>
				<p>
					<label for="follow_email">'.__("Follow-up email","vbegy").'</label>
					<input name="follow_email" id="follow_email" type="text" value="'.$follow_email.'">
				</p>
			</div>
			<div class="form-style form-style-2">';
				if ($you_avatar) {
					$you_avatar_img = get_aq_resize_url(esc_attr($you_avatar),"full",79,79);
					$out .= "<div class='user-profile-img'><img alt='".$user_info->display_name."' src='".$you_avatar_img."'></div>";
				}
				$out .= '
					<label for="you_avatar">'.__("Profile Picture","vbegy").'</label>
					<div class="fileinputs">
						<input type="file" name="you_avatar" id="you_avatar" value="'.$you_avatar.'">
						<div class="fakefile">
							<button type="button" class="small margin_0">Select file</button>
							<span><i class="icon-arrow-up"></i>Browse</span>
						</div>
					</div>
				<div class="clearfix"></div>
				<p></p>
				
				<p>
					<label for="description">'.__("About Yourself","vbegy").'</label>
					<textarea name="description" id="description" cols="58" rows="8">'.$user_info->description.'</textarea>
				</p>
			</div>
			<div class="form-inputs clearfix">
				<p>
					<label for="facebook">'.__("Facebook","vbegy").'</label>
					<input type="text" name="facebook" id="facebook" value="'.$facebook.'">
				</p>
				<p>
					<label for="twitter">'.__("Twitter","vbegy").'</label>
					<input type="text" name="twitter" id="twitter" value="'.$twitter.'">
				</p>
				<p>
					<label for="linkedin">'.__("Linkedin","vbegy").'</label>
					<input type="text" name="linkedin" id="linkedin" value="'.$linkedin.'">
				</p>
				<p>
					<label for="google">'.__("Google plus","vbegy").'</label>
					<input type="text" name="google" id="google" value="'.$google.'">
				</p>
			</div>
			
			<p class="form-submit">
				<input type="hidden" name="action" value="update">
				<input type="hidden" name="admin_bar_front" value="1">
				<input type="hidden" name="user_id" id="user_id" value="'.$user_info->ID.'">
				<input type="hidden" name="user_login" id="user_login" value="'.$user_info->user_login.'">
				<input type="submit" value="'.__("Edit","vbegy").'" class="button color small login-submit submit">
			</p>
		
		</form>';
	}
	return $out;
}
/* aau_sanitize_user */
function aau_sanitize_user($username, $raw_username, $strict) {
	$username = wp_strip_all_tags( $raw_username );
	$username = remove_accents( $username );
	$username = preg_replace( '|%([a-fA-F0-9][a-fA-F0-9])|', '', $username );
	$username = preg_replace( '/&.+?;/', '', $username ); // Kill entities

	if ( $strict )
		$username = preg_replace( '|[^a-z\p{Arabic}0-9 _.\-@]|iu', '', $username );

	$username = trim( $username );
	$username = preg_replace( '|\s+|', ' ', $username );

	return $username;
}
add_filter('sanitize_user', 'aau_sanitize_user', 10, 3);
/* lenient_username */
add_filter('sanitize_user', 'lenient_username', 1, 3);
function lenient_username($username, $raw_username, $strict) {
	$username = $raw_username;
	$username = wp_strip_all_tags( $username );
	$username = remove_accents( $username );
	$username = preg_replace('|[^a-z0-9 _.\-@ضصثقفغعهخحجدذشسيبلاتنمك طئءؤـرلاىةوزظًٌَُّلإإألأٍِ~ْلآآ]|i', '', $username); 
	$username = preg_replace( '/&.+?;/', '', $username );
	$username = preg_replace( '|\s+|', ' ', $username );
	return $username;
}
?>