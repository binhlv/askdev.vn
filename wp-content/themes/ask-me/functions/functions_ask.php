<?php
ob_start();
function ask_members_only() {
	if (!is_user_logged_in()) ask_redirect_login();
}
/* question_poll */
function question_poll() {
	$poll_id = (int)$_POST['poll_id'];
	$poll_title = stripslashes($_POST['poll_title']);
	$post_id = (int)$_POST['post_id'];

	$custom = get_post_custom($post_id);
	$asks = unserialize($custom["ask"][0]);
	
	$question_poll_num = get_post_meta($post_id,'question_poll_num',true);
	$question_poll_num++;
	update_post_meta($post_id,'question_poll_num',$question_poll_num);
	
	$needle = $asks[$poll_id];
	$value = $needle["value"];
	
	if ($value == "") {
		$value_end = 1;
	}else {
		$value_end = $value+1;
	}
	$replacement = array("title" => $poll_title,"value" => $value_end,"id" => $poll_id);
	
	for($i = 1; $i<= count($asks); $i++) {
		if($asks[$i] == $needle) {
			$asks[$i] = $replacement;
		}
	}
	
	$update = update_post_meta($post_id,'ask',$asks);

	if($update) {
		setcookie('question_poll'.$post_id,"ask_yes_poll",time()+3600*24*365,'/');
	}
	
	die();
}
add_action('wp_ajax_question_poll','question_poll');
add_action('wp_ajax_nopriv_question_poll','question_poll');
/* question_vote_up */
function question_vote_up() {
	$id = (int)$_POST['id'];
	$count = get_post_meta($id,'question_vote',true);
	if(!$count)
		$count = 0;
	$count++;
	$update = update_post_meta($id,'question_vote',$count);
	if($update) {
		setcookie('question_vote'.$id,"ask_yes",time()+3600*24*365,'/');
	}
	echo $count;
	die();
}
add_action('wp_ajax_question_vote_up','question_vote_up');
add_action('wp_ajax_nopriv_question_vote_up','question_vote_up');
/* question_vote_down */
function question_vote_down() {
	$id = (int)$_POST['id'];
	$count = get_post_meta($id,'question_vote',true);
	if(!$count)
		$count = 0;
	$count--;
	$update = update_post_meta($id,'question_vote',$count);
	if($update) {
		setcookie('question_vote'.$id,"ask_yes",time()+3600*24*365,'/');
	}
	echo $count;
	die();
}
add_action('wp_ajax_question_vote_down','question_vote_down');
add_action('wp_ajax_nopriv_question_vote_down','question_vote_down');
/* comment_vote_up */
function comment_vote_up() {
	$id = (int)$_POST['id'];
	$get_comment = get_comment($id);
	$post_id = $get_comment->comment_post_ID;
	
	if ($get_comment->user_id != 0){
		$user_votes_id = $get_comment->user_id;
		$add_votes = get_user_meta($user_votes_id,"add_votes_all",true);
		if ($add_votes == "" or $add_votes == 0) {
			update_user_meta($user_votes_id,"add_votes_all",1);
		}else {
			update_user_meta($user_votes_id,"add_votes_all",$add_votes+1);
		}
	
		$current_user = $get_comment->user_id;
		$user_vote = get_user_by("id",$get_comment->user_id);
		$_points = get_user_meta($get_comment->user_id,$user_vote->user_login."_points",true);
		$_points++;
	
		update_user_meta($get_comment->user_id,$user_vote->user_login."_points",$_points);
		add_user_meta($get_comment->user_id,$user_vote->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),(vpanel_options("point_rating_answer") != ""?vpanel_options("point_rating_answer"):1),"+",__("Rating your answer","vbegy"),$post_id,$id));
	
		$points_user = get_user_meta($get_comment->user_id,"points",true);
		update_user_meta($get_comment->user_id,"points",$points_user+(vpanel_options("point_rating_answer") != ""?vpanel_options("point_rating_answer"):1));
	}
	
	$count = get_comment_meta($id,'comment_vote');
	$count = (!empty($count[0]["vote"])?$count[0]["vote"]:0);
	if(!$count)
		$count = 0;
	$count++;
	$update = update_comment_meta($id,'comment_vote',array("vote" => $count,"post_id" => $post_id));
	if($update) {
		setcookie('comment_vote'.$id,"ask_yes_comment",time()+3600*24*365,'/');
	}
	echo $count;
	die();
}
add_action('wp_ajax_comment_vote_up','comment_vote_up');
add_action('wp_ajax_nopriv_comment_vote_up','comment_vote_up');
/* comment_vote_down */
function comment_vote_down() {
	$id = (int)$_POST['id'];
	$get_comment = get_comment($id);
	$post_id = $get_comment->comment_post_ID;
	
	if ($get_comment->user_id != 0){
		$user_votes_id = $get_comment->user_id;
		$add_votes = get_user_meta($user_votes_id,"add_votes_all",true);
		if ($add_votes == "" or $add_votes == 0) {
			update_user_meta($user_votes_id,"add_votes_all",1);
		}else {
			update_user_meta($user_votes_id,"add_votes_all",$add_votes+1);
		}
		
		$current_user = $get_comment->user_id;
		$user_vote = get_user_by("id",$get_comment->user_id);
		$_points = get_user_meta($get_comment->user_id,$user_vote->user_login."_points",true);
		$_points++;
		
		update_user_meta($get_comment->user_id,$user_vote->user_login."_points",$_points);
		add_user_meta($get_comment->user_id,$user_vote->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),(vpanel_options("point_rating_answer") != ""?vpanel_options("point_rating_answer"):1),"-",__("Rating your answer","vbegy"),$post_id,$id));
	
		$points_user = get_user_meta($get_comment->user_id,"points",true);
		update_user_meta($get_comment->user_id,"points",$points_user-(vpanel_options("point_rating_answer") != ""?vpanel_options("point_rating_answer"):1));
	}
	
	$count = get_comment_meta($id,'comment_vote');
	$count = (!empty($count[0]["vote"])?$count[0]["vote"]:0);
	if(!$count)
		$count = 0;
	$count--;
	$update = update_comment_meta($id,'comment_vote',array("vote" => $count,"post_id" => $post_id));
	if($update) {
		setcookie('comment_vote'.$id,"ask_yes_comment",time()+3600*24*365,'/');
	}
	echo $count;
	die();
}
add_action('wp_ajax_comment_vote_down','comment_vote_down');
add_action('wp_ajax_nopriv_comment_vote_down','comment_vote_down');
/* following_not */
function following_not () {
	$following_not_id = (int)$_POST["following_not_id"];
	$get_user_by_following_not_id = get_user_by("id",$following_not_id);
	
	$following_me = get_user_meta(get_current_user_id(),"following_me");
	$points = get_user_meta($get_user_by_following_not_id->ID,"points",true);
	$get_user_by_following_not_id = get_user_by("id",$following_not_id);
	$remove_following_me = remove_item_by_value($following_me[0],$get_user_by_following_not_id->ID);
	update_user_meta(get_current_user_id(),"following_me",$remove_following_me);
	$new_points = $points-1;
	if ($new_points < 0) {
		$new_points = 0;
	}
	update_user_meta($get_user_by_following_not_id->ID,"points",$new_points);
	
	$_points = get_user_meta($get_user_by_following_not_id->ID,$get_user_by_following_not_id->user_login."_points",true);
	$_points++;
	
	update_user_meta($get_user_by_following_not_id->ID,$get_user_by_following_not_id->user_login."_points",$_points);
	add_user_meta($get_user_by_following_not_id->ID,$get_user_by_following_not_id->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),(vpanel_options("point_following_me") != ""?vpanel_options("point_following_me"):1),"-",__("User unfollow You","vbegy"),"",""));
	
	$following_you = get_user_meta($following_not_id,"following_you");
	$get_user_by_following_not_id2 = get_user_by("id",get_current_user_id());
	$remove_following_you = remove_item_by_value($following_you[0],$get_user_by_following_not_id2->ID);
	update_user_meta($following_not_id,"following_you",$remove_following_you);
	
	$echo_following_you = get_user_meta($following_not_id,"following_you");
	echo (isset($echo_following_you[0]) && is_array($echo_following_you[0])?count($echo_following_you[0]):0);
	die();
}
add_action('wp_ajax_following_not','following_not');
add_action('wp_ajax_nopriv_following_not','following_not');
/* following_me */
function following_me () {
	$following_you_id = (int)$_POST["following_you_id"];
	$get_user_by_following_id = get_user_by("id",$following_you_id);
	$get_user_by_following_id2 = get_user_by("id",get_current_user_id());

	$following_me_get = get_user_meta(get_current_user_id(),"following_me");
	if (empty($following_me_get)) {
		update_user_meta(get_current_user_id(),"following_me",array($get_user_by_following_id->ID));
	}else {
		update_user_meta(get_current_user_id(),"following_me",array_merge($following_me_get[0],array($get_user_by_following_id->ID)));
	}
	
	$points_get = get_user_meta($get_user_by_following_id->ID,"points",true);
	if ($points_get == "" or $points_get == 0) {
		update_user_meta($get_user_by_following_id->ID,"points",1);
	}else {
		$new_points = $points_get+1;
		update_user_meta($get_user_by_following_id->ID,"points",$new_points);
	}
	
	$_points = get_user_meta($get_user_by_following_id->ID,$get_user_by_following_id->user_login."_points",true);
	$_points++;
	
	update_user_meta($get_user_by_following_id->ID,$get_user_by_following_id->user_login."_points",$_points);
	add_user_meta($get_user_by_following_id->ID,$get_user_by_following_id->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),(vpanel_options("point_following_me") != ""?vpanel_options("point_following_me"):1),"+",__("User follow You","vbegy"),"",""));

	$following_you_get = get_user_meta($following_you_id,"following_you");
	if (empty($following_you_get)) {
		update_user_meta($following_you_id,"following_you",array($get_user_by_following_id2->ID));
	}else {
		update_user_meta($following_you_id,"following_you",array_merge($following_you_get[0],array($get_user_by_following_id2->ID)));
	}
	
	$echo_following_you = get_user_meta($following_you_id,"following_you");
	echo (isset($echo_following_you[0]) && is_array($echo_following_you[0])?count($echo_following_you[0]):0);
	die();
}
add_action('wp_ajax_following_me','following_me');
add_action('wp_ajax_nopriv_following_me','following_me');
/* add_point */
function add_point () {
	$input_add_point = (int)$_POST["input_add_point"];
	$post_id = (int)$_POST["post_id"];
	$user_id = get_current_user_id();
	$user_name = get_user_by("id",$user_id);
	$points_user = get_user_meta($user_id,"points",true);
	$get_post = get_post($post_id);
	if (get_current_user_id() != $get_post->post_author) {
		_e("Sorry no mistake, this is not a question asked .","vbegy");
	}else if ($points_user >= $input_add_point) {
		if ($input_add_point == "") {
			_e("You must enter a numeric value and a value greater than zero .","vbegy");
		}else if ($input_add_point == 0) {
			_e("You must enter a numeric value and a value greater than zero .","vbegy");
		}else {
			$question_points = get_post_meta($post_id,"question_points",true);
			if ($question_points == 0) {
				$question_points = $input_add_point;
			}else {
				$question_points = $input_add_point+$question_points;
			}
			update_post_meta($post_id,"question_points",$question_points);
			
			$_points = get_user_meta($user_id,$user_name->user_login."_points",true);
			$_points++;
			update_user_meta($user_id,$user_name->user_login."_points",$_points);
			add_user_meta($user_id,$user_name->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),$input_add_point,"-",__("Discount points to bump question","vbegy"),$post_id));
			$points_user = get_user_meta($user_id,"points",true);
			update_user_meta($user_id,"points",$points_user-$input_add_point);
		}
	}else {
		_e("Your points are insufficient .","vbegy");
	}
	die();
}
add_action('wp_ajax_add_point','add_point');
add_action('wp_ajax_nopriv_add_point','add_point');
/* ask_redirect_login */
function ask_redirect_login() {
	if (vpanel_options("login_page") != "") {
		wp_redirect(get_permalink(vpanel_options("login_page")));
	}else {
		wp_redirect(wp_login_url(home_url()));
	}
	exit;
}
/* ask_get_filesize */
if (!function_exists('ask_get_filesize')) {
	function ask_get_filesize( $file ) { 
		$bytes = filesize($file);
		$s = array('b', 'Kb', 'Mb', 'Gb');
		$e = floor(log($bytes)/log(1024));
		return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
	}
}
/* ask_human_time_diff */
if (!function_exists('ask_human_time_diff')) {
	function ask_human_time_diff( $date ) { 
		
		if (strtotime($date)<strtotime('NOW -7 day')) return date('jS F Y', strtotime($date));
		else return human_time_diff(strtotime($date), current_time('timestamp')) . __(' ago ','vbegy');
	}
}
/* sendEmail */
function sendEmail($fromEmail,$fromEmailName,$toEmail,$toEmailName,$subject,$message,$extra='') {
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'To: '.$toEmailName.' <'.$toEmail.'>' . "\r\n";
	$headers .= 'From: '.$fromEmailName.' <'.$fromEmail.'>' . "\r\n";
	if(wp_mail( $toEmail, $subject, $message, $headers )) {
		
	}else {
		@mail($toEmail, $subject, $message, $headers);
	}
}
/* report_q */
function report_q() {
	$post_id = (int)$_POST['post_id'];
	$explain = esc_attr($_POST['explain']);
	$get_post = get_post($post_id);
	
	/* option */
	$ask_option = get_option("ask_option");
	$ask_option_array = get_option("ask_option_array");
	if ($ask_option_array == "") {
		$ask_option_array = array();
	}
	if ($ask_option != "") {
		$ask_option++;
		update_option("ask_option",$ask_option);
		array_push($ask_option_array,$ask_option);
		update_option("ask_option_array",$ask_option_array);
	}else {
		$ask_option = 1;
		add_option("ask_option",$ask_option);
		add_option("ask_option_array",array($ask_option));
	}
	$ask_time = current_time('timestamp');
	/* option */
	if (get_current_user_id() > 0 && is_user_logged_in()) {
		$name_last = "";
	}else {
		$name_last = 1;
	}
	/* add option */
	add_option("ask_option_".$ask_option,array("post_id" => $post_id,"the_date" => $ask_time,"report_new" => 1,"user_id" => (get_current_user_id() != "" or get_current_user_id() != 0?get_current_user_id():""),"the_author" => $name_last,"item_id_option" => $ask_option,"value" => $explain));
	
	$post_mail = "
	".__("Hi there","vbegy")."<br />
	
	".__("Abuse have been reported on the use of the following question","vbegy")."<br />
	
	<a href=".$get_post->guid.">".$get_post->post_title."</a><br />
	
	";
	
	sendEmail(get_bloginfo("admin_email"),get_bloginfo('name'),get_bloginfo("admin_email"),get_bloginfo('name'),__("Hi there","vbegy"),$post_mail);
	die();
}
add_action('wp_ajax_report_q','report_q');
add_action('wp_ajax_nopriv_report_q','report_q');
/* report_c */
function report_c() {
	$comment_id = (int)$_POST['comment_id'];
	$explain = esc_attr($_POST['explain']);
	$get_comment = get_comment($comment_id);
	
	$get_post = get_post($get_comment->comment_post_ID);
	
	/* option */
	$ask_option_answer = get_option("ask_option_answer");
	$ask_option_answer_array = get_option("ask_option_answer_array");
	if ($ask_option_answer_array == "") {
		$ask_option_answer_array = array();
	}
	if ($ask_option_answer != "") {
		$ask_option_answer++;
		update_option("ask_option_answer",$ask_option_answer);
		array_push($ask_option_answer_array,$ask_option_answer);
		update_option("ask_option_answer_array",$ask_option_answer_array);
	}else {
		$ask_option_answer = 1;
		add_option("ask_option_answer",$ask_option_answer);
		add_option("ask_option_answer_array",array($ask_option_answer));
	}
	$ask_time = current_time('timestamp');
	/* option */
	if (get_current_user_id() > 0 && is_user_logged_in()) {
		$name_last = "";
	}else {
		$name_last = 1;
	}
	/* add option */
	add_option("ask_option_answer_".$ask_option_answer,array("post_id" => $get_comment->comment_post_ID,"comment_id" => $comment_id,"the_date" => $ask_time,"report_new" => 1,"user_id" => (get_current_user_id() != "" or get_current_user_id() != 0?get_current_user_id():""),"the_author" => $name_last,"item_id_option" => $ask_option_answer,"value" => $explain));
	$post_mail = "
	".__("Hi there","vbegy")."<br />
	
	".__("Abuse have been reported on the use of the following comment","vbegy")."<br />
	
	<a href=".$get_post->guid.'#comment_'.$comment_id.">".$get_post->post_title."</a><br />
	
	";
	sendEmail(get_bloginfo("admin_email"),get_bloginfo('name'),get_bloginfo("admin_email"),get_bloginfo('name'),__("Hi there","vbegy"),$post_mail);
	die();
}
add_action('wp_ajax_report_c','report_c');
add_action('wp_ajax_nopriv_report_c','report_c');
/* best_answer */
function best_answer() {
	$comment_id = (int)$_POST['comment_id'];
	$get_comment = get_comment($comment_id);
	$user_id = $get_comment->user_id;
	$post_id = $get_comment->comment_post_ID;
	update_post_meta($post_id,"the_best_answer",$comment_id);
	$post_author = get_post($comment_id);
	$user_author = $post_author->post_author;
	if ($user_id != 0) {
		$user_name = get_user_by("id",$user_id);
		
		$_points = get_user_meta($user_id,$user_name->user_login."_points",true);
		$_points++;
		update_user_meta($user_id,$user_name->user_login."_points",$_points);
		add_user_meta($user_id,$user_name->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),(vpanel_options("point_best_answer") != ""?vpanel_options("point_best_answer"):5),"+",__("Choose your answer Best Answer","vbegy"),$post_id,$get_comment->comment_ID));
		$points_user = get_user_meta($user_id,"points",true);
		update_user_meta($user_id,"points",$points_user+(vpanel_options("point_best_answer") != ""?vpanel_options("point_best_answer"):5));
		
		$the_best_answer_u = get_user_meta($user_id,"the_best_answer",true);
		$the_best_answer_u++;
		update_user_meta($user_id,"the_best_answer",$the_best_answer_u);
	}
	$user_name2 = get_user_by("id",$user_author);
	$option_name = "best_answer_option";
	$best_answer_option = get_option($option_name);
	$best_answer_option++;
	update_option($option_name,$best_answer_option);
	die();
}
add_action('wp_ajax_best_answer','best_answer');
add_action('wp_ajax_nopriv_best_answer','best_answer');
/* question_close */
function question_close() {
	$post_id = (int)$_POST['post_id'];
	$post_author = get_post($post_id);
	$user_author = $post_author->post_author;
	if (($user_author != 0 && $user_author == get_current_user_id()) || is_super_admin(get_current_user_id())) {
		echo $post_id." ".$user_author;
		update_post_meta($post_id,'closed_question',1);
	}
	die();
}
add_action('wp_ajax_question_close','question_close');
add_action('wp_ajax_nopriv_question_close','question_close');
/* question_open */
function question_open() {
	$post_id = (int)$_POST['post_id'];
	$post_author = get_post($post_id);
	$user_author = $post_author->post_author;
	if (($user_author != 0 && $user_author == get_current_user_id()) || is_super_admin(get_current_user_id())) {
		echo $post_id." ".$user_author;
		delete_post_meta($post_id,'closed_question');
	}
	die();
}
add_action('wp_ajax_question_open','question_open');
add_action('wp_ajax_nopriv_question_open','question_open');
/* best_answer_remove */
function best_answer_re() {
	$comment_id = (int)$_POST['comment_id'];
	$get_comment = get_comment($comment_id);
	$user_id = $get_comment->user_id;
	$post_id = $get_comment->comment_post_ID;
	delete_post_meta($post_id,"the_best_answer",$comment_id);
	$post_author = get_post($comment_id);
	$user_author = $post_author->post_author;
	if ($user_id != 0) {
		$user_name = get_user_by("id",$user_id);
		
		$_points = get_user_meta($user_id,$user_name->user_login."_points",true);
		$_points++;
		update_user_meta($user_id,$user_name->user_login."_points",$_points);
		add_user_meta($user_id,$user_name->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),(vpanel_options("point_best_answer") != ""?vpanel_options("point_best_answer"):5),"-",__("Cancel your answer Best Answer","vbegy"),$post_id,$get_comment->comment_ID));
		$points_user = get_user_meta($user_id,"points",true);
		update_user_meta($user_id,"points",$points_user-(vpanel_options("point_best_answer") != ""?vpanel_options("point_best_answer"):5));
		
		$the_best_answer_u = get_user_meta($user_id,"the_best_answer",true);
		$the_best_answer_u--;
		update_user_meta($user_id,"the_best_answer",$the_best_answer_u);
	}
	$user_name2 = get_user_by("id",$user_author);
	$option_name = "best_answer_option";
	$best_answer_option = get_option($option_name);
	$best_answer_option--;
	update_option($option_name,$best_answer_option);
	die();
}
add_action('wp_ajax_best_answer_re','best_answer_re');
add_action('wp_ajax_nopriv_best_answer_re','best_answer_re');
/* comment_question_before */
add_filter ('preprocess_comment', 'comment_question_before');
function comment_question_before($commentdata) {
	if (!is_admin()) {
		$the_captcha = vpanel_options("the_captcha");
		$captcha_style = vpanel_options("captcha_style");
		$captcha_question = vpanel_options("captcha_question");
		$captcha_answer = vpanel_options("captcha_answer");
		$show_captcha_answer = vpanel_options("show_captcha_answer");
		if ($the_captcha == 1) {
			if (empty($_POST["ask_captcha"])) {
				wp_die(__("There are required fields ( captcha ).","vbegy"));
			}
			if ($captcha_style == "question_answer") {
				if ($captcha_answer != $_POST["ask_captcha"]) {
					wp_die(__('The captcha is incorrect, please try again.','vbegy'));
				}
			}else {
				if ($_SESSION["security_code"] != $_POST["ask_captcha"]) {
					wp_die(__('The captcha is incorrect, please try again.','vbegy'));
				}
			}
		}
	}
	return $commentdata;
}
/* comment_question */
add_action ('comment_post', 'comment_question');
function comment_question($comment_id) {
	global $post;
	if ($post->post_type == "question") :
		add_comment_meta($comment_id, 'comment_type',"question", true);
		if(isset($_FILES['attachment']) && !empty($_FILES['attachment']['name'])) :
			require_once(ABSPATH . "wp-admin" . '/includes/file.php');					
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$comment_attachment = wp_handle_upload($_FILES['attachment'], array('test_form'=>false), current_time('mysql'));
			if ( isset($comment_attachment['error']) ) :
				// Ohhh Bugger
				wp_die( 'Attachment Error: ' . $comment_attachment['error'] );
				exit;
			endif;
			$comment_attachment_data = array(
				'post_mime_type' => $comment_attachment['type'],
				'post_title' => preg_replace('/\.[^.]+$/', '', basename($comment_attachment['file'])),
				'post_content' => '',
				'post_status' => 'inherit',
				'post_author' => (get_current_user_id() != "" or get_current_user_id() != 0?get_current_user_id():"")
			);
			$comment_attachment_id = wp_insert_attachment( $comment_attachment_data, $comment_attachment['file'], $post->ID );
			$comment_attachment_metadata = wp_generate_attachment_metadata( $comment_attachment_id, $comment_attachment['file'] );
			wp_update_attachment_metadata( $comment_attachment_id,  $comment_attachment_metadata );
			
			add_comment_meta($comment_id, 'added_file',$comment_attachment_id, true);
		
		endif;
		$remember_answer = get_post_meta($post->ID,"remember_answer",true);
		$get_comment = get_comment($comment_id);
		$user_id = $get_comment->user_id;
		
		$get_the_author = get_user_by("id",$post->post_author);
		$the_author = $get_the_author->user_login;

		if ($remember_answer == 1) {
			$the_name = $get_comment->comment_author;
			
			$the_mail = $get_the_author->user_email;
			
			$post_mail = "
			".__("Hi there","vbegy")."<br />
		
			".__("We would tell you","vbegy")." ".$the_author." ".__("That the new post was added on a common theme by","vbegy")." ".$the_name." ".__("Entitled","vbegy")." ".$the_name."  ".$post->post_title."<br />
			
			".__("Click on the link below to go to the topic","vbegy")."<br />
			
			<a href=".$post->guid.">".$post->post_title."</a><br />
			
			".__("There may be more of Posts and we hope the answer to encourage members and get them to help.","vbegy")."<br />
			
			".__("Accept from us Sincerely","vbegy")."<br />
			";
			sendEmail(get_bloginfo("admin_email"),get_bloginfo('name'),$the_mail,$the_author,__("Hi there","vbegy"),$post_mail);
		}
		if ($user_id != 0) {
			$user_vote = get_user_by("id",$user_id);
			if ($user_id != $post->post_author) {
				$_points = get_user_meta($user_id,$user_vote->user_login."_points",true);
				$_points++;
				
				update_user_meta($user_id,$user_vote->user_login."_points",$_points);
				add_user_meta($user_id,$user_vote->user_login."_points_".$_points,array(date_i18n('Y/m/d',current_time('timestamp')),date_i18n('g:i a',current_time('timestamp')),(vpanel_options("point_add_comment") != ""?vpanel_options("point_add_comment"):2),"+",__("You answer the question","vbegy"),$post->ID,$get_comment->comment_ID));
			
				$points_user = get_user_meta($user_id,"points",true);
				update_user_meta($user_id,"points",$points_user+(vpanel_options("point_add_comment") != ""?vpanel_options("point_add_comment"):2));
			}
			
			$add_answer = get_user_meta($user_id,"add_answer_all",true);
			$add_answer_m = get_user_meta($user_id,"add_answer_m_".date_i18n('m_Y',current_time('timestamp')),true);
			$add_answer_d = get_user_meta($user_id,"add_answer_d_".date_i18n('d_m_Y',current_time('timestamp')),true);
			if ($add_answer_d == "" or $add_answer_d == 0) {
				update_user_meta($user_id,"add_answer_d_".date_i18n('d_m_Y',current_time('timestamp')),1);
			}else {
				update_user_meta($user_id,"add_answer_d_".date_i18n('d_m_Y',current_time('timestamp')),$add_answer_d+1);
			}
			
			if ($add_answer_m == "" or $add_answer_m == 0) {
				update_user_meta($user_id,"add_answer_m_".date_i18n('m_Y',current_time('timestamp')),1);
			}else {
				update_user_meta($user_id,"add_answer_m_".date_i18n('m_Y',current_time('timestamp')),$add_answer_m+1);
			}
			
			if ($add_answer == "" or $add_answer == 0) {
				update_user_meta($user_id,"add_answer_all",1);
			}else {
				update_user_meta($user_id,"add_answer_all",$add_answer+1);
			}
	
		}
	endif;
}
/* vbegy_session */
function vbegy_session ($message = ""){
	$question_publish = vpanel_options("question_publish");
	if ($question_publish == "draft") {
		if(!session_id())
			session_start();
		if ($message) {
			$_SESSION['vbegy_session'] = $message;
		}else {
			if (isset($_SESSION['vbegy_session'])) {
				$last_message = $_SESSION['vbegy_session'];
				unset($_SESSION['vbegy_session']);
				echo $last_message;
			}
		}
	}
}
/* new_question */
function new_question() {
	if ($_POST) :
		$return = process_new_questions();
		if ( is_wp_error($return) ) :
   			echo '<div class="ask_error"><span><p>'.$return->get_error_message().'</p></span></div>';
   		else :
   			$question_publish = vpanel_options("question_publish");
   			if ($question_publish == "draft") {
				if(!session_id()) session_start();
				$_SESSION['vbegy_session'] = '<div class="alert-message success"><i class="icon-ok"></i><p><span>'.__("Added been successfully","vbegy").'</span><br>'.__("Has been added successfully, the question under review .","vbegy").'</p></div>';
				wp_redirect(get_bloginfo('url'));
			}else {
				$get_post = get_post($return);
				$send_email_new_question = vpanel_options("send_email_new_question");
				if ($send_email_new_question == 1) {
					$question_category = wp_get_post_terms($get_post->ID,'question-category',array("fields" => "all"));
					$get_question_category = get_option("questions_category_".$question_category[0]->term_id);
					$yes_private = 1;
					if (isset($question_category[0]) && isset($get_question_category['private']) && $get_question_category['private'] == "on") {
						$yes_private = 0;
					}else if (isset($question_category[0]) && !isset($get_question_category['private']) && $question_category[0]->parent == 0) {
						$yes_private = 1;
					}
					if (isset($question_category[0]) && $question_category[0]->parent > 0) {
						$get_question_category_parent = get_option("questions_category_".$question_category[0]->parent);
						if (isset($get_question_category_parent[0]) && isset($get_question_category_parent['private']) && $get_question_category_parent['private'] == "on") {
							$yes_private = 0;
						}
					}
					if ($yes_private == 1) {
						$users = get_users('blog_id=1&orderby=registered');
						$post_mail = "
						".__("Hi there","vbegy")."<br />
						
						".__("There are a new question","vbegy")."<br />
						
						<a href=".$get_post->guid.">".$get_post->post_title."</a><br />
						
						";
						foreach ( $users as $user ) {
							sendEmail(get_bloginfo("admin_email"),get_bloginfo('name'),esc_html($user->user_email),esc_html($user->display_name),__("Hi there","vbegy"),$post_mail);
						}
					}
				}
				wp_redirect(get_permalink($return));
			}
			exit;
   		endif;
	endif;
}
add_action('new_question', 'new_question');
/* process_new_questions */
function process_new_questions() {
	global $posted;
	set_time_limit(0);
	$errors = new WP_Error();
	$posted = array();
	$video_desc_active = vpanel_options("video_desc_active");
	$ask_question_no_register = vpanel_options("ask_question_no_register");
	
	$fields = array(
		'title','comment','category','question_poll','remember_answer','question_tags','video_type','video_id','video_description','attachment','ask_captcha','username','email'
	);
	
	foreach ($fields as $field) :
		if (isset($_POST[$field])) $posted[$field] = trim(stripslashes(htmlspecialchars($_POST[$field]))); else $posted[$field] = '';
	endforeach;
	
	if (!is_user_logged_in() && $ask_question_no_register == 1) {
		if (empty($posted['username'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( username ).","vbegy"));
		if (empty($posted['email'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( email ).","vbegy"));
	}
	
	/* Validate Requried Fields */
	if (empty($posted['title'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( title ).","vbegy"));
	if (empty($posted['category']) || $posted['category'] == '-1') $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( category ).","vbegy"));
	if (isset($posted['question_poll']) && $posted['question_poll'] == 1) {
		foreach($_POST['ask'] as $ask) {
			if (empty($ask['ask']) && count($_POST['ask']) < 2) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("Please enter at least two values in poll.","vbegy"));
		}
	}
	if (empty($posted['comment'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( comment ).","vbegy"));
	if ($video_desc_active == 1 && $posted['video_description'] == 1 && empty($posted['video_id'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( Video ID ).","vbegy"));
	
	$the_captcha = vpanel_options("the_captcha");
	$captcha_style = vpanel_options("captcha_style");
	$captcha_question = vpanel_options("captcha_question");
	$captcha_answer = vpanel_options("captcha_answer");
	$show_captcha_answer = vpanel_options("show_captcha_answer");
	if ($the_captcha == 1) {
		if (empty($posted["ask_captcha"])) {
			$errors->add('required-captcha', __("There are required fields ( captcha ).","vbegy"));
		}
		if ($captcha_style == "question_answer") {
			if ($captcha_answer != $posted["ask_captcha"]) {
				$errors->add('required-captcha-error', __('The captcha is incorrect, please try again.','vbegy'));
			}
		}else {
			if ($_SESSION["security_code"] != $posted["ask_captcha"]) {
				$errors->add('required-captcha-error', __('The captcha is incorrect, please try again.','vbegy'));
			}
		}
	}
	
	if (sizeof($errors->errors)>0) return $errors;
	
	/* Create question */
	$question_publish = vpanel_options("question_publish");
	$data = array(
		'post_content' => esc_attr($posted['comment']),
		'post_title' => esc_attr($posted['title']),
		'post_status' => ($question_publish == "draft"?"draft":"publish"),
		'post_author' => (!is_user_logged_in() && $ask_question_no_register == 1?0:get_current_user_id()),
		'post_type' => 'question',
	);		
		
	$question_id = wp_insert_post($data);		
		
	if ($question_id==0 || is_wp_error($question_id)) wp_die(__("Error in question.","vbegy"));

	$terms = array();
	if ($posted['category']) $terms[] = get_term_by( 'id', $posted['category'], 'question-category')->slug;
	if (sizeof($terms)>0) wp_set_object_terms($question_id, $terms, 'question-category');

	if ($posted['question_poll'])  {
		update_post_meta($question_id, 'question_poll', $posted['question_poll']);
	}else {
		update_post_meta($question_id, 'question_poll', 2);
	}

	if ($_POST['ask']) 
		update_post_meta($question_id, 'ask', $_POST['ask']);
	
	if ($posted['remember_answer']) 
		update_post_meta($question_id, 'remember_answer', $posted['remember_answer']);
	
	if ($video_desc_active == 1) {
		if ($posted['video_description']) 
			update_post_meta($question_id, 'video_description', $posted['video_description']);
		
		if ($posted['video_type']) 
			update_post_meta($question_id, 'video_type', $posted['video_type']);
			
		if ($posted['video_id']) 
			update_post_meta($question_id, 'video_id', $posted['video_id']);	
	}
	
	$attachment = '';

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
		
	if(isset($_FILES['attachment']) && !empty($_FILES['attachment']['name'])) :
			
		$attachment = wp_handle_upload($_FILES['attachment'], array('test_form'=>false), current_time('mysql'));
					
		if ( isset($attachment['error']) ) :
			$errors->add('upload-error', __("Attachment Error: ","vbegy") . $attachment['error'] );
			
			return $errors;
		endif;
		
	endif;
	if ($attachment) :
		$attachment_data = array(
			'post_mime_type' => $attachment['type'],
			'post_title' => preg_replace('/\.[^.]+$/', '', basename($attachment['file'])),
			'post_content' => '',
			'post_status' => 'inherit',
			'post_author' => (!is_user_logged_in() && $ask_question_no_register == 1?0:get_current_user_id())
		);
		$attachment_id = wp_insert_attachment( $attachment_data, $attachment['file'], $question_id );
		$attachment_metadata = wp_generate_attachment_metadata( $attachment_id, $attachment['file'] );
		wp_update_attachment_metadata( $attachment_id,  $attachment_metadata );
		//set_post_thumbnail( $question_id, $attachment_id );
		add_post_meta($question_id, 'added_file',$attachment_id, true);
	
	endif;
	
	/* Tags */
	
	if (isset($posted['question_tags']) && $posted['question_tags']) :
				
		$tags = explode(',', trim(stripslashes($posted['question_tags'])));
		$tags = array_map('strtolower', $tags);
		$tags = array_map('trim', $tags);

		if (sizeof($tags)>0) :
			wp_set_object_terms($question_id, $tags, 'question_tags');
		endif;
		
	endif;
	
	if (!is_user_logged_in() && $ask_question_no_register == 1 && get_current_user_id() == 0) {
		$question_username = esc_attr($posted['username']);
		$question_email = esc_attr($posted['email']);
		update_post_meta($question_id, 'question_username', $question_username);
		update_post_meta($question_id, 'question_email', $question_email);
	}else {
		
		$user_id = get_current_user_id();
		
		$add_questions = get_user_meta($user_id,"add_questions_all",true);
		$add_questions_m = get_user_meta($user_id,"add_questions_m_".date_i18n('m_Y',current_time('timestamp')),true);
		$add_questions_d = get_user_meta($user_id,"add_questions_d_".date_i18n('d_m_Y',current_time('timestamp')),true);
		if ($add_questions_d == "" or $add_questions_d == 0) {
			update_user_meta($user_id,"add_questions_d_".date_i18n('d_m_Y',current_time('timestamp')),1);
		}else {
			update_user_meta($user_id,"add_questions_d_".date_i18n('d_m_Y',current_time('timestamp')),$add_questions_d+1);
		}
		
		if ($add_questions_m == "" or $add_questions_m == 0) {
			update_user_meta($user_id,"add_questions_m_".date_i18n('m_Y',current_time('timestamp')),1);
		}else {
			update_user_meta($user_id,"add_questions_m_".date_i18n('m_Y',current_time('timestamp')),$add_questions_m+1);
		}
		
		if ($add_questions == "" or $add_questions == 0) {
			update_user_meta($user_id,"add_questions_all",1);
		}else {
			update_user_meta($user_id,"add_questions_all",$add_questions+1);
		}
	}

	do_action('new_questions', $question_id);
	
	/* Successful */
	return $question_id;
}
/* edit_question */
function edit_question() {
	if ($_POST) :
		$return = process_edit_questions();
		if ( is_wp_error($return) ) :
   			echo '<div class="ask_error"><span><p>'.$return->get_error_message().'</p></span></div>';
   		else :
   			if(!session_id()) session_start();
   			$_SESSION['vbegy_session_e'] = '<div class="alert-message success"><i class="icon-ok"></i><p><span>'.__("Edited been successfully","vbegy").'</span><br>'.__("Has been edited successfully .","vbegy").'</p></div>';
			wp_redirect(get_permalink($return));
			exit;
   		endif;
	endif;
}
add_action('edit_question', 'edit_question');
/* process_edit_questions */
function process_edit_questions() {
	global $posted;
	set_time_limit(0);
	$errors = new WP_Error();
	$posted = array();
	$video_desc_active = vpanel_options("video_desc_active");
	
	$fields = array(
		'ID','title','comment','category','question_poll','remember_answer','question_tags','video_type','video_id','video_description'
	);
	
	foreach ($fields as $field) :
		if (isset($_POST[$field])) $posted[$field] = trim(stripslashes(htmlspecialchars($_POST[$field]))); else $posted[$field] = '';
	endforeach;
	/* Validate Requried Fields */
	
	$get_question = (isset($posted['ID'])?(int)$posted['ID']:0);
	$get_post_q = get_post($get_question);
	if (isset($get_question) && $get_question != 0 && $get_post_q && get_post_type($get_question) == "question") {
		$user_login_id_l = get_user_by("id",$get_post_q->post_author);
		if ($user_login_id_l->ID != get_current_user_id()) {
			$errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("Sorry you can't edit this question .","vbegy"));
		}
	}else {
		$errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("Sorry no question select or not found .","vbegy"));
	}
	if (empty($posted['title'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( title ).","vbegy"));
	if (empty($posted['category']) || $posted['category'] == '-1') $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( category ).","vbegy"));
	if (isset($posted['question_poll']) && $posted['question_poll'] == 1) {
		foreach($_POST['ask'] as $ask) {
			if (empty($ask['ask']) && count($_POST['ask']) < 2) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("Please enter at least two values in poll.","vbegy"));
		}
	}
	if (empty($posted['comment'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( comment ).","vbegy"));
	if ($video_desc_active == 1 && $posted['video_description'] == 1 && empty($posted['video_id'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields ( Video ID ).","vbegy"));
	if (sizeof($errors->errors)>0) return $errors;
	
	$question_id = $get_question;
	
	/* Create question */
	$question_publish = vpanel_options("question_publish");
	$data = array(
		'ID' => esc_attr($question_id),
		'post_content' => esc_attr($posted['comment']),
		'post_title' => esc_attr($posted['title']),
	);		
		
	wp_update_post($data);
	
	$terms = array();
	if ($posted['category']) $terms[] = get_term_by( 'id', $posted['category'], 'question-category')->slug;
	if (sizeof($terms)>0) wp_set_object_terms($question_id, $terms, 'question-category');

	if ($posted['question_poll'] && $posted['question_poll'] != "")  {
		update_post_meta($question_id, 'question_poll', $posted['question_poll']);
	}else {
		update_post_meta($question_id, 'question_poll', 2);
	}

	if ($_POST['ask']) 
		update_post_meta($question_id, 'ask', $_POST['ask']);
	
	if ($posted['remember_answer'] && $posted['remember_answer'] != "") {
		update_post_meta($question_id, 'remember_answer', $posted['remember_answer']);
	}else {
		delete_post_meta($question_id, 'remember_answer');
	}
	
	if ($video_desc_active == 1) {
		if ($posted['video_description'] && $posted['video_description'] != "") {
			update_post_meta($question_id, 'video_description', $posted['video_description']);
		}else {
			delete_post_meta($question_id, 'video_description');
		}
		
		if ($posted['video_type']) 
			update_post_meta($question_id, 'video_type', $posted['video_type']);
			
		if ($posted['video_id']) 
			update_post_meta($question_id, 'video_id', $posted['video_id']);	
	}
	
	/* Tags */
	
	if (isset($posted['question_tags']) && $posted['question_tags']) :
				
		$tags = explode(',', trim(stripslashes($posted['question_tags'])));
		$tags = array_map('strtolower', $tags);
		$tags = array_map('trim', $tags);

		if (sizeof($tags)>0) :
			wp_set_object_terms($question_id, $tags, 'question_tags');
		endif;
		
	endif;

	do_action('edit_questions', $question_id);
	
	/* Successful */
	return $question_id;
}
/* vbegy_session_edit */
function vbegy_session_edit ($message = ""){
	if(!session_id())
		session_start();
	if ($message) {
		$_SESSION['vbegy_session_e'] = $message;
	}else {
		if (isset($_SESSION['vbegy_session_e'])) {
			$last_message = $_SESSION['vbegy_session_e'];
			unset($_SESSION['vbegy_session_e']);
			echo $last_message;
		}
	}
}
/* vbegy_session_activate */
function vbegy_session_activate ($message = ""){
	if(!session_id())
		session_start();
	if ($message) {
		$_SESSION['vbegy_session_a'] = $message;
	}else {
		if (isset($_SESSION['vbegy_session_a'])) {
			$last_message = $_SESSION['vbegy_session_a'];
			unset($_SESSION['vbegy_session_a']);
			echo $last_message;
		}
	}
}
/* ask_process_edit_profile_form */
function ask_process_edit_profile_form() {
	global $posted;
	require_once(ABSPATH . 'wp-admin/includes/user.php');
	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	$errors = new WP_Error();
	$posted = array(
		'email' => esc_html($_POST['email']),
		'pass1' => esc_html($_POST['pass1']),
		'pass2' => esc_html($_POST['pass2']),
	);
	
	if (empty($posted['email'])) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("There are required fields.","vbegy"));
	if ($posted['pass1'] !== $posted['pass2']) $errors->add('required-field', '<strong>'.__("Error","vbegy").' :&nbsp;</strong> '.__("Password does not match.","vbegy"));
	
	if (sizeof($errors->errors)>0) return $errors;
	
	$current_user = wp_get_current_user();
	isset($_POST['admin_bar_front'] ) ? 'true' : 'false';
	$get_you_avatar = get_user_meta(get_current_user_id(),"you_avatar",true);
	$errors = edit_user(get_current_user_id());
	if ( is_wp_error( $errors ) ) return $errors;
	do_action('personal_options_update', get_current_user_id());
	
	if(isset($_FILES['you_avatar']) && !empty($_FILES['you_avatar']['name'])) :
		$you_avatar = wp_handle_upload($_FILES['you_avatar'], array('test_form'=>false), current_time('mysql'));
		if ($you_avatar) :
			update_user_meta(get_current_user_id(),"you_avatar",$you_avatar["url"]);
		endif;
		if ( isset($you_avatar['error']) ) :
			$errors->add('upload-error',  __('Error in upload the image : ','vbegy') . $you_avatar['error'] );
			return $errors;
		endif;
	else:
		update_user_meta(get_current_user_id(),"you_avatar",$get_you_avatar);
	endif;
	
	return;
}
/* ask_edit_profile_form */
function ask_edit_profile_form() {
	if ($_POST) :
		$return = ask_process_edit_profile_form();
		if ( is_wp_error($return) ) :
   			echo '<div class="ask_error"><span><p>'.$return->get_error_message().'</p></span></div>';
   		else :
   			echo '<div class="ask_done"><span><p>'.__("Profile has been updated","vbegy").'</p></span></div>';
   		endif;
	endif;
}
add_action('ask_edit_profile_form', 'ask_edit_profile_form');
/* add_favorite */
function add_favorite() {
	$post_id = (int)$_POST['post_id'];
	$user_login_id2 = get_user_by("id",get_current_user_id());
	
	$_favorites = get_user_meta(get_current_user_id(),$user_login_id2->user_login."_favorites");
	if (is_array($_favorites[0])) {
		if (!in_array($post_id,$_favorites[0])) {
			$array_merge = array_merge($_favorites[0],array($post_id));
			update_user_meta(get_current_user_id(),$user_login_id2->user_login."_favorites",$array_merge);
		}
	}else {
		update_user_meta(get_current_user_id(),$user_login_id2->user_login."_favorites",array($post_id));
	}
	
	$count = get_post_meta($post_id,'question_favorites',true);
	if(!$count)
		$count = 0;
	$count++;
	$update = update_post_meta($post_id,'question_favorites',$count);
	
	die();
}
add_action('wp_ajax_add_favorite','add_favorite');
add_action('wp_ajax_nopriv_add_favorite','add_favorite');
/* remove_favorite */
function remove_favorite() {
	$post_id = (int)$_POST['post_id'];
	$user_login_id2 = get_user_by("id",get_current_user_id());
	
	$_favorites = get_user_meta(get_current_user_id(),$user_login_id2->user_login."_favorites");
	if (is_array($_favorites[0])) {
		if (in_array($post_id,$_favorites[0])) {
			$remove_item = remove_item_by_value($_favorites[0],$post_id);
			update_user_meta(get_current_user_id(),$user_login_id2->user_login."_favorites",$remove_item);
		}
	}
	
	$count = get_post_meta($post_id,'question_favorites',true);
	if(!$count)
		$count = 0;
	$count--;
	if($count < 0)
		$count = 0;
	$update = update_post_meta($post_id,'question_favorites',$count);
	die();
}
add_action('wp_ajax_remove_favorite','remove_favorite');
add_action('wp_ajax_nopriv_remove_favorite','remove_favorite');
/* remove_item_by_value */
function remove_item_by_value($array, $val = '', $preserve_keys = true) {
	if (empty($array) || !is_array($array)) return false;
	if (!in_array($val, $array)) return $array;
	
	foreach($array as $key => $value) {
		if ($value == $val) unset($array[$key]);
	}
	
	return ($preserve_keys === true) ? $array : array_values($array);
}
/* question_comments */
function question_comments($comment, $args, $depth) {
	global $post;
	$GLOBALS['comment'] = $comment; 
		$comment_vote = get_comment_meta($comment->comment_ID,'comment_vote');
		$comment_vote = (!empty($comment_vote)?$comment_vote[0]["vote"]:"");
		$the_best_answer = get_post_meta($post->ID,"the_best_answer",true);
		if ($comment->user_id != 0){
			$user_login_id_l = get_user_by("id",$comment->user_id);
			$user_profile_page = add_query_arg("u", $user_login_id_l->user_login,get_page_link(vpanel_options('user_profile_page')));
		}
		
		?>
		<div 
		<?php if($the_best_answer == $comment->comment_ID) {?>
		style="border:1px solid <?php echo (vpanel_options("border_best") != ""?vpanel_options("border_best"):"#F00");
		echo (vpanel_options("background_best") != ""?";background:".vpanel_options("background_best"):"");
		?>" 
		<?php }?>
		class="comments_in <?php echo ($the_best_answer == $comment->comment_ID?"the_best_answer":"")?> " rel="posts-<?php echo get_the_ID()?>" id="comment_<?php echo $comment->comment_ID?>">
			<div class="comment_avt">
				<?php if ($comment->user_id != 0){ ?>
					<a class="tooltip_n" original-title="<?php the_author();?>" href="<?php echo $user_profile_page?>"><?php if (get_the_author_meta('you_avatar',$comment->user_id) != "") { ?>
					<img alt="" src="<?php echo esc_attr(get_the_author_meta('you_avatar',$comment->user_id));?>" />
					<?php }else {echo get_avatar($comment->user_id,75);}?></a>
				<?php }else {
					echo get_avatar($comment->comment_author_email,75);
				}?>
			</div><!-- End comment_avt -->
			<div class="comments_top">
				<div class="comment_top_r">
					<div class="comment_author">
						<?php if ($comment->user_id != 0){ ?><a href="<?php echo $user_profile_page?>"><?php }?>
							<?php echo $comment->comment_author?>
						<?php if ($comment->user_id != 0){ ?></a><?php }?>
					</div>
					<div class="comment_date"><?php echo get_comment_date('Y/m/d'); ?></div>
					<div class="comment_time"><?php echo get_comment_date('g:i a'); ?></div>
					<div class="loader"></div>
					<a class="question_r_l comment_l report_c" href="#" title="<?php _e("Report abuse","vbegy")?>"></a>
				</div><!-- End comment_top_r -->
				<div class="clear"></div>
			</div><!-- End comments_top -->
			<div class="clear"></div>
			<div class="question_bottom_r" id="comment-<?php echo $comment->comment_ID?>">
				<div class="no_vote_more"></div>
				<div class="question_vote_result english <?php echo ($comment_vote < 0?"question_vote_red":"")?>"><?php echo ($comment_vote != ""?$comment_vote:0)?></div>
				<?php if (is_user_logged_in()){?>
					<div class="loader"></div>
					<a href="#" class="tooltip_s comment_vote_up<?php echo (isset($_COOKIE['comment_vote'.$comment->comment_ID])?" ".$_COOKIE['comment_vote'.$comment->comment_ID]."-".$comment->comment_ID:"")?>" title="<?php _e("Like","vbegy");?>" id="comment_vote_up-<?php echo $comment->comment_ID?>"></a>
					<a href="#" class="tooltip_s comment_vote_down<?php echo (isset($_COOKIE['comment_vote'.$comment->comment_ID])?" ".$_COOKIE['comment_vote'.$comment->comment_ID]."-".$comment->comment_ID:"")?>" id="comment_vote_down-<?php echo $comment->comment_ID?>" title="<?php _e("Dislike","vbegy");?>"></a>
				<?php }else { ?>
					<div class="loader"></div>
					<a href="#" class="comment_vote_up vote_not_user" title="<?php _e("Like","vbegy");?>"></a>
					<a href="#" class="comment_vote_down vote_not_user" title="<?php _e("Dislike","vbegy");?>"></a>
				<?php }?>
			</div><!-- End question_bottom_r -->
			<div class="question_bottom_l comment_comment">
				<?php echo apply_filters('the_content', $comment->comment_content);?>
				<?php $added_file = get_comment_meta($comment->comment_ID,'added_file', true);
				if ($added_file != "") {
					echo "<div class='clear'></div><a class='attachment_c' href='".wp_get_attachment_url($added_file)."'>".__("Attachment","vbegy")."</a>";
				}
				?>

				<div class="clear"></div>
				<?php
				if ($the_best_answer == $comment->comment_ID) {
					echo '
					<a class="commentform best_answer_re button" title="'.__("Cancel the best answer","vbegy").'" href="#">'.__("Cancel the best answer","vbegy").'</a>
					<span class="commentform button">'.__("Best answer","vbegy").' </span>
					<div class="loader"></div>
					';
				}
				if (is_user_logged_in() and get_current_user_id() == $post->post_author and ($the_best_answer == 0 or $the_best_answer == "")){?>
					<div class="loader"></div>
					<a class="commentform best_answer_a button" title="<?php _e("Select as best answer","vbegy");?>" href="#"><?php _e("Select as best answer","vbegy");?></a>
				<?php
				}
				?>
				<div class="reply"><?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></div><!-- End reply -->
			</div><!-- End question_bottom_l -->
			<div class="clear"></div>
		</div><!-- End comments_in -->
	<?php
}
/* excerpt_row */
function excerpt_row($excerpt_length,$content) {
	global $post;
	$words = explode(' ', $content, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, '...');
		$content = implode(' ', $words);
	endif;
		$content = strip_tags($content);
	echo $content;
}
/* excerpt_title_row */
function excerpt_title_row($excerpt_length,$title) {
	global $post;
	$words = explode(' ', $title, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, '');
		$title = implode(' ', $words);
	endif;
		$title = strip_tags($title);
	echo $title;
}