<?php ob_start();?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> xmlns:fb="http://ogp.me/ns/fb#">
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<?php if (vpanel_options("v_responsive") == 1) {?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php }?>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<title><?php
	global $page, $paged;
	wp_title('|', true, 'right');
	bloginfo('name');
	$site_description = get_bloginfo('description', 'display');
	if ($site_description && ( is_home() || is_front_page() ))
	echo " | $site_description";
	if ($paged >= 2 || $page >= 2)
	echo ' | ' . sprintf(__('Page %s','vbegy'), max($paged, $page));?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php wp_head();?>
</head>
<body <?php body_class();?>>
	<div class="background-cover"></div>
	<?php
	$vbegy_layout = "";
	if (is_single() || is_page()) {
		$vbegy_layout = rwmb_meta('vbegy_layout','radio',$post->ID);
	}
	
	$cat_layout = "";
	if (is_category()) {
		$category_id = get_query_var('cat');
		$cat_layout = vpanel_options('cat_layout'.$category_id);
	}else if (is_tax("question-category")) {
		$tax_id = get_term_by('slug',get_query_var('term'),"question-category");
		$tax_id = $tax_id->term_id;
		$cat_layout = vpanel_options('cat_layout'.$tax_id);
	}
	$home_layout = vpanel_options("home_layout");
	$top_panel_skin = vpanel_options("top_panel_skin");
	$header_skin = vpanel_options("header_skin");
	$header_fixed = vpanel_options("header_fixed");
	$author_layout = vpanel_options("author_layout");
	if (is_author() && $author_layout != "default") {
		$home_layout = $author_layout;
	}
	?>
	<div class="loader"><div class="loader_html"></div></div>
	
	<div class="panel-pop" id="signup">
		<h2><?php _e("Register Now","vbegy");?><i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<?php echo do_shortcode("[ask_signup]");?>
		</div>
	</div><!-- End signup -->
	
	<div class="panel-pop" id="login-comments">
		<h2><?php _e("Login","vbegy");?><i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<?php echo do_shortcode("[ask_login]");?>
		</div>
	</div><!-- End login-comments -->
	
	<div class="panel-pop panel-pop-ask" id="ask-question">
		<h2><?php _e("Add question","vbegy");?><i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<?php echo do_shortcode("[ask_question]");?>
		</div>
	</div><!-- End ask-question -->
	
	<div class="panel-pop" id="lost-password">
		<h2><?php _e("Lost Password","vbegy");?><i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<p><?php _e("Lost your password? Please enter your username and email address. You will receive a link to create a new password via email.","vbegy");?></p>
			<?php echo do_shortcode("[ask_lost_pass]");?>
			<div class="clearfix"></div>
		</div>
	</div><!-- End lost-password -->
	<?php
	if (is_author()) {
		$grid_template = vpanel_options("author_template");
	}else if (is_single() || is_page()) {
		$grid_template = rwmb_meta('vbegy_home_template','radio',$post->ID);
	}else if (is_category()) {
		$grid_template = vpanel_options("cat_template".$category_id);
	}else if (is_tax("question-category")) {
		$grid_template = vpanel_options("cat_template".$tax_id);
	}else {
		$grid_template = vpanel_options("home_template");
	}
	
	if ((is_author() && $grid_template == "default") || ((is_single() || is_page()) && $grid_template == "default") || (is_category() && $grid_template == "default") || (is_tax("question-category") && $grid_template == "default")) {
		$grid_template = vpanel_options("home_template");
	}
	?>
	<div id="wrap" class="<?php echo $grid_template." ";if ($header_fixed == 1) {echo "fixed-enabled ";}if (is_category() && $cat_layout != "default") {if ($cat_layout == "fixed") {echo "boxed ";}else if ($cat_layout == "fixed_2") {echo "boxed2 ";}}else if (is_tax("question-category") && $cat_layout != "default") {if ($cat_layout == "fixed") {echo "boxed ";}else if ($cat_layout == "fixed_2") {echo "boxed2 ";}}else if ((is_single() || is_page()) && $vbegy_layout != "default") {if ($vbegy_layout == "fixed") {echo "boxed ";}else if ($vbegy_layout == "fixed_2") {echo "boxed2 ";}}else {if ($home_layout == "fixed") {echo "boxed ";}else if ($home_layout == "fixed_2") {echo "boxed2 ";}}?>">
		
		<?php $login_panel = vpanel_options("login_panel");
		$top_menu = vpanel_options("top_menu");
		if ($login_panel == 1 && $top_menu == 1) {?>
			<div class="login-panel <?php if ($top_panel_skin == "panel_light") {echo "panel_light";}else {echo "panel_dark";}?>">
				<section class="container">
					<div class="row">
						<?php if (is_user_logged_in()) {?>
							<div class="col-md-12">
								<div class="page-content">
									<?php echo is_user_logged_in_data()?>
								</div><!-- End page-content -->
							</div><!-- End col-md-12 -->
						<?php }else {?>
							<div class="col-md-6">
								<div class="page-content">
									<h2><?php _e("Login","vbegy")?></h2>
									<div class="form-style form-style-3">
										<?php echo do_shortcode("[ask_login]");?>
									</div>
								</div><!-- End page-content -->
							</div><!-- End col-md-6 -->
							<div class="col-md-6">
								<div class="page-content Register">
									<h2><?php _e("Register Now","vbegy")?></h2>
									<p><?php echo vpanel_options("register_content")?></p>
									<a class="button color small signup"><?php _e("Create an account","vbegy")?></a>
								</div><!-- End page-content -->
							</div><!-- End col-md-6 -->
						<?php }?>
					</div>
				</section>
			</div><!-- End login-panel -->
		<?php }
		
		if ($top_menu) {?>
			<div id="header-top">
				<section class="container clearfix">
					<nav class="header-top-nav">
						<?php 
						if (is_user_logged_in()) {
							wp_nav_menu(array('container_class' => 'header-top','menu_class' => '','theme_location' => 'top_bar_login','fallback_cb' => 'vpanel_nav_fallback'));
						}else {
							wp_nav_menu(array('container_class' => 'header-top','menu_class' => '','theme_location' => 'top_bar','fallback_cb' => 'vpanel_nav_fallback'));
						}?>
					</nav>
					<div class="header-search">
						<form method="get" action="<?php echo home_url('/'); ?>">
						    <input type="text" value="<?php if (get_search_query() != "") {echo the_search_query();}else {_e("Search here ...","vbegy");}?>" onfocus="if(this.value=='<?php _e("Search here ...","vbegy");?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e("Search here ...","vbegy");?>';" name="s">
						    <button type="submit" class="search-submit"></button>
						</form>
					</div>
				</section><!-- End container -->
			</div><!-- End header-top -->
		<?php }
		if (is_page_template("templet-home.php")) {
			$index_top_box = rwmb_meta('vbegy_index_top_box','checkbox',$post->ID);
		}else {
			$index_top_box = vpanel_options('index_top_box');
		}
		$breadcrumbs = vpanel_options("breadcrumbs");
		$logo_position = vpanel_options("logo_position");?>
		<header id="header" class='<?php if ($header_skin == "header_light") {echo "header_light ";}
		if (is_front_page() || is_home()) {
			if ($index_top_box != 1) {
				echo "index-no-box ";
			}
		}else {
			if ($breadcrumbs != 1) {
				echo "index-no-box ";
			}
		}
		if ($logo_position == "right_logo") {echo "header_2 ";}else if ($logo_position == "center_logo") {echo "header_3 ";}?>'>
			<section class="container clearfix">
				<div class="logo">
					<?php
					$logo_display = vpanel_options("logo_display");
					$logo_img = vpanel_options("logo_img");
					if ($logo_display == "custom_image") {?>
					    <a class="logo-img" href="<?php echo esc_url(home_url('/'));?>" itemprop="url" title="<?php echo esc_attr(get_bloginfo('name','display'))?>"><img itemprop="logo" alt="" src="<?php echo $logo_img?>"></a>
					<?php }else {?>
						<h2><a href="<?php echo esc_url(home_url('/'));?>" itemprop="url" title="<?php echo esc_attr(get_bloginfo('name','display'))?>"><?php bloginfo('name');?></a></h2>
					<?php }?>
					<meta itemprop="name" content="<?php bloginfo('name'); ?>">
				</div>
				<nav class="navigation">
					<?php wp_nav_menu(array('container_class' => 'header-menu','menu_class' => '','theme_location' => 'header_menu','fallback_cb' => 'vpanel_nav_fallback'));?>
				</nav>
			</section><!-- End container -->
		</header><!-- End header -->
		<?php if (is_page_template("templet-home.php") || is_front_page()) {
			if (is_page_template("templet-home.php")) {
				$index_top_box_layout = rwmb_meta('vbegy_index_top_box_layout','radio',$post->ID);
				$index_about = rwmb_meta('vbegy_index_about','text',$post->ID);
				$index_about_h = rwmb_meta('vbegy_index_about_h','text',$post->ID);
				$index_join = rwmb_meta('vbegy_index_join','text',$post->ID);
				$index_join_h = rwmb_meta('vbegy_index_join_h','text',$post->ID);
				$index_about_login = rwmb_meta('vbegy_index_about_login','text',$post->ID);
				$index_about_h_login = rwmb_meta('vbegy_index_about_h_login','text',$post->ID);
				$index_join_login = rwmb_meta('vbegy_index_join_login','text',$post->ID);
				$index_join_h_login = rwmb_meta('vbegy_index_join_h_login','text',$post->ID);
				$index_title = rwmb_meta('vbegy_index_title','text',$post->ID);
				$index_content = rwmb_meta('vbegy_index_content','textarea',$post->ID);
			}else {
				$index_top_box_layout = vpanel_options('index_top_box_layout');
				$index_about = vpanel_options('index_about');
				$index_about_h = vpanel_options('index_about_h');
				$index_join = vpanel_options('index_join');
				$index_join_h = vpanel_options('index_join_h');
				$index_about_login = vpanel_options('index_about_login');
				$index_about_h_login = vpanel_options('index_about_h_login');
				$index_join_login = vpanel_options('index_join_login');
				$index_join_h_login = vpanel_options('index_join_h_login');
				$index_title = vpanel_options("index_title");
				$index_content = vpanel_options("index_content");
			}
			if ($index_top_box == 1) {?>
				<div class="section-warp ask-me">
					<div class="container clearfix">
						<div class="box_icon box_warp box_no_border box_no_background">
							<div class="row">
								<?php if ($index_top_box_layout == 2) {?>
									<div class="col-md-12">
										<h2><?php echo $index_title;?></h2>
										<p><?php echo $index_content;?></p>
										<div class="clearfix"></div>
										<?php if (is_user_logged_in()) {
											if ($index_about_login != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_about_h_login?>"><?php echo $index_about_login?></a>
											<?php }
											if ($index_join_login != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_join_h_login?>"><?php echo $index_join_login?></a>
											<?php }
										}else {
											if ($index_about != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_about_h?>"><?php echo $index_about?></a>
											<?php }
											if ($index_join != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_join_h?>"><?php echo $index_join?></a>
											<?php }
										}?>
										<div class="clearfix"></div>
										<form class="form-style form-style-2">
											<p>
												<input type="text" id="question_title" value="<?php _e("Ask any question and you be sure find your answer ?","vbegy");?>" onfocus="if(this.value=='<?php _e("Ask any question and you be sure find your answer ?","vbegy");?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e("Ask any question and you be sure find your answer ?","vbegy");?>';">
												<i class="icon-pencil"></i>
												<span class="color button small publish-question"><?php _e("Ask Now","vbegy");?></span>
											</p>
										</form>
									</div>
								<?php }else {?>
									<div class="col-md-3">
										<h2><?php echo $index_title;?></h2>
										<p><?php echo $index_content;?></p>
										<div class="clearfix"></div>
										<?php if (is_user_logged_in()) {
											if ($index_about_login != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_about_h_login?>"><?php echo $index_about_login?></a>
											<?php }
											if ($index_join_login != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_join_h_login?>"><?php echo $index_join_login?></a>
											<?php }
										}else {
											if ($index_about != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_about_h?>"><?php echo $index_about?></a>
											<?php }
											if ($index_join != "") {?>
												<a class="color button dark_button medium" href="<?php echo $index_join_h?>"><?php echo $index_join?></a>
											<?php }
										}?>
									</div>
									<div class="col-md-9">
										<form class="form-style form-style-2">
											<p>
												<textarea rows="4" id="question_title" onfocus="if(this.value=='<?php _e("Ask any question and you be sure find your answer ?","vbegy");?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e("Ask any question and you be sure find your answer ?","vbegy");?>';"><?php _e("Ask any question and you be sure find your answer ?","vbegy");?></textarea>
												<i class="icon-pencil"></i>
												<span class="color button small publish-question"><?php _e("Ask Now","vbegy");?></span>
											</p>
										</form>
									</div>
								<?php }?>
							</div><!-- End row -->
						</div><!-- End box_icon -->
					</div><!-- End container -->
				</div><!-- End section-warp -->
			<?php
			}
		}else {
			if ($breadcrumbs == 1) {
				breadcrumbs();
			}
		}
		$sidebar_layout = vpanel_options('sidebar_layout');
		if ($sidebar_layout == 'left') {
			$sidebar_dir = 'page-left-sidebar';
			$homepage_content_span = 'col-md-9';
		}elseif ($sidebar_layout == 'full') {
			$sidebar_dir = 'page-full-width';
			$homepage_content_span = 'col-md-12';
		}else {
			$sidebar_dir = 'page-right-sidebar';
			$homepage_content_span = 'col-md-9';
		}
		
		if (is_single() || is_page()) {
			$sidebar_post = rwmb_meta('vbegy_sidebar','radio',$post->ID);
			$sidebar_dir = '';
			if (isset($sidebar_post) && $sidebar_post != "default" && $sidebar_post != "") {
				if ($sidebar_post == 'left') {
					$sidebar_dir = 'page-left-sidebar';
					$homepage_content_span = 'col-md-9';
				}elseif ($sidebar_post == 'full') {
					$sidebar_dir = 'page-full-width';
					$homepage_content_span = 'col-md-12';
				}else {
					$sidebar_dir = 'page-right-sidebar';
					$homepage_content_span = 'col-md-9';
				}
			}else {
				$sidebar_dir = 'page-right-sidebar';
				$homepage_content_span = 'col-md-9';
			}
		}
		if (is_author()) {
			$sidebar_layout = vpanel_options('author_sidebar_layout');
			if ($sidebar_layout == 'left') {
				$sidebar_dir = 'page-left-sidebar';
				$homepage_content_span = 'col-md-9';
			}elseif ($sidebar_layout == 'full') {
				$sidebar_dir = 'page-full-width';
				$homepage_content_span = 'col-md-12';
			}else {
				$sidebar_dir = 'page-right-sidebar';
				$homepage_content_span = 'col-md-9';
			}
		}else if (is_category()) {
			$sidebar_layout = vpanel_options('cat_sidebar_layout'.$category_id);
			if ($sidebar_layout == 'left') {
				$sidebar_dir = 'page-left-sidebar';
				$homepage_content_span = 'col-md-9';
			}elseif ($sidebar_layout == 'full') {
				$sidebar_dir = 'page-full-width';
				$homepage_content_span = 'col-md-12';
			}else {
				$sidebar_dir = 'page-right-sidebar';
				$homepage_content_span = 'col-md-9';
			}
		}else if (is_tax("question-category")) {
			$sidebar_layout = vpanel_options('cat_sidebar_layout'.$tax_id);
			if ($sidebar_layout == 'left') {
				$sidebar_dir = 'page-left-sidebar';
				$homepage_content_span = 'col-md-9';
			}elseif ($sidebar_layout == 'full') {
				$sidebar_dir = 'page-full-width';
				$homepage_content_span = 'col-md-12';
			}else {
				$sidebar_dir = 'page-right-sidebar';
				$homepage_content_span = 'col-md-9';
			}
		}
		
		$vbegy_header_adv_type = rwmb_meta('vbegy_header_adv_type','radio',$post->ID);
		$vbegy_header_adv_code = rwmb_meta('vbegy_header_adv_code','textarea',$post->ID);
		$vbegy_header_adv_href = rwmb_meta('vbegy_header_adv_href','text',$post->ID);
		$vbegy_header_adv_img = rwmb_meta('vbegy_header_adv_img','upload',$post->ID);
		
		if ((is_single() || is_page()) && (($vbegy_header_adv_type == "display_code" && $vbegy_header_adv_code != "") || ($vbegy_header_adv_type == "custom_image" && $vbegy_header_adv_img != ""))) {
			$header_adv_type = $vbegy_header_adv_type;
			$header_adv_code = $vbegy_header_adv_code;
			$header_adv_href = $vbegy_header_adv_href;
			$header_adv_img = $vbegy_header_adv_img;
		}else {
			$header_adv_type = vpanel_options("header_adv_type");
			$header_adv_code = vpanel_options("header_adv_code");
			$header_adv_href = vpanel_options("header_adv_href");
			$header_adv_img = vpanel_options("header_adv_img");
		}
		if (($header_adv_type == "display_code" && $header_adv_code != "") || ($header_adv_type == "custom_image" && $header_adv_img != "")) {
			echo '<div class="clearfix"></div>
			<div class="advertising">';
			if ($header_adv_type == "display_code") {
				echo stripcslashes($header_adv_code);
			}else {
				if ($header_adv_href != "") {
					echo '<a href="'.$header_adv_href.'">';
				}
				echo '<img alt="" src="'.$header_adv_img.'">';
				if ($header_adv_href != "") {
					echo '</a>';
				}
			}
			echo '</div><!-- End advertising -->
			<div class="clearfix"></div>';
		}
		?>
		
		<section class="container main-content <?php echo (!is_404()?$sidebar_dir:"page-full-width");?>">
			<?php $question_publish = vpanel_options("question_publish");
			if ($question_publish == "draft") {
				vbegy_session();
			}
			vbegy_session_edit();
			vbegy_session_activate();?>
			<div class="row">
				<div class="<?php echo (!is_404()?$homepage_content_span:"col-md-12");?>">
				<?php $confirm_email = vpanel_options("confirm_email");
				//echo get_current_user_id()." ".get_user_meta(get_current_user_id(),"activation",true);
				if (is_user_logged_in() && $confirm_email == 1) {
					$if_user_id = get_user_by("id",get_current_user_id());
					if (isset($if_user_id->caps["activation"]) && $if_user_id->caps["activation"] == 1) {
						$get_user_a = (isset($_GET['u'])?esc_attr($_GET['u']):"");
						$get_activate = (isset($_GET['activate'])?esc_attr($_GET['activate']):"");
						if (isset($_GET['u']) && isset($_GET['activate'])) {
							$activation = get_user_meta(get_current_user_id(),"activation",true);
							if ($activation == $get_activate) {
								wp_update_user( array ('ID' => get_current_user_id(), 'role' => 'subscriber') ) ;
								delete_user_meta(get_current_user_id(),"activation");
								if(!session_id()) session_start();
								$_SESSION['vbegy_session_a'] = '<div class="alert-message success"><i class="icon-ok"></i><p><span>'.__("Activate the membership","vbegy").'</span><br>'.__("Your membership is activate now .","vbegy").'</p></div>';
								wp_safe_redirect(get_bloginfo(url));
							}else {
								echo '<div class="alert-message error"><i class="icon-ok"></i><p><span>'.sprintf(__("Activate the membership","vbegy").'</span><br>'.__("Please activate your membership, Click on the link has been sent to email .","vbegy"),"ssss").'</p></div>';
							}
						}else if (!isset($_GET['u']) && !isset($_GET['activate']) && !isset($_SESSION['vbegy_session_a'])) {
							if (isset($_GET['get_activate']) && $_GET['get_activate'] == "do") {
								$user_data = get_user_by("id",get_current_user_id());
								$rand_a = rand(1,1000000000000);
								update_user_meta(get_current_user_id(),"activation",$rand_a);
								$post_mail = "
								".__("Hi there","vbegy")."<br />
								
								".__("This is the link to activate your membership","vbegy")."<br />
								
								<a href=".add_query_arg(array("u" => get_current_user_id(),"activate" => $rand_a),esc_url(home_url('/'))).">".__("Activate","vbegy")."</a><br />
								
								";
								sendEmail(get_bloginfo("admin_email"),get_bloginfo('name'),esc_html($user_data->user_email),esc_html($user_data->display_name),__("Hi there","vbegy"),$post_mail);
								$_SESSION['vbegy_session_a'] = '<div class="alert-message success"><i class="icon-ok"></i><p><span>'.__("Activate the membership","vbegy").'</span><br>'.__("Check your email again .","vbegy").'</p></div>';
								wp_safe_redirect(get_bloginfo(url));
							}else {
								echo '<div class="alert-message error"><i class="icon-ok"></i><p><span>'.sprintf(__("Activate the membership","vbegy").'</span><br>'.__("Please activate your membership, Click on the link has been sent to email, If you lost it <a href='%s'>Click here</a> to send it again .","vbegy"),add_query_arg(array("get_activate" => "do"),esc_url(home_url('/')))).'</p></div>';
							}
						}
						get_footer();
						die();
					}
				}?>