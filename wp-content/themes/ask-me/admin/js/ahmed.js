jQuery(document).ready( function($) {
	    jQuery("input.vpanel_save").click(function(){
    	jQuery("#ajax-saving").fadeIn("slow");
    	jQuery("#loading").show();
    	if (jQuery(".vpanel_editor").length > 0) {
    		tinyMCE.triggerSave();
    	}
    	var options_fromform = jQuery('#main_options_form').serialize();
    	var options_fromform2 = options_fromform+"&action=update_options";
    	jQuery.post(ajax_a,options_fromform2,function (data) {
    		jQuery(".vpanel_save").blur();
    		//jQuery("body").prepend(data);
    		setTimeout(function(){
    			jQuery("#ajax-saving").fadeOut("slow");
    			jQuery("#loading").hide();
    		},200);
    		if (data == 2) {
    			jQuery("#import_setting").val("");
    			location.reload();
    		}
    	});
    	return false;
    });
	
    jQuery("#reset_c").click(function(){
		jQuery("#ajax-reset").fadeIn("slow");
		var defaults = "&action=reset_options";
		jQuery.post(ajax_a,defaults,function (data) {
			jQuery("#reset_c").blur();
			setTimeout(function(){
				jQuery("#ajax-reset").fadeOut("slow");
				//jQuery("body").prepend(data);
				location.reload();
			},200);
		});
    	return false;
    });
    
    /* Delete reports */
    jQuery(".reports-delete").click(function () {
    	var answer = confirm("If you press will delete report !");
    	if (answer) {
    		var reports_delete = jQuery(this);
    		var reports_delete_id = reports_delete.attr("attr");
    		if (reports_delete.hasClass("reports-answers")) {
	    		jQuery.post(ajax_a,"action=reports_answers_delete&reports_delete_id="+reports_delete_id,function (result) {
	    			reports_delete.parent().parent().addClass('removered').fadeOut(function() {
	    				jQuery(this).remove();
	    				if (jQuery(".reports-table-items .reports-table-item").length == 0) {
	    					jQuery(".reports-table-items").html("<p>There are no reports yet</p>");
	    				}
	    				if (jQuery(".ask-reports-items .ask-reports").length == 0) {
	    					jQuery(".ask-reports-items").html("<p>There are no reports yet</p>");
	    				}
	    			});
	    		});
	    	}else {
	    		jQuery.post(ajax_a,"action=reports_delete&reports_delete_id="+reports_delete_id,function (result) {
	    			reports_delete.parent().parent().addClass('removered').fadeOut(function() {
	    				jQuery(this).remove();
	    				if (jQuery(".reports-table-items .reports-table-item").length == 0) {
	    					jQuery(".reports-table-items").html("<p>There are no reports yet</p>");
	    				}
	    				if (jQuery(".ask-reports-items .ask-reports").length == 0) {
	    					jQuery(".ask-reports-items").html("<p>There are no reports yet</p>");
	    				}
	    			});
	    		});
	    	}
    	}
    	return false;
    });
    
    /* View reports */
    jQuery(".reports-view").click(function () {
    	var reports_view = jQuery(this);
    	var reports_view_attr = "#reports-"+reports_view.attr("attr");
    	jQuery(reports_view_attr).slideDown();
    	
    	jQuery("body").prepend("<div class='reports-hidden'></div>");
    	wrap_pop();
    	var count_report_new = jQuery(".wp-submenu-head .count_lasts").text();
    	var count_report_new_last = count_report_new-1;
    	if (reports_view.hasClass("reports-answers")) {
	    	jQuery.post(ajax_a,"action=reports_answers_view&reports_view_id="+reports_view.attr("attr"),function (result) {
	    		reports_view.parent().find(".reports-new").hide();
	    		
	    		var count_report_answer_new = jQuery(".count_report_answer_new").text();
	    		var count_report_answer_new_last = count_report_answer_new-1;
	    		if (count_report_new > 0 && reports_view.parent().find(".reports-new").length > 0) {
	    			jQuery(".count_lasts").text(count_report_new_last);
	    			jQuery(".count_lasts").removeClass("count-"+count_report_new).addClass("count-"+count_report_new_last);
	    			
	    			jQuery(".count_report_answer_new").text(count_report_answer_new_last);
	    			jQuery(".count_report_answer_new").removeClass("count-"+count_report_answer_new).addClass("count-"+count_report_answer_new_last);
	    		}
	    	});
    	}else {
    		jQuery.post(ajax_a,"action=reports_view&reports_view_id="+reports_view.attr("attr"),function (result) {
    			reports_view.parent().find(".reports-new").hide();
    			
    			var count_report_question_new = jQuery(".count_report_question_new").text();
    			var count_report_question_new_last = count_report_question_new-1;
    			if (count_report_new > 0 && reports_view.parent().find(".reports-new").length > 0) {
    				jQuery(".count_lasts").text(count_report_new_last);
    				jQuery(".count_lasts").removeClass("count-"+count_report_new).addClass("count-"+count_report_new_last);
    				
    				jQuery(".count_report_question_new").text(count_report_question_new_last);
    				jQuery(".count_report_question_new").removeClass("count-"+count_report_question_new).addClass("count-"+count_report_question_new_last);
    			}
    		});
    	}
    	return false;
    });
    
    /* Close reports */
    jQuery(".reports-close").click(function () {
    	jQuery(".reports-pop").animate({"top":"-50%"},500).hide(function () {
    		jQuery(this).animate({"top":"-50%"},500);
    	});
    	jQuery(".reports-hidden").remove();
    	return false;
    });
    
    /* Function pop */
    function wrap_pop() {
    	jQuery(".reports-hidden").click(function () {
    		jQuery(".reports-pop").slideUp();
    		jQuery(this).remove();
    	});
    }
    
    
	jQuery(".vpanel_checkbox:checkbox,#vbegy_page_builder:checkbox,#vbegy_review_display,#vbegy_background_full,#vbegy_top_menu,#vbegy_header_adv,#vbegy_main_menu,#vbegy_social_icon_h,#vbegy_rss_icon_h,#vbegy_news_ticker,#vbegy_carousel_h,#header_fixed,#vpanel_question_poll,#vpanel_remember_answer,#vbegy_index_top_box,#vbegy_index_tabs,#vbegy_about_widget,#vbegy_social,#vbegy_rss,#vpanel_video_description").checkbox({cls:'vpanel_checkbox'});
	
    jQuery(".vpanel_multicheck:checkbox,.rwmb-checkbox-list:checkbox").checkbox({cls:'vpanel_multicheck'});
    
    jQuery('input[name="vbegy_sidebar"][value="right"]').checkbox({cls:'jquery-sidebar-right'});
    jQuery('input[name="vbegy_sidebar"][value="full"]').checkbox({cls:'jquery-sidebar-full'});
    jQuery('input[name="vbegy_sidebar"][value="left"]').checkbox({cls:'jquery-sidebar-left'});
    jQuery('input[name="vbegy_sidebar"][value="default"],input[name="vbegy_layout"][value="default"],input[name="vbegy_site_skin_l"][value="default"],input[name="vbegy_home_template"][value="default"]').checkbox({cls:'jquery-sidebar-default'});
    
    jQuery('input[name="vbegy_layout"][value="full"]').checkbox({cls:'jquery-layout-full'});
    jQuery('input[name="vbegy_layout"][value="fixed"]').checkbox({cls:'jquery-layout-fixed'});
    jQuery('input[name="vbegy_layout"][value="fixed_2"]').checkbox({cls:'jquery-layout-fixed_2'});
    
    jQuery('input[name="vbegy_home_template"][value="grid_1200"]').checkbox({cls:'jquery-grid_1200'});
    jQuery('input[name="vbegy_home_template"][value="grid_970"]').checkbox({cls:'jquery-grid_970'});
    jQuery('input[name="vbegy_site_skin_l"][value="site_light"]').checkbox({cls:'jquery-site_light'});
    jQuery('input[name="vbegy_site_skin_l"][value="site_dark"]').checkbox({cls:'jquery-site_dark'});
    
    jQuery('input[name="vbegy_skin"][value="default_color"]').checkbox({cls:'jquery-skin-default_color'});
    jQuery('input[name="vbegy_skin"][value="default"]').checkbox({cls:'jquery-skin-default'});
    jQuery('input[name="vbegy_skin"][value="green"]').checkbox({cls:'jquery-skin-green'});
    jQuery('input[name="vbegy_skin"][value="gray"]').checkbox({cls:'jquery-skin-gray'});
    jQuery('input[name="vbegy_skin"][value="moderate_cyan"]').checkbox({cls:'jquery-skin-moderate_cyan'});
    jQuery('input[name="vbegy_skin"][value="orange"]').checkbox({cls:'jquery-skin-orange'});
    jQuery('input[name="vbegy_skin"][value="purple"]').checkbox({cls:'jquery-skin-purple'});
    jQuery('input[name="vbegy_skin"][value="blue"]').checkbox({cls:'jquery-skin-blue'});
    jQuery('input[name="vbegy_skin"][value="yellow"]').checkbox({cls:'jquery-skin-yellow'});
    jQuery('input[name="vbegy_skin"][value="strong_cyan"]').checkbox({cls:'jquery-skin-strong_cyan'});
    jQuery('input[name="vbegy_skin"][value="red"]').checkbox({cls:'jquery-skin-red'});
    
    
    jQuery('input[name="vbegy_footer_layout"][value="footer_1c"]').checkbox({cls:'jquery-footer_1c'});
    jQuery('input[name="vbegy_footer_layout"][value="footer_2c"]').checkbox({cls:'jquery-footer_2c'});
    jQuery('input[name="vbegy_footer_layout"][value="footer_3c"]').checkbox({cls:'jquery-footer_3c'});
    jQuery('input[name="vbegy_footer_layout"][value="footer_4c"]').checkbox({cls:'jquery-footer_4c'});
    jQuery('input[name="vbegy_footer_layout"][value="footer_no"]').checkbox({cls:'jquery-footer_no'});
    
    jQuery('.tooltip_n').tipsy({gravity: 'n'});
    jQuery('.tooltip_s').tipsy({gravity: 's'});
    
});