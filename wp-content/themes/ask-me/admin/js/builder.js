jQuery(function() {
	function uploaded_image() {
		jQuery(".adv-label").each(function () {
			var adv_label = jQuery(this);
			if (jQuery("input[type='radio']:checked",adv_label).val() == "custom_image") {
				jQuery(".image-url",adv_label.parent()).show(10);
				jQuery(".adv-url",adv_label.parent()).show(10);
				jQuery(".adv-code",adv_label.parent()).hide(10);
			}else if (jQuery("input[type='radio']:checked",adv_label).val() == "display_code") {
				jQuery(".image-url",adv_label.parent()).hide(10);
				jQuery(".adv-url",adv_label.parent()).hide(10);
				jQuery(".adv-code",adv_label.parent()).show(10);
			}
			jQuery("input[type='radio']",adv_label).click(function () {
				if (jQuery(this).val() == "custom_image") {
					jQuery(".image-url",jQuery(this).parent().parent()).slideDown(500);
					jQuery(".adv-url",jQuery(this).parent().parent()).slideDown(500);
					jQuery(".adv-code",jQuery(this).parent().parent()).slideUp(500);
				}else if (jQuery(this).val() == "display_code") {
					jQuery(".image-url",jQuery(this).parent().parent()).slideUp(500);
					jQuery(".adv-url",jQuery(this).parent().parent()).slideUp(500);
					jQuery(".adv-code",jQuery(this).parent().parent()).slideDown(500);
				}
			});
		});
	}
	
    jQuery("#expand-all .expand-all2").live("click" ,function () {
    	jQuery(".widget-content").slideUp(300);
    	jQuery(".builder-toggle-close").css("display","none");
    	jQuery(".builder-toggle-open").css("display","block");
    	jQuery(".expand-all").css("display","block");
    	jQuery(".expand-all2").css("display","none");
    });
    
    jQuery("#sidebar_add").click(function() {
		var sidebar_name = jQuery('#sidebar_name').val();
		if (sidebar_name != "" ) {
			if( sidebar_name.length > 0){
				jQuery('#sidebars_list').append('<li><div class="widget-head">'+sidebar_name+' <input id="sidebars" name="sidebars[]" type="hidden" value="'+sidebar_name+'"><a class="del-builder-item del-sidebar-item">x</a></div></li>');
			}
		}else {
			alert("Please write the name !");
		}
		jQuery('#sidebar_name').val("");

	});
    
	var categories_select = jQuery('#categories_select').html();
	jQuery(".add-item").live("click" , function() {
		var builder_item = jQuery(this).attr("add-item");
		if (builder_item == "add_slide") {
			jQuery('#vbegy_slideshow_post ul').append('<li id="builder_slide_'+ builder_slide_j +'" class="ui-state-default"><div class="widget-head text"><span class="vpanel'+ builder_slide_j +'">Slide item - '+ builder_slide_j +'</span><a class="builder-toggle-open" style="display:none">+</a><a class="builder-toggle-close" style="display:block">-</a></div><div class="widget-content" style="display:block"><label for="builder_slide_item['+ builder_slide_j +'][image_url]"><span>Image URL :</span><input id="builder_slide_item['+ builder_slide_j +'][image_url]" name="builder_slide_item['+ builder_slide_j +'][image_url]" placeholder="No file chosen" type="text" class="upload upload_image_'+ builder_slide_j +'"><input class="upload_image_button button upload-button-2" rel="'+ builder_slide_j +'" type="button" value="Upload"><input type="hidden" class="image_id" name="builder_slide_item['+ builder_slide_j +'][image_id]" value=""><div class="clear"></div></label><label for="builder_slide_item['+ builder_slide_j +'][slide_link]"><span>Slide Link :</span><input id="builder_slide_item['+ builder_slide_j +'][slide_link]" name="builder_slide_item['+ builder_slide_j +'][slide_link]" value="#" type="text"></label></div><a class="del-builder-item">x</a></li>');
		}
		if (builder_item == "add_slide") {
			jQuery('#builder_slide_'+ builder_slide_j).hide().fadeIn();
			builder_slide_j ++ ;
		}
		jQuery('.tooltip_s').tipsy({gravity: 's'});
		return false;
	});
	
	
	jQuery(".del-builder-item").live("click" , function() {
		if (jQuery(this).hasClass("del-sidebar-item")) {
			jQuery(this).parent().parent().addClass('removered').fadeOut(function() {
				jQuery(this).remove();
			});
		}else {
			jQuery(this).parent().addClass('removered').fadeOut(function() {
				jQuery(this).remove();
			});
		}
		return false;
	});
	
	uploaded_image();
	
	jQuery( "#question_poll_item" ).sortable({placeholder: "ui-state-highlight"});
	
	jQuery("#upload_add_ask").click(function() {
		jQuery('#question_poll_item').append('<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="widget-content option-item"><div class="rwmb-input"><input id="ask['+ nextCell +'][title]" class="ask" name="ask['+ nextCell +'][title]" value="" type="text"><input id="ask['+ nextCell +'][value]" name="ask['+ nextCell +'][value]" value="" type="hidden"><input id="ask['+ nextCell +'][id]" name="ask['+ nextCell +'][id]" value="'+ nextCell +'" type="hidden"><a class="del-cat">x</a></div></div></li>');
		nextCell ++ ;
		return false;
	});
	
	jQuery(".del-cat").live("click" , function() {
		jQuery(this).parent().parent().addClass('removered').fadeOut(function() {
			jQuery(this).remove();
		});
	});

	var question_poll = jQuery("#vpanel_question_poll:checked").length;
	if (question_poll == 1) {
		jQuery(".vpanel_poll_options").slideDown(500);
	}else {
		jQuery(".vpanel_poll_options").slideUp(500);
	}
	
	jQuery("#vpanel_question_poll").click(function() {
		var vpanel_question_poll = jQuery("#vpanel_question_poll:checked").length;
		if (vpanel_question_poll == 1) {
			jQuery(".vpanel_poll_options").slideDown(500);
		}else {
			jQuery(".vpanel_poll_options").slideUp(500);
		}
	});
	
});