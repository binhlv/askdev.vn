<?php
/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.WordPress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => 'One',
		'two' => 'Two',
		'three' => 'Three',
		'four' => 'Four',
		'five' => 'Five'
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => 'French Toast',
		'two' => 'Pancake',
		'three' => 'Omelette',
		'four' => 'Crepe',
		'five' => 'Waffle'
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all the question category into an array
	$options_categories_q = array();
	$args = array(
		'type'                     => 'question',
		'child_of'                 => 0,
		'parent'                   => '',
		'orderby'                  => 'name',
		'order'                    => 'ASC',
		'hide_empty'               => 0,
		'hierarchical'             => 1,
		'exclude'                  => '',
		'include'                  => '',
		'number'                   => '',
		'taxonomy'                 => 'question-category',
		'pad_counts'               => false );
	
	$options_categories_obj_q = get_categories($args);
	$options_categories_q = array();
	foreach ($options_categories_obj_q as $category_q) {
		$options_categories_q[$category_q->term_id] = $category_q->name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}
	
	// Pull all the sidebars into an array
	$sidebars = get_option('sidebars');
	$new_sidebars = array('default'=> 'Default');
	foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
		$new_sidebars[$sidebar['id']] = $sidebar['name'];
	}
	
	$export = array("vpanel_".vpanel_name,"sidebars");
	$current_options = array();
	foreach ($export as $options) {
		if (get_option($options)) {
			$current_options[$options] = get_option($options);
		}else {
			$current_options[$options] = array();
		}
	}
	$current_options_e = serialize($current_options);
	
	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/admin/images/';

	$options = array();
	
	$options[] = array(
		'name' => 'General settings',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Header code",
		'desc' => "Past your Google analytics code in the box",
		'id' => 'head_code',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => "Footer code",
		'desc' => "Paste footer code in the box",
		'id' => 'footer_code',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => "Custom CSS code",
		'desc' => "Advanced CSS options , Paste your CSS code in the box",
		'id' => 'custom_css',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => 'Enable responsive layout',
		'desc' => 'Select on to enable responsive layout .',
		'id' => 'v_responsive',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Enable SEO options',
		'desc' => 'Select on to enable SEO options .',
		'id' => 'seo_active',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "SEO keywords",
		'desc' => "Paste your keywords in the box",
		'id' => 'the_keywords',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "WordPress login logo",
		'desc' => "This is the logo that appears on the default WordPress login page",
		'id' => 'login_logo',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom favicon",
		'desc' => "Upload the site’s favicon here , You can create new favicon here favicon.cc",
		'id' => 'favicon',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom favicon for iPhone",
		'desc' => "Upload your custom iPhone favicon",
		'id' => 'iphone_icon',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom iPhone retina favicon",
		'desc' => "Upload your custom iPhone retina favicon",
		'id' => 'iphone_icon_retina',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom favicon for iPad",
		'desc' => "Upload your custom iPad favicon",
		'id' => 'ipad_icon',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom iPad retina favicon",
		'desc' => "Upload your custom iPad retina favicon",
		'id' => 'ipad_icon_retina',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Header settings',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Top panel settings',
		'desc' => 'Select on to enable the top panel .',
		'id' => 'login_panel',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Select top panel skin",
		'desc' => "Select your preferred skin for the top panel .",
		'id' => "top_panel_skin",
		'std' => "panel_dark",
		'type' => "images",
		'options' => array(
			'panel_dark' => $imagepath . 'panel_dark.jpg',
			'panel_light' => $imagepath . 'panel_light.jpg'
		)
	);
	
	$options[] = array(
		'name' => 'Header top menu settings',
		'desc' => 'Select on to enable the top menu in the header .',
		'id' => 'top_menu',
		'std' => 1,
		'type' => 'checkbox');
	
	if (is_rtl()) {
		$options[] = array(
			'name' => "Logo position",
			'desc' => "Select where you would like your logo to appear .",
			'id' => "logo_position",
			'std' => "left_logo",
			'type' => "images",
			'options' => array(
				'left_logo' => $imagepath . 'right_logo.jpg',
				'right_logo' => $imagepath . 'left_logo.jpg',
				'center_logo' => $imagepath . 'center_logo.jpg'
			)
		);
	}else {
		$options[] = array(
			'name' => "Logo position",
			'desc' => "Select where you would like your logo to appear .",
			'id' => "logo_position",
			'std' => "left_logo",
			'type' => "images",
			'options' => array(
				'left_logo' => $imagepath . 'left_logo.jpg',
				'right_logo' => $imagepath . 'right_logo.jpg',
				'center_logo' => $imagepath . 'center_logo.jpg'
			)
		);
	}
	
	$options[] = array(
		'name' => "Header skin",
		'desc' => "Select your preferred header skin .",
		'id' => "header_skin",
		'std' => "header_dark",
		'type' => "images",
		'options' => array(
			'header_dark' => $imagepath . 'left_logo.jpg',
			'header_light' => $imagepath . 'header_light.jpg'
		)
	);
	
	$options[] = array(
		'name' => 'Fixed header option',
		'desc' => 'Select on to enable fixed header .',
		'id' => 'header_fixed',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Logo display',
		'desc' => 'choose Logo display .',
		'id' => 'logo_display',
		'std' => 'display_title',
		'type' => 'radio',
		'options' => array("display_title" => "Display site title","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Logo upload',
		'desc' => 'Upload your custom logo. ',
		'id' => 'logo_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Breadcrumbs settings',
		'desc' => 'Select on to enable breadcrumbs .',
		'id' => 'breadcrumbs',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Home page',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Home top box settings',
		'desc' => 'Select on if you want to enable the home top box .',
		'id' => 'index_top_box',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Home top box layout',
		'desc' => 'Home top box layout .',
		'id' => 'index_top_box_layout',
		'std' => '1',
		'type' => 'radio',
		'options' => array("1" => "Style 1","2" => "Style 2"));
	
	$options[] = array(
		'name' => 'Home top box title',
		'desc' => 'Put the Home top box title .',
		'id' => 'index_title',
		'std' => 'Welcome to Ask me',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Home top box content',
		'desc' => 'Put the Home top box content .',
		'id' => 'index_content',
		'std' => 'Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque.',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'About Us title',
		'desc' => 'Put the About Us title .',
		'id' => 'index_about',
		'std' => 'About Us',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'About Us link',
		'desc' => 'Put the About Us link .',
		'id' => 'index_about_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Join Now title',
		'desc' => 'Put the Join Now title .',
		'id' => 'index_join',
		'std' => 'Join Now',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Join Now link',
		'desc' => 'Put the Join Now link .',
		'id' => 'index_join_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'About Us title if logged in',
		'desc' => 'Put the About Us title if logged in .',
		'id' => 'index_about_login',
		'std' => 'About Us',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'About Us link if login',
		'desc' => 'Put the About Us link if logged in .',
		'id' => 'index_about_h_login',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Ask question title if logged in',
		'desc' => 'Put the Ask question title if logged in .',
		'id' => 'index_join_login',
		'std' => 'Ask question',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Ask question link if logged in',
		'desc' => 'Put the Ask question link if logged in .',
		'id' => 'index_join_h_login',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Go to the page and add new page template <a href="post-new.php?post_type=page">from here</a> , choose the template page ( Home ) set it a static page <a href="options-reading.php">from here</a> .',
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Questions',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Any one can ask question without register',
		'desc' => 'Any one can ask question without register enable or disable .',
		'id' => 'ask_question_no_register',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Add question page",
		'desc' => "Create a page using the Add question template and select it here",
		'id' => 'add_question',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "Edit question page",
		'desc' => "Create a page using the Edit question template and select it here",
		'id' => 'edit_question',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => 'Attachment in add question form',
		'desc' => 'Select on to enable the attachment in add question form .',
		'id' => 'attachment_question',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Attachment in a new answer form',
		'desc' => 'Select on to enable the attachment in a new answer form .',
		'id' => 'attachment_answer',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Number of recently answered questions',
		'desc' => 'Set how many questions should be displayed on the recently answered page, This is shown on the recently answered template .',
		'id' => 'recently_answered_num',
		'std' => '30',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Charge points for questions',
		'desc' => 'How many points should be taken from the user’s account for asking questions .',
		'id' => 'question_points',
		'std' => '5',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Charge points for questions settings',
		'desc' => 'Select on if you want to charge points from users for asking questions .',
		'id' => 'question_points_active',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Choose question status',
		'desc' => 'Choose question status after user publish the question .',
		'id' => 'question_publish',
		'options' => array("publish" => "Publish","draft" => "Draft"),
		'std' => 'publish',
		'type' => 'select');
	
	$options[] = array(
		'name' => 'Video description settings',
		'desc' => 'Select on if you want to let users to add video with their question .',
		'id' => 'video_desc_active',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'video description position',
		'desc' => 'Choose the video description position .',
		'id' => 'video_desc',
		'options' => array("before" => "Before content","after" => "After content"),
		'std' => 'after',
		'type' => 'select');
	
	$options[] = array(
		'name' => 'Send email for all the user to notified a new question',
		'desc' => 'Send email enable or disable .',
		'id' => 'send_email_new_question',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'User setting',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Confirm with email enable or disable ( in register form )',
		'desc' => 'Confirm with email enable or disable .',
		'id' => 'confirm_email',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Captcha enable or disable ( in ask question form and register form )',
		'desc' => 'Captcha enable or disable .',
		'id' => 'the_captcha',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Captcha style",
		'desc' => "Choose the captcha style",
		'id' => 'captcha_style',
		'std' => 'question_answer',
		'type' => 'radio',
		'options' => 
			array(
				"question_answer" => "Question and answer",
				"normal_captcha" => "Normal captcha"
		)
	);
	
	$options[] = array(
		'name' => 'Captcha answer enable or disable in form ( in ask question form and register form )',
		'desc' => 'Captcha answer enable or disable .',
		'id' => 'show_captcha_answer',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Captcha question",
		'desc' => "put the Captcha question",
		'id' => 'captcha_question',
		'type' => 'text',
		'std' => "What is the capital of Egypt ?");
	
	$options[] = array(
		'name' => "Captcha answer",
		'desc' => "put the Captcha answer",
		'id' => 'captcha_answer',
		'type' => 'text',
		'std' => "Cairo");
	
	$options[] = array(
		'name' => 'Add profile picture in register form',
		'desc' => 'Add profile picture in register form .',
		'id' => 'profile_picture',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Profile picture in register form is required',
		'desc' => 'Profile picture in register form is required .',
		'id' => 'profile_picture_required',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Register content',
		'desc' => 'Put the register content in top panel and register page .',
		'id' => 'register_content',
		'std' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravdio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequa. Vivamus vulputate posuere nisl quis consequat.',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'Show the points , favorite question , authors i follow , followers , question follow and answer follow for any one',
		'desc' => 'Show this pages only for me or any one  ( private pages or not ) .',
		'id' => 'show_point_favorite',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Login and register page",
		'desc' => "Select the Login and register page",
		'id' => 'login_register_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User edit profile page",
		'desc' => "Select the User edit profile page",
		'id' => 'user_edit_profile_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User post page",
		'desc' => "Select User post page",
		'id' => 'post_user_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User question page",
		'desc' => "Select User question page",
		'id' => 'question_user_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User answer page",
		'desc' => "Select User answer page",
		'id' => 'answer_user_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User favorite question page",
		'desc' => "Select User favorite question page",
		'id' => 'favorite_user_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User point page",
		'desc' => "Select User point page",
		'id' => 'point_user_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "Authors I Follow page",
		'desc' => "Select Authors I Follow page",
		'id' => 'i_follow_user_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User Followers page",
		'desc' => "Select User Followers page",
		'id' => 'followers_user_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User follow question page",
		'desc' => "Select User follow question page",
		'id' => 'follow_question_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "User follow answer page",
		'desc' => "Select User follow answer page",
		'id' => 'follow_answer_page',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => "Points choose best answer",
		'desc' => "put the Points choose best answer",
		'id' => 'point_best_answer',
		'type' => 'text',
		'std' => 5);
	
	$options[] = array(
		'name' => "Points add comment",
		'desc' => "put the Points add comment",
		'id' => 'point_add_comment',
		'type' => 'text',
		'std' => 2);
	
	$options[] = array(
		'name' => "Points Rating answer",
		'desc' => "put the Points Rating answer",
		'id' => 'point_rating_answer',
		'type' => 'text',
		'std' => 1);
	
	$options[] = array(
		'name' => "Points following user",
		'desc' => "put the Points following user",
		'id' => 'point_following_me',
		'type' => 'text',
		'std' => 1);
	
	$options[] = array(
		'name' => 'Author Styling',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Author sidebar layout",
		'desc' => "Author sidebar layout .",
		'id' => "author_sidebar_layout",
		'std' => "default",
		'type' => "images",
		'options' => array(
			'default' => $imagepath . 'sidebar_default.jpg',
			'right' => $imagepath . 'sidebar_right.jpg',
			'full' => $imagepath . 'sidebar_no.jpg',
			'left' => $imagepath . 'sidebar_left.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Author Page Sidebar",
		'desc' => "Author Page Sidebar .",
		'id' => "author_sidebar",
		'std' => '',
		'options' => $new_sidebars,
		'type' => 'select');
	
	$options[] = array(
		'name' => "Author page layout",
		'desc' => "Author page layout .",
		'id' => "author_layout",
		'std' => "full",
		'type' => "images",
		'options' => array(
			'default' => $imagepath . 'sidebar_default.jpg',
			'full' => $imagepath . 'full.jpg',
			'fixed' => $imagepath . 'fixed.jpg',
			'fixed_2' => $imagepath . 'fixed_2.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose template",
		'desc' => "Choose template layout .",
		'id' => "author_template",
		'std' => "grid_1200",
		'type' => "images",
		'options' => array(
			'default' => $imagepath . 'sidebar_default.jpg',
			'grid_1200' => $imagepath . 'template_1200.jpg',
			'grid_970' => $imagepath . 'template_970.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Site skin",
		'desc' => "Choose Site skin .",
		'id' => "author_skin_l",
		'std' => "site_light",
		'type' => "images",
		'options' => array(
			'default' => $imagepath . 'sidebar_default.jpg',
			'site_light' => $imagepath . 'light.jpg',
			'site_dark' => $imagepath . 'dark.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose Your Skin",
		'desc' => "Choose Your Skin",
		'class' => "site_skin",
		'id' => "author_skin",
		'std' => "default",
		'type' => "images",
		'options' => array(
			'default_color'	=> $imagepath . 'default_color.jpg',
			'default'		=> $imagepath . 'default.jpg',
			'blue'			=> $imagepath . 'blue.jpg',
			'gray'			=> $imagepath . 'gray.jpg',
			'green'			=> $imagepath . 'green.jpg',
			'moderate_cyan' => $imagepath . 'moderate_cyan.jpg',
			'orange'		=> $imagepath . 'orange.jpg',
			'purple'	    => $imagepath . 'purple.jpg',
			'red'			=> $imagepath . 'red.jpg',
			'strong_cyan'	=> $imagepath . 'strong_cyan.jpg',
			'yellow'		=> $imagepath . 'yellow.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Primary Color",
		'desc' => "Primary Color",
		'id' => 'author_primary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Secondary Color ( it's darkness more than primary color )",
		'desc' => "Secondary Color ( it's darkness more than primary color )",
		'id' => 'author_secondary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Background Type",
		'desc' => "Background Type",
		'id' => 'author_background_type',
		'std' => 'patterns',
		'type' => 'radio',
		'options' => 
			array(
				"patterns" => "Patterns",
				"custom_background" => "Custom Background"
			)
	);

	$options[] = array(
		'name' => "Background Color",
		'desc' => "Background Color",
		'id' => 'author_background_color',
		'std' => "#FFF",
		'type' => 'color' );
		
	$options[] = array(
		'name' => "Choose Pattern",
		'desc' => "Choose Pattern",
		'id' => "author_background_pattern",
		'std' => "bg13",
		'type' => "images",
		'options' => array(
			'bg1' => $imagepath . 'bg1.jpg',
			'bg2' => $imagepath . 'bg2.jpg',
			'bg3' => $imagepath . 'bg3.jpg',
			'bg4' => $imagepath . 'bg4.jpg',
			'bg5' => $imagepath . 'bg5.jpg',
			'bg6' => $imagepath . 'bg6.jpg',
			'bg7' => $imagepath . 'bg7.jpg',
			'bg8' => $imagepath . 'bg8.jpg',
			'bg9' => $imagepath . '../../images/patterns/bg9.png',
			'bg10' => $imagepath . '../../images/patterns/bg10.png',
			'bg11' => $imagepath . '../../images/patterns/bg11.png',
			'bg12' => $imagepath . '../../images/patterns/bg12.png',
			'bg13' => $imagepath . 'bg13.jpg',
			'bg14' => $imagepath . 'bg14.jpg',
			'bg15' => $imagepath . '../../images/patterns/bg15.png',
			'bg16' => $imagepath . '../../images/patterns/bg16.png',
			'bg17' => $imagepath . 'bg17.jpg',
			'bg18' => $imagepath . 'bg18.jpg',
			'bg19' => $imagepath . 'bg19.jpg',
			'bg20' => $imagepath . 'bg20.jpg',
			'bg21' => $imagepath . '../../images/patterns/bg21.png',
			'bg22' => $imagepath . 'bg22.jpg',
			'bg23' => $imagepath . '../../images/patterns/bg23.png',
			'bg24' => $imagepath . '../../images/patterns/bg24.png',
	));

	$options[] = array(
		'name' =>  "Custom Background",
		'desc' => "Custom Background",
		'id' => 'author_custom_background',
		'std' => $background_defaults,
		'type' => 'background' );
		
	$options[] = array(
		'name' => "Full Screen Background",
		'desc' => "Click on to Full Screen Background",
		'id' => 'author_full_screen_background',
		'std' => '0',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Blog & Article settings',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Blog display",
		'desc' => "Choose the Blog display",
		'id' => 'home_display',
		'std' => 'blog_1',
		'type' => 'radio',
		'options' => 
			array(
				"blog_1" => "Blog 1",
				"blog_2" => "Blog 2"
		)
	);
	
	$options[] = array(
		'name' => 'Post meta enable or disable',
		'desc' => 'Post meta enable or disable .',
		'id' => 'post_meta',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Share enable or disable',
		'desc' => 'Share enable or disable .',
		'id' => 'post_share',
		'std' => 1,
		'type' => 'checkbox');
		
	$options[] = array(
		'name' => 'Author info box enable or disable',
		'desc' => 'Author info box enable or disable .',
		'id' => 'post_author_box',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Related post enable or disable',
		'desc' => 'Related post enable or disable .',
		'id' => 'related_post',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Related post number',
		'desc' => 'Type related post number from here .',
		'id' => 'related_number',
		'std' => '5',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Comments enable or disable',
		'desc' => 'Comments enable or disable .',
		'id' => 'post_comments',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Comments enable or disable for user only',
		'desc' => 'Comments enable or disable for user only .',
		'id' => 'post_comments_user',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Navigation post enable or disable',
		'desc' => 'Navigation post ( next and previous posts) enable or disable .',
		'id' => 'post_navigation',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Sidebar',
		'type' => 'heading');
	
	$options[] = array(
		'desc' => "Add your sidebars .",
		'id' => "sidebars",
		'std' => '',
		'type' => 'sidebar');
	
	$options[] = array(
		'name' => "Sidebar layout",
		'desc' => "Sidebar layout .",
		'id' => "sidebar_layout",
		'std' => "default",
		'type' => "images",
		'options' => array(
			'default' => $imagepath . 'sidebar_default.jpg',
			'right' => $imagepath . 'sidebar_right.jpg',
			'full' => $imagepath . 'sidebar_no.jpg',
			'left' => $imagepath . 'sidebar_left.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Home Page Sidebar",
		'desc' => "Home Page Sidebar .",
		'id' => "sidebar_home",
		'std' => '',
		'options' => $new_sidebars,
		'type' => 'select');
	
	$options[] = array(
		'name' => "Else home page , single and page",
		'desc' => "Else home page , single and page .",
		'id' => "else_sidebar",
		'std' => '',
		'options' => $new_sidebars,
		'type' => 'select');
	
	$options[] = array(
		'name' => 'Styling',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Home page layout",
		'desc' => "Home page layout .",
		'id' => "home_layout",
		'std' => "full",
		'type' => "images",
		'options' => array(
			'full' => $imagepath . 'full.jpg',
			'fixed' => $imagepath . 'fixed.jpg',
			'fixed_2' => $imagepath . 'fixed_2.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose template",
		'desc' => "Choose template layout .",
		'id' => "home_template",
		'std' => "grid_1200",
		'type' => "images",
		'options' => array(
			'grid_1200' => $imagepath . 'template_1200.jpg',
			'grid_970' => $imagepath . 'template_970.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Site skin",
		'desc' => "Choose Site skin .",
		'id' => "site_skin_l",
		'std' => "site_light",
		'type' => "images",
		'options' => array(
			'site_light' => $imagepath . 'light.jpg',
			'site_dark' => $imagepath . 'dark.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose Your Skin",
		'desc' => "Choose Your Skin",
		'class' => "site_skin",
		'id' => "site_skin",
		'std' => "default",
		'type' => "images",
		'options' => array(
			'default'		=> $imagepath . 'default.jpg',
			'blue'			=> $imagepath . 'blue.jpg',
			'gray'			=> $imagepath . 'gray.jpg',
			'green'			=> $imagepath . 'green.jpg',
			'moderate_cyan' => $imagepath . 'moderate_cyan.jpg',
			'orange'		=> $imagepath . 'orange.jpg',
			'purple'	    => $imagepath . 'purple.jpg',
			'red'			=> $imagepath . 'red.jpg',
			'strong_cyan'	=> $imagepath . 'strong_cyan.jpg',
			'yellow'		=> $imagepath . 'yellow.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Primary Color",
		'desc' => "Primary Color",
		'id' => 'primary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Secondary Color ( it's darkness more than primary color )",
		'desc' => "Secondary Color ( it's darkness more than primary color )",
		'id' => 'secondary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Background Type",
		'desc' => "Background Type",
		'id' => 'background_type',
		'std' => 'patterns',
		'type' => 'radio',
		'options' => 
			array(
				"patterns" => "Patterns",
				"custom_background" => "Custom Background"
			)
		);

	$options[] = array(
		'name' => "Background Color",
		'desc' => "Background Color",
		'id' => 'background_color',
		'std' => "#FFF",
		'type' => 'color' );
		
	$options[] = array(
		'name' => "Choose Pattern",
		'desc' => "Choose Pattern",
		'id' => "background_pattern",
		'std' => "bg13",
		'type' => "images",
		'options' => array(
			'bg1' => $imagepath . 'bg1.jpg',
			'bg2' => $imagepath . 'bg2.jpg',
			'bg3' => $imagepath . 'bg3.jpg',
			'bg4' => $imagepath . 'bg4.jpg',
			'bg5' => $imagepath . 'bg5.jpg',
			'bg6' => $imagepath . 'bg6.jpg',
			'bg7' => $imagepath . 'bg7.jpg',
			'bg8' => $imagepath . 'bg8.jpg',
			'bg9' => $imagepath . '../../images/patterns/bg9.png',
			'bg10' => $imagepath . '../../images/patterns/bg10.png',
			'bg11' => $imagepath . '../../images/patterns/bg11.png',
			'bg12' => $imagepath . '../../images/patterns/bg12.png',
			'bg13' => $imagepath . 'bg13.jpg',
			'bg14' => $imagepath . 'bg14.jpg',
			'bg15' => $imagepath . '../../images/patterns/bg15.png',
			'bg16' => $imagepath . '../../images/patterns/bg16.png',
			'bg17' => $imagepath . 'bg17.jpg',
			'bg18' => $imagepath . 'bg18.jpg',
			'bg19' => $imagepath . 'bg19.jpg',
			'bg20' => $imagepath . 'bg20.jpg',
			'bg21' => $imagepath . '../../images/patterns/bg21.png',
			'bg22' => $imagepath . 'bg22.jpg',
			'bg23' => $imagepath . '../../images/patterns/bg23.png',
			'bg24' => $imagepath . '../../images/patterns/bg24.png',
	));

	$options[] = array(
		'name' =>  "Custom Background",
		'desc' => "Custom Background",
		'id' => 'custom_background',
		'std' => $background_defaults,
		'type' => 'background' );
		
	$options[] = array(
		'name' => "Full Screen Background",
		'desc' => "Click on to Full Screen Background",
		'id' => 'full_screen_background',
		'std' => '0',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Question Categories',
		'type' => 'heading');
	
	foreach ($options_categories_obj_q as $category) {
		
		$options[] = array(
			'name' => $category->cat_name,
			'class' => 'cat_name',
			'type' => 'info');
		
		$options[] = array(
			'content' => '<div class="warp_cat_name">',
			'type' => 'content');
		
		$options[] = array(
			'name' => "Page layout",
			'desc' => "Page layout .",
			'class' => "cat_layout",
			'id' => "cat_layout".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'full' => $imagepath . 'full.jpg',
				'fixed' => $imagepath . 'fixed.jpg',
				'fixed_2' => $imagepath . 'fixed_2.jpg')
		);
		
		$options[] = array(
			'name' => "Choose template",
			'desc' => "Choose template layout .",
			'id' => "cat_template".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'grid_1200' => $imagepath . 'template_1200.jpg',
				'grid_970' => $imagepath . 'template_970.jpg'
			)
		);
		
		$options[] = array(
			'name' => "Sidebar layout",
			'desc' => "Sidebar layout .",
			'id' => "cat_sidebar_layout".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'right' => $imagepath . 'sidebar_right.jpg',
				'full' => $imagepath . 'sidebar_no.jpg',
				'left' => $imagepath . 'sidebar_left.jpg',
			)
		);
		
		$options[] = array(
			'name' => "Page Sidebar",
			'desc' => "Page Sidebar .",
			'id' => "cat_sidebar".$category->cat_ID,
			'std' => '',
			'options' => $new_sidebars,
			'type' => 'select');
		
		$options[] = array(
			'name' => "Site skin",
			'desc' => "Choose Site skin .",
			'id' => "cat_skin_l".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'site_light' => $imagepath . 'light.jpg',
				'site_dark' => $imagepath . 'dark.jpg'
			)
		);
		
		$options[] = array(
			'name' => "Choose Your Skin",
			'desc' => "Choose Your Skin",
			'class' => "site_skin",
			'id' => "cat_skin".$category->cat_ID,
			'std' => "default_color",
			'type' => "images",
			'options' => array(
				'default_color'	=> $imagepath . 'default_color.jpg',
				'default'		=> $imagepath . 'default.jpg',
				'blue'			=> $imagepath . 'blue.jpg',
				'gray'			=> $imagepath . 'gray.jpg',
				'green'			=> $imagepath . 'green.jpg',
				'moderate_cyan' => $imagepath . 'moderate_cyan.jpg',
				'orange'		=> $imagepath . 'orange.jpg',
				'purple'	    => $imagepath . 'purple.jpg',
				'red'			=> $imagepath . 'red.jpg',
				'strong_cyan'	=> $imagepath . 'strong_cyan.jpg',
				'yellow'		=> $imagepath . 'yellow.jpg',
			)
		);
		
		$options[] = array(
			'name' => "Primary Color",
			'desc' => "Primary Color",
			'id' => 'primary_color'.$category->cat_ID,
			'type' => 'color' );
		
		$options[] = array(
			'name' => "Secondary Color ( it's darkness more than primary color )",
			'desc' => "Secondary Color ( it's darkness more than primary color )",
			'id' => 'secondary_color'.$category->cat_ID,
			'type' => 'color' );
		
		$options[] = array(
			'name' => "Background Type",
			'desc' => "Background Type",
			'class' => "cat_background_type",
			'id' => 'cat_background_type'.$category->cat_ID,
			'std' => 'patterns',
			'type' => 'radio',
			'options' => 
				array(
					"patterns" => "Patterns",
					"custom_background" => "Custom Background"
				)
			);
	
		$options[] = array(
			'name' => "Background Color",
			'desc' => "Background Color",
			'class' => "cat_background_color",
			'id' => 'cat_background_color'.$category->cat_ID,
			'std' => "#FFF",
			'type' => 'color' );
			
		$options[] = array(
			'name' => "Choose Pattern",
			'desc' => "Choose Pattern",
			'class' => "cat_background_pattern",
			'id' => "cat_background_pattern".$category->cat_ID,
			'std' => "bg13",
			'type' => "images",
			'options' => array(
				'bg1' => $imagepath . 'bg1.jpg',
				'bg2' => $imagepath . 'bg2.jpg',
				'bg3' => $imagepath . 'bg3.jpg',
				'bg4' => $imagepath . 'bg4.jpg',
				'bg5' => $imagepath . 'bg5.jpg',
				'bg6' => $imagepath . 'bg6.jpg',
				'bg7' => $imagepath . 'bg7.jpg',
				'bg8' => $imagepath . 'bg8.jpg',
				'bg9' => $imagepath . '../../images/patterns/bg9.png',
				'bg10' => $imagepath . '../../images/patterns/bg10.png',
				'bg11' => $imagepath . '../../images/patterns/bg11.png',
				'bg12' => $imagepath . '../../images/patterns/bg12.png',
				'bg13' => $imagepath . 'bg13.jpg',
				'bg14' => $imagepath . 'bg14.jpg',
				'bg15' => $imagepath . '../../images/patterns/bg15.png',
				'bg16' => $imagepath . '../../images/patterns/bg16.png',
				'bg17' => $imagepath . 'bg17.jpg',
				'bg18' => $imagepath . 'bg18.jpg',
				'bg19' => $imagepath . 'bg19.jpg',
				'bg20' => $imagepath . 'bg20.jpg',
				'bg21' => $imagepath . '../../images/patterns/bg21.png',
				'bg22' => $imagepath . 'bg22.jpg',
				'bg23' => $imagepath . '../../images/patterns/bg23.png',
				'bg24' => $imagepath . '../../images/patterns/bg24.png',
		));
	
		$options[] = array(
			'name' =>  "Custom Background",
			'desc' => "Custom Background",
			'class' => "cat_custom_background",
			'id' => 'cat_custom_background'.$category->cat_ID,
			'std' => $background_defaults,
			'type' => 'background' );
			
		$options[] = array(
			'name' => "Full Screen Background",
			'desc' => "Click on to Full Screen Background",
			'class' => "cat_full_screen_background",
			'id' => 'cat_full_screen_background'.$category->cat_ID,
			'std' => '0',
			'type' => 'checkbox');
	
		$options[] = array(
			'content' => '</div>',
			'type' => 'content');
		
	}
	
	$options[] = array(
		'name' => 'Categories Design',
		'type' => 'heading');
	
	foreach ($options_categories_obj as $category) {
		
		$options[] = array(
			'name' => $category->cat_name,
			'class' => 'cat_name',
			'type' => 'info');
		
		$options[] = array(
			'content' => '<div class="warp_cat_name">',
			'type' => 'content');
		
		$options[] = array(
			'name' => "Page layout",
			'desc' => "Page layout .",
			'class' => "cat_layout",
			'id' => "cat_layout".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'full' => $imagepath . 'full.jpg',
				'fixed' => $imagepath . 'fixed.jpg',
				'fixed_2' => $imagepath . 'fixed_2.jpg')
		);
		
		$options[] = array(
			'name' => "Choose template",
			'desc' => "Choose template layout .",
			'id' => "cat_template".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'grid_1200' => $imagepath . 'template_1200.jpg',
				'grid_970' => $imagepath . 'template_970.jpg'
			)
		);
		
		$options[] = array(
			'name' => "Sidebar layout",
			'desc' => "Sidebar layout .",
			'id' => "cat_sidebar_layout".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'right' => $imagepath . 'sidebar_right.jpg',
				'full' => $imagepath . 'sidebar_no.jpg',
				'left' => $imagepath . 'sidebar_left.jpg',
			)
		);
		
		$options[] = array(
			'name' => "Page Sidebar",
			'desc' => "Page Sidebar .",
			'id' => "cat_sidebar".$category->cat_ID,
			'std' => '',
			'options' => $new_sidebars,
			'type' => 'select');
		
		$options[] = array(
			'name' => "Site skin",
			'desc' => "Choose Site skin .",
			'id' => "cat_skin_l".$category->cat_ID,
			'std' => "default",
			'type' => "images",
			'options' => array(
				'default' => $imagepath . 'sidebar_default.jpg',
				'site_light' => $imagepath . 'light.jpg',
				'site_dark' => $imagepath . 'dark.jpg'
			)
		);
		
		$options[] = array(
			'name' => "Choose Your Skin",
			'desc' => "Choose Your Skin",
			'class' => "site_skin",
			'id' => "cat_skin".$category->cat_ID,
			'std' => "default_color",
			'type' => "images",
			'options' => array(
				'default_color'	=> $imagepath . 'default_color.jpg',
				'default'		=> $imagepath . 'default.jpg',
				'blue'			=> $imagepath . 'blue.jpg',
				'gray'			=> $imagepath . 'gray.jpg',
				'green'			=> $imagepath . 'green.jpg',
				'moderate_cyan' => $imagepath . 'moderate_cyan.jpg',
				'orange'		=> $imagepath . 'orange.jpg',
				'purple'	    => $imagepath . 'purple.jpg',
				'red'			=> $imagepath . 'red.jpg',
				'strong_cyan'	=> $imagepath . 'strong_cyan.jpg',
				'yellow'		=> $imagepath . 'yellow.jpg',
			)
		);
		
		$options[] = array(
			'name' => "Primary Color",
			'desc' => "Primary Color",
			'id' => 'primary_color'.$category->cat_ID,
			'type' => 'color' );
		
		$options[] = array(
			'name' => "Secondary Color ( it's darkness more than primary color )",
			'desc' => "Secondary Color ( it's darkness more than primary color )",
			'id' => 'secondary_color'.$category->cat_ID,
			'type' => 'color' );
		
		$options[] = array(
			'name' => "Background Type",
			'desc' => "Background Type",
			'class' => "cat_background_type",
			'id' => 'cat_background_type'.$category->cat_ID,
			'std' => 'patterns',
			'type' => 'radio',
			'options' => 
				array(
					"patterns" => "Patterns",
					"custom_background" => "Custom Background"
				)
			);
	
		$options[] = array(
			'name' => "Background Color",
			'desc' => "Background Color",
			'class' => "cat_background_color",
			'id' => 'cat_background_color'.$category->cat_ID,
			'std' => "#FFF",
			'type' => 'color' );
			
		$options[] = array(
			'name' => "Choose Pattern",
			'desc' => "Choose Pattern",
			'class' => "cat_background_pattern",
			'id' => "cat_background_pattern".$category->cat_ID,
			'std' => "bg13",
			'type' => "images",
			'options' => array(
				'bg1' => $imagepath . 'bg1.jpg',
				'bg2' => $imagepath . 'bg2.jpg',
				'bg3' => $imagepath . 'bg3.jpg',
				'bg4' => $imagepath . 'bg4.jpg',
				'bg5' => $imagepath . 'bg5.jpg',
				'bg6' => $imagepath . 'bg6.jpg',
				'bg7' => $imagepath . 'bg7.jpg',
				'bg8' => $imagepath . 'bg8.jpg',
				'bg9' => $imagepath . '../../images/patterns/bg9.png',
				'bg10' => $imagepath . '../../images/patterns/bg10.png',
				'bg11' => $imagepath . '../../images/patterns/bg11.png',
				'bg12' => $imagepath . '../../images/patterns/bg12.png',
				'bg13' => $imagepath . 'bg13.jpg',
				'bg14' => $imagepath . 'bg14.jpg',
				'bg15' => $imagepath . '../../images/patterns/bg15.png',
				'bg16' => $imagepath . '../../images/patterns/bg16.png',
				'bg17' => $imagepath . 'bg17.jpg',
				'bg18' => $imagepath . 'bg18.jpg',
				'bg19' => $imagepath . 'bg19.jpg',
				'bg20' => $imagepath . 'bg20.jpg',
				'bg21' => $imagepath . '../../images/patterns/bg21.png',
				'bg22' => $imagepath . 'bg22.jpg',
				'bg23' => $imagepath . '../../images/patterns/bg23.png',
				'bg24' => $imagepath . '../../images/patterns/bg24.png',
		));
	
		$options[] = array(
			'name' =>  "Custom Background",
			'desc' => "Custom Background",
			'class' => "cat_custom_background",
			'id' => 'cat_custom_background'.$category->cat_ID,
			'std' => $background_defaults,
			'type' => 'background' );
			
		$options[] = array(
			'name' => "Full Screen Background",
			'desc' => "Click on to Full Screen Background",
			'class' => "cat_full_screen_background",
			'id' => 'cat_full_screen_background'.$category->cat_ID,
			'std' => '0',
			'type' => 'checkbox');
	
		$options[] = array(
			'content' => '</div>',
			'type' => 'content');
	}
	
	$options[] = array(
		'name' => 'Advertising',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Advertising after header",
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'header_adv_type',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'header_adv_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'header_adv_href',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'header_adv_code',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "Advertising after share and tags ( in post and question )",
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'share_adv_type',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'share_adv_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'share_adv_href',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'share_adv_code',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "Advertising after content",
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'content_adv_type',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'content_adv_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'content_adv_href',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'content_adv_code',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'Footer settings',
		'type' => 'heading');
		
	$options[] = array(
		'name' => "Footer skin",
		'desc' => "Choose the footer skin .",
		'id' => "footer_skin",
		'std' => "footer_dark",
		'type' => "images",
		'options' => array(
			'footer_dark' => $imagepath . 'footer_dark.jpg',
			'footer_light' => $imagepath . 'footer_light.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Footer Layout",
		'desc' => "Footer columns Layout .",
		'id' => "footer_layout",
		'std' => "footer_4c",
		'type' => "images",
		'options' => array(
			'footer_1c' => $imagepath . 'footer_1c.jpg',
			'footer_2c' => $imagepath . 'footer_2c.jpg',
			'footer_3c' => $imagepath . 'footer_3c.jpg',
			'footer_4c' => $imagepath . 'footer_4c.jpg',
			'footer_5c' => $imagepath . 'footer_5c.jpg',
			'footer_no' => $imagepath . 'footer_no.jpg'
		)
	);
	
	$options[] = array(
		'name' => 'Copyrights',
		'desc' => 'Put the copyrights of footer .',
		'id' => 'footer_copyrights',
		'std' => 'Copyright 2014 Ask me | <a href="#">By 2code</a>',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'Social enable or disable',
		'desc' => 'Social enable or disable .',
		'id' => 'social_icon_f',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Twitter URL',
		'desc' => 'Type the twitter URL from here .',
		'id' => 'twitter_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Facebook URL',
		'desc' => 'Type the facebook URL from here .',
		'id' => 'facebook_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Google plus URL',
		'desc' => 'Type the google plus URL from here .',
		'id' => 'gplus_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Youtube URL',
		'desc' => 'Type the youtube URL from here .',
		'id' => 'youtube_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Skype',
		'desc' => 'Type the skype from here .',
		'id' => 'skype_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Flickr URL',
		'desc' => 'Type the flickr URL from here .',
		'id' => 'flickr_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Rss enable or disable',
		'desc' => 'Rss enable or disable .',
		'id' => 'rss_icon_f',
		'std' => 1,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Advanced",
		'id' => "advanced",
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Twitter consumer key',
		'desc' => 'Twitter consumer key. ',
		'id' => 'twitter_consumer_key',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter consumer secret',
		'desc' => 'Twitter consumer secret. ',
		'id' => 'twitter_consumer_secret',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter access token',
		'desc' => 'Twitter access token. ',
		'id' => 'twitter_access_token',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter access token secret',
		'desc' => 'Twitter access token secret. ',
		'id' => 'twitter_access_token_secret',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => "If you wont to export setting please refresh the page before that",
		'type' => 'info');

	$options[] = array(
		'name' => "Export Setting",
		'desc' => "Copy this to saved file",
		'id' => 'export_setting',
		'export' => $current_options_e,
		'type' => 'export');

	$options[] = array(
		'name' => "Import Setting",
		'desc' => "Put here the import setting",
		'id' => 'import_setting',
		'type' => 'import');
	
	return $options;
}