<?php
/* excerpt */
function excerpt($excerpt_length) {
	global $post;
	$content = $post->post_content;
	$words = explode(' ',$content,$excerpt_length + 1);
	if (count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words,'...');
		$content = implode(' ',$words);
	endif;
		$content = strip_tags($content);
	echo $content;
}
/* excerpt_title */
function excerpt_title($excerpt_length) {
	global $post;
	$title = $post->post_title;
	$words = explode(' ',$title,$excerpt_length + 1);
	if (count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words,'');
		$title = implode(' ',$words);
	endif;
		$title = strip_tags($title);
	echo $title;
}
/* excerpt_any */
function excerpt_any($excerpt_length,$title) {
	$words = explode(' ',$title,$excerpt_length + 1);
	if (count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words,'');
		$title = implode(' ',$words);
	endif;
		$title = strip_tags($title);
	return $title;
}
/* add post-thumbnails */
add_theme_support('post-thumbnails');
/* get_aq_resize_img */
function get_aq_resize_img($thumbnail_size,$img_width_f,$img_height_f) {
	$thumb = get_post_thumbnail_id();
	if ($thumb != "") {
		$img_url = wp_get_attachment_url($thumb,$thumbnail_size);
		$img_width = $img_width_f;
		$img_height = $img_height_f;
		$image = aq_resize($img_url,$img_width,$img_height,true);
		if ($image) {
			$last_image = $image;
		}else {
			$last_image = "http://placehold.it/".$img_width_f."x".$img_height_f;
		}
		return "<img alt='".get_the_title()."' width='".$img_width."' height='".$img_height."' src='".$last_image."'>";
	}else {
		return "<img alt='".get_the_title()."' src='".vpanel_image()."'>";
	}
}
/* get_aq_resize_img_url */
function get_aq_resize_img_url($url,$thumbnail_size,$img_width_f,$img_height_f) {
	$thumb = $url;
	if ($thumb != "") {
		$img_url = $thumb;
		$img_width = $img_width_f;
		$img_height = $img_height_f;
		$image = aq_resize($img_url,$img_width,$img_height,true);
		if ($image) {
			$last_image = $image;
		}else {
			$last_image = "http://placehold.it/".$img_width_f."x".$img_height_f;
		}
		return "<img alt='".get_the_title()."' width='".$img_width."' height='".$img_height."' src='".$last_image."'>";
	}else {
		return "<img alt='".get_the_title()."' src='".vpanel_image()."'>";
	}
}
/* get_aq_resize_url */
function get_aq_resize_url($url,$thumbnail_size,$img_width_f,$img_height_f) {
	$img_url = $url;
	$img_width = $img_width_f;
	$img_height = $img_height_f;
	$image = aq_resize($img_url,$img_width,$img_height,true);
	if ($image) {
		$last_image = $image;
	}else {
		$last_image = "http://placehold.it/".$img_width_f."x".$img_height_f;
	}
	return $last_image;
}
/* get_aq_resize_img_full */
function get_aq_resize_img_full($thumbnail_size) {
	$thumb = get_post_thumbnail_id();
	if ($thumb != "") {
		$img_url = wp_get_attachment_url($thumb,$thumbnail_size);
		$image = $img_url;
		return "<img alt='".get_the_title()."' src='".$image."'>";
	}else {
		return "<img alt='".get_the_title()."' src='".vpanel_image()."'>";
	}
}
/* vpanel_image */
function vpanel_image() {
	global $post;
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i',$post->post_content,$matches);
	if (isset($matches[1][0])) {
		return $matches[1][0];
	}else {
		return false;
	}
}
/* formatMoney */
function formatMoney($number,$fractional=false) {
    if ($fractional) {
        $number = sprintf('%.2f',$number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/','$1,$2',$number);
        if ($replaced != $number) {
            $number = $replaced;
        }else {
            break;
        }
    }
    return $number;
}
/* get_gplus_count */
function get_gplus_count($url) { //Google+
    if ($url != '') {
        $first_curl_function = strrev('tini_lruc');
        $ch = $first_curl_function();
        curl_setopt($ch,CURLOPT_URL,"https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ");
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_POSTFIELDS,'[{"method":"pos.plusones.get","id":"p",
		"params":{"nolog":true,"id":"https://plus.google.com/' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},
		"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-type: application/json'));
        $curl_function_2 = strrev('cexe_lruc');
        $result = $curl_function_2 ($ch);
        curl_close ($ch);
        $json = json_decode($result,true);
        return(formatMoney($json[0]['result']['metadata']['globalCounts']['count']));
    }else {
        return(0);
    }
}
/* get_facebook_count */
function get_facebook_count($fan_page) { //Facebook
    if ($fan_page != '') {
        $url = 'http://graph.facebook.com/'.trim($fan_page);
        $file_get_function =  strrev('stnetnoc_teg_elif');
        $counter = json_decode($file_get_function($url));
        return(formatMoney(intval($counter->likes)));
    }else {
        return('0');
    }
}
/* get_twitter_count */
function get_twitter_count($user_name) {
	if ($user_name != "") {
		try {
			$twitter_username 		= $user_name;
			$consumer_key 			= vpanel_options('twitter_consumer_key');
			$consumer_secret		= vpanel_options('twitter_consumer_secret');
			$access_token 			= vpanel_options('twitter_access_token');
			$access_token_secret 	= vpanel_options('twitter_access_token_secret');
			
			$twitterConnection = new TwitterOAuth( $consumer_key , $consumer_secret , $access_token , $access_token_secret	);
			$twitterData = $twitterConnection->get('users/show', array('screen_name' => $twitter_username));
			$twitter['page_url'] = 'http://www.twitter.com/'.$twitter_username;
			$twitter['followers_count'] = $twitterData->followers_count;;
		}catch (Exception $e) {
			$twitter['page_url'] = 'http://www.twitter.com/'.$twitter_username;
			$twitter['followers_count'] = 0;
		}
		if( !empty( $twitter['followers_count'] ) &&  get_option( 'followers_count') != $twitter['followers_count'] )
			update_option( 'followers_count' , $twitter['followers_count'] );
			
		if( $twitter['followers_count'] == 0 && get_option( 'followers_count') )
			$twitter['followers_count'] = get_option( 'followers_count');
				
		elseif( $twitter['followers_count'] == 0 && !get_option( 'followers_count') )
			$twitter['followers_count'] = 0;
		
		return $twitter;
	}
}
/* get_youtube_count */
function get_youtube_count ($channel_name) {
    if ($channel_name != '') {
    	$allow_cash = true;
        if ($allow_cash == true) {
            $social_cash = get_transient('social_count_youtube');
            if ($social_cash != '' and !empty($social_cash)) {
                return ($social_cash);
            }
        }
        $file_get_function = strrev('stnetnoc_teg_elif');
        $youtube_data = $file_get_function('http://gdata.youtube.com/feeds/api/users/' . trim($channel_name) . '?alt=json');
        $youtube_data = json_decode($youtube_data, true);
        $youtube_count = $youtube_data['entry']['yt$statistics']['subscriberCount'];
        if (intval($youtube_count) <= 0) return 0;
        if ($allow_cash == true) {
            set_transient('social_count_youtube', formatMoney($youtube_count), 1111);
        }
        return (formatMoney($youtube_count));
    }
}
/* Vpanel_Questions */
function Vpanel_Questions($questions_per_page = 5,$orderby,$display_date,$questions_excerpt,$post_or_question) {
	global $post;
	if ($orderby == "popular") {
		$orderby = array('orderby' => 'comment_count');
	}elseif ($orderby == "random") {
		$orderby = array('orderby' => 'rand');
	}else {
		$orderby = array();
	}
	query_posts(array_merge($orderby,array('post_type' => $post_or_question,'ignore_sticky_posts' => 1,'posts_per_page' => $questions_per_page)));
	if ( have_posts()) :
		echo "<ul class='related-posts'>";
			while ( have_posts()) : the_post();?>
				<li class="related-item">
					<h3>
						<a href="<?php the_permalink();?>" title="<?php printf('%s',the_title_attribute('echo=0')); ?>" rel="bookmark">
							<?php if ($questions_excerpt == 0) {?>
								<i class="icon-double-angle-right"></i>
							<?php }
							excerpt_title(5);?>
						</a>
					</h3>
					<?php if ($questions_excerpt != 0) {?>
						<p><?php excerpt($questions_excerpt);?></p>
					<?php }
					if ($display_date == "on") {?>
						<div class="clear"></div><span <?php echo ($questions_excerpt == 0?"class='margin_t_5'":"")?>><?php the_time('F j, Y');?></span>
					<?php }?>
				</li>
			<?php endwhile;
		echo "</ul>";
	endif;
	wp_reset_query();
}
/* Vpanel_comments */
function Vpanel_comments($post_or_question = "question",$comments_number = 5,$comment_excerpt = 30) {
	$comments = get_comments(array("post_type" => $post_or_question,"status" => "approve","number" => $comments_number));
	echo "<div class='widget_highest_points widget_comments'><ul>";
		foreach ($comments as $comment) {
			$you_avatar = get_the_author_meta('you_avatar',$comment->user_id);
			$user_profile_page = get_author_posts_url($comment->user_id);
		    ?>
		    <li>
		    	<div class="author-img">
		    		<?php if ($comment->user_id != 0) {?>
			    		<a href="<?php echo $user_profile_page?>">
	    			<?php }
		    			if ($you_avatar && $comment->user_id != 0) {
		    				$you_avatar_img = get_aq_resize_url(esc_attr($you_avatar),"full",65,65);
		    				echo "<img alt='".$comment->comment_author."' src='".$you_avatar_img."'>";
		    			}else {
		    				echo get_avatar(get_the_author_meta('user_email',$comment->user_id),'65','');
		    			}
		    		if ($comment->user_id != 0) {?>
			    		</a>
		    		<?php }?>
		    	</div> 
		    	<h6><a href="<?php echo get_permalink($comment->comment_post_ID);?>#comment-<?php echo $comment->comment_ID;?>"><?php echo strip_tags($comment->comment_author);?> : <?php echo wp_html_excerpt($comment->comment_content,$comment_excerpt);?></a></h6>
		    </li>
		    <?php
		}
	echo "</ul></div>";
}
/* vbegy_comment */
function vbegy_comment($comment,$args,$depth) {
    $GLOBALS['comment'] = $comment;
    $add_below = '';
    ?>
    <li <?php comment_class('comment');?> id="comment-<?php comment_ID();?>">
        <div class="comment-body clearfix"> 
            <div class="avatar-img">
            	<?php 
            	if (get_the_author_meta('you_avatar', $comment->user_id)) {
            		$you_avatar_img = get_aq_resize_url(esc_attr(get_the_author_meta('you_avatar', $comment->user_id)),"full",65,65);
            		echo "<img alt='".$comment->comment_author."' src='".$you_avatar_img."'>";
            	}else {
            		echo get_avatar($comment,65);
            	}?>
            </div>
            <div class="comment-text">
                <div class="author clearfix">
                	<div class="comment-meta">
                        <span><?php echo get_comment_author();?></span>
                        <div class="date"><i class="icon-time"></i><?php printf('%1$s at %2$s',get_comment_date(), get_comment_time()) ?></div> 
                    </div>
                    <div class="comment-reply">
                    <?php
                    edit_comment_link('Edit','  ','');
                    comment_reply_link(array_merge( $args,array('reply_text' => '<i class="icon-reply"></i>'.__("Reply","vbegy"),'add_below' => $add_below,'depth' => $depth,'max_depth' => $args['max_depth'])));?>
                    </div>
                </div>
                <div class="text">
                	<?php if ($comment->comment_approved == '0') : ?>
                	    <em><?php _e('Your comment is awaiting moderation.','vbegy')?></em><br>
                	<?php endif; ?>
                	<?php comment_text() ?>
                </div>
            </div>
        </div>
    <?php
}
/* vbegy_answer */
function vbegy_answer($comment,$args,$depth) {
	global $post;
    $GLOBALS['comment'] = $comment;
    $add_below = '';
    $comment_vote = get_comment_meta($comment->comment_ID,'comment_vote');
    $comment_vote = (!empty($comment_vote)?$comment_vote[0]["vote"]:"");
    $the_best_answer = get_post_meta($post->ID,"the_best_answer",true);
    $comment_best_answer = ($the_best_answer == $comment->comment_ID?"comment-best-answer":"");
    ?>
    <li <?php comment_class('comment '.$comment_best_answer);?> id="comment-<?php comment_ID();?>">
    	<div class="comment-body clearfix" rel="post-<?php echo $post->ID?>"> 
    	    <div class="avatar-img">
    	    	<?php if ($comment->user_id != 0){
    	    		if (get_the_author_meta('you_avatar', $comment->user_id)) {
    	    			$you_avatar_img = get_aq_resize_url(esc_attr(get_the_author_meta('you_avatar', $comment->user_id)),"full",65,65);
    	    			echo "<img alt='".$comment->comment_author."' src='".$you_avatar_img."'>";
    	    		}else {
    	    			echo get_avatar($comment,65);
    	    		}
    	    	}else {
    	    		echo get_avatar($comment->comment_author_email,65);
    	    	}?>
    	    </div>
    	    <div class="comment-text">
    	        <div class="author clearfix">
    	        	<div class="comment-author"><a href="<?php echo get_author_posts_url($comment->user_id,get_the_author_meta('user_nicename', $comment->user_id));?>"><?php echo get_comment_author();?></a></div>
    	        	<div class="comment-vote">
    	            	<ul class="single-question-vote">
    	            		<?php if (is_user_logged_in()){?>
    	            			<li class="loader_3"></li>
    	            			<li><a href="#" class="single-question-vote-up comment_vote_up<?php echo (isset($_COOKIE['comment_vote'.$comment->comment_ID])?" ".$_COOKIE['comment_vote'.$comment->comment_ID]."-".$comment->comment_ID:"")?>" title="<?php _e("Like","vbegy");?>" id="comment_vote_up-<?php echo $comment->comment_ID?>"><i class="icon-thumbs-up"></i></a></li>
    	            			<li><a href="#" class="single-question-vote-down comment_vote_down<?php echo (isset($_COOKIE['comment_vote'.$comment->comment_ID])?" ".$_COOKIE['comment_vote'.$comment->comment_ID]."-".$comment->comment_ID:"")?>" id="comment_vote_down-<?php echo $comment->comment_ID?>" title="<?php _e("Dislike","vbegy");?>"><i class="icon-thumbs-down"></i></a></li>
    	            		<?php }else { ?>
    	            			<li class="loader_3"></li>
    	            			<li><a href="#" class="single-question-vote-up comment_vote_up vote_not_user" title="<?php _e("Like","vbegy");?>"><i class="icon-thumbs-up"></i></a></li>
    	            			<li><a href="#" class="single-question-vote-down comment_vote_down vote_not_user" title="<?php _e("Dislike","vbegy");?>"><i class="icon-thumbs-down"></i></a></li>
    	            		<?php }?>
    	            	</ul>
    	        	</div>
    	        	<span class="question-vote-result question_vote_result <?php echo ($comment_vote < 0?"question_vote_red":"")?>"><?php echo ($comment_vote != ""?$comment_vote:0)?></span>
    	        	<div class="comment-meta">
    	                <div class="date"><i class="icon-time"></i><?php printf('%1$s at %2$s',get_comment_date(), get_comment_time()) ?></div> 
    	            </div>
    	            <div class="comment-reply">
	    	            <?php
	    	            edit_comment_link('Edit','  ','');?>
    	                <a class="question_r_l comment_l report_c" href="#"><i class="icon-flag"></i><?php _e("Report","vbegy")?></a>
	    	            <?php comment_reply_link(array_merge( $args,array('reply_text' => '<i class="icon-reply"></i>'.__("Reply","vbegy"),'add_below' => $add_below,'depth' => $depth,'max_depth' => $args['max_depth'])));?>
    	            </div>
    	        </div>
    	        <div class="text">
    	        	<div class="explain-reported">
    	        		<h3><?php _e("Please briefly explain why you feel this answer should be reported .","vbegy")?></h3>
    	        		<textarea name="explain-reported"></textarea>
    	        		<div class="clearfix"></div>
    	        		<div class="loader_3"></div>
    	        		<a class="color button small report"><?php _e("Report","vbegy")?></a>
    	        		<a class="color button small dark_button cancel"><?php _e("Cancel","vbegy")?></a>
    	        	</div><!-- End reported -->
    	        	<?php if ($comment->comment_approved == '0') : ?>
    	        	    <em><?php _e('Your comment is awaiting moderation.','vbegy')?></em><br>
    	        	<?php endif; ?>
    	        	<?php comment_text();?>
    	        	<div class="clearfix"></div>
    	        	<?php $added_file = get_comment_meta($comment->comment_ID,'added_file', true);
    	        	if ($added_file != "") {
    	        		echo "<div class='clearfix'></div><br><a href='".wp_get_attachment_url($added_file)."'>".__("Attachment","vbegy")."</a>";
    	        	}
    	        	?>
    	        </div>
    	        <div class="clearfix"></div>
	        	<div class="loader_3"></div>
    	        <?php
    	        if ($the_best_answer == $comment->comment_ID) {
    	        	echo '<div class="commentform question-answered question-answered-done"><i class="icon-ok"></i>'.__("Best answer","vbegy").'</div>
    	        	<div class="clearfix"></div>';
    	        	if (is_user_logged_in() and get_current_user_id() == $post->post_author and $the_best_answer != 0){
	    	        	echo '<a class="commentform best_answer_re question-report" title="'.__("Cancel the best answer","vbegy").'" href="#">'.__("Cancel the best answer","vbegy").'</a>';
    	        	}
    	        }
    	        if (is_user_logged_in() and get_current_user_id() == $post->post_author and ($the_best_answer == 0 or $the_best_answer == "")){?>
    	        	<a class="commentform best_answer_a question-report" title="<?php _e("Select as best answer","vbegy");?>" href="#"><?php _e("Select as best answer","vbegy");?></a>
    	        <?php
    	        }
    	        ?>
    	        <div class="no_vote_more"></div>
    	    </div>
    	</div>
    <?php
}
/* vpanel_pagination */
if ( ! function_exists('vpanel_pagination')) {
	function vpanel_pagination( $args = array(),$query = '') {
		global $wp_rewrite,$wp_query;
		do_action('vpanel_pagination_start');
		if ( $query) {
			$wp_query = $query;
		} // End IF Statement
		/* If there's not more than one page,return nothing. */
		if ( 1 >= $wp_query->max_num_pages)
			return;
		/* Get the current page. */
		$current = ( get_query_var('paged') ? absint( get_query_var('paged')) : 1);
		/* Get the max number of pages. */
		$max_num_pages = intval( $wp_query->max_num_pages);
		/* Set up some default arguments for the paginate_links() function. */
		$defaults = array(
			'base' => add_query_arg('paged','%#%'),
			'format' => '',
			'total' => $max_num_pages,
			'current' => $current,
			'prev_next' => true,
			'prev_text' => __('<i class="icon-angle-left"></i>','vbegy'),// Translate in WordPress. This is the default.
			'next_text' => __('<i class="icon-angle-right"></i>','vbegy'),// Translate in WordPress. This is the default.
			'show_all' => false,
			'end_size' => 1,
			'mid_size' => 1,
			'add_fragment' => '',
			'type' => 'plain',
			'before' => '<div class="pagination">',// Begin vpanel_pagination() arguments.
			'after' => '</div>',
			'echo' => true,
		);
		/* Add the $base argument to the array if the user is using permalinks. */
		if ( $wp_rewrite->using_permalinks())
			$defaults['base'] = user_trailingslashit( trailingslashit( get_pagenum_link()) . 'page/%#%');
		/* If we're on a search results page,we need to change this up a bit. */
		if ( is_search()) {
		/* If we're in BuddyPress,use the default "unpretty" URL structure. */
			if ( class_exists('BP_Core_User')) {
				$search_query = get_query_var('s');
				$paged = get_query_var('paged');
				$base = user_trailingslashit( home_url()) . '?s=' . $search_query . '&paged=%#%';
				$defaults['base'] = $base;
			} else {
				$search_permastruct = $wp_rewrite->get_search_permastruct();
				if ( !empty( $search_permastruct))
					$defaults['base'] = user_trailingslashit( trailingslashit( get_search_link()) . 'page/%#%');
			}
		}
		/* Merge the arguments input with the defaults. */
		$args = wp_parse_args( $args,$defaults);
		/* Allow developers to overwrite the arguments with a filter. */
		$args = apply_filters('vpanel_pagination_args',$args);
		/* Don't allow the user to set this to an array. */
		if ('array' == $args['type'])
			$args['type'] = 'plain';
		/* Make sure raw querystrings are displayed at the end of the URL,if using pretty permalinks. */
		$pattern = '/\?(.*?)\//i';
		preg_match( $pattern,$args['base'],$raw_querystring);
		if ( $wp_rewrite->using_permalinks() && $raw_querystring)
			$raw_querystring[0] = str_replace('','',$raw_querystring[0]);
			if (!empty($raw_querystring)) {
				@$args['base'] = str_replace( $raw_querystring[0],'',$args['base']);
				@$args['base'] .= substr( $raw_querystring[0],0,-1);
			}
		/* Get the paginated links. */
		$page_links = paginate_links( $args);
		/* Remove 'page/1' from the entire output since it's not needed. */
		$page_links = str_replace( array('&#038;paged=1\'','/page/1\''),'\'',$page_links);
		/* Wrap the paginated links with the $before and $after elements. */
		$page_links = $args['before'] . $page_links . $args['after'];
		/* Allow devs to completely overwrite the output. */
		$page_links = apply_filters('vpanel_pagination',$page_links);
		do_action('vpanel_pagination_end');
		/* Return the paginated links for use in themes. */
		if ( $args['echo'])
			echo $page_links;
		else
			return $page_links;
	}
}
/* vpanel_admin_bar */
function vpanel_admin_bar() {
	global $wp_admin_bar;
	if (is_super_admin()) {
		$wp_admin_bar->add_menu( array(
			'parent' => 0,
			'id' => 'vpanel_page',
			'title' => 'Ask me Settings' ,
			'href' => admin_url( 'admin.php?page=options')
		));
	}
}
add_action( 'wp_before_admin_bar_render', 'vpanel_admin_bar' );
/* breadcrumbs */
function breadcrumbs($args = array()) {
    $delimiter  = __('<span class="crumbs-span">/</span>','vbegy');
    $home       = __('Home','vbegy');
    $before     = '<h1>';
    $after      = '</h1>';
    if (!is_home() && !is_front_page() || is_paged()) {
        echo '<div class="breadcrumbs"><section class="container"><div class="row"><div class="col-md-12">';
        global $post,$wp_query;
        $item = array();
        $homeLink = home_url();
        if (is_page()) {
			echo $before . get_the_title() . $after;
        }else if (is_attachment()) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID);
			echo $before . get_the_title() . $after;
        }elseif ( is_singular() ) {
    		$post = $wp_query->get_queried_object();
    		$post_id = (int) $wp_query->get_queried_object_id();
    		$post_type = $post->post_type;
    		$post_type_object = get_post_type_object( $post_type );
    		if ( 'post' === $wp_query->post->post_type || 'question' === $wp_query->post->post_type ) {
    			echo $before . get_the_title() . $after;
    		}
    		if ( 'page' !== $wp_query->post->post_type ) {
    			if ( isset( $args["singular_{$wp_query->post->post_type}_taxonomy"] ) && is_taxonomy_hierarchical( $args["singular_{$wp_query->post->post_type}_taxonomy"] ) ) {
    				$terms = wp_get_object_terms( $post_id, $args["singular_{$wp_query->post->post_type}_taxonomy"] );
    				echo array_merge( $item, breadcrumbs_plus_get_term_parents( $terms[0], $args["singular_{$wp_query->post->post_type}_taxonomy"] ) );
    			}
    			elseif ( isset( $args["singular_{$wp_query->post->post_type}_taxonomy"] ) )
    				echo get_the_term_list( $post_id, $args["singular_{$wp_query->post->post_type}_taxonomy"], '', ', ', '' );
    		}
    	}else if (is_category() || is_tag() || is_tax()) {
            global $wp_query;
            $term = $wp_query->get_queried_object();
			$taxonomy = get_taxonomy( $term->taxonomy );
			if ( ( is_taxonomy_hierarchical( $term->taxonomy ) && $term->parent ) && $parents = breadcrumbs_plus_get_term_parents( $term->parent, $term->taxonomy ) )
				$item = array_merge( $item, $parents );
			$item['last'] = $term->name;
            echo $before . '' . single_cat_title('', false) . '' . $after;
        }elseif (is_day()) {
            echo $before . __('Daily Archives : ','vbegy') . get_the_time('d') . $after;
        }elseif (is_month()) {
            echo $before . __('Monthly Archives : ','vbegy') . get_the_time('F') . $after;
        }elseif (is_year()) {
            echo $before . __('Yearly Archives : ','vbegy') . get_the_time('Y') . $after;
        }elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post' && get_post_type() != 'question') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo $before . get_the_title() . $after;
            }else {
            	if (get_post_type() != 'question') {
                	$cat = get_the_category(); $cat = $cat[0];
                	echo $before . get_the_title() . $after;
                }else {
					$cat = get_the_category(); $cat = $cat[0];
					echo $before . get_the_title() . $after;
                }
            }
        }elseif (!is_single() && !is_page() && get_post_type() != 'post' && get_post_type() != 'question') {
        	if (is_author()) {
        	    global $author;
				$userdata = get_userdata($author);
				echo $before . $userdata->display_name . $after;
        	}else {
				$post_type = get_post_type_object(get_post_type());
				echo $before . (isset($post_type->labels->singular_name)?$post_type->labels->singular_name:__("Error 404","vbegy")) . $after;
        	}
        	
        }elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            echo $before . get_the_title() . $after;
        }elseif (is_page() && !$post->post_parent) {
            echo $before . get_the_title() . $after;
        }elseif (is_page() && $post->post_parent) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
            echo $before . get_the_title() . $after;
        }elseif (is_search()) {
            echo $before . get_search_query() . $after;
        }elseif (is_tag()) {
            echo $before . single_tag_title('', false) . $after;
        }elseif ( is_author() ) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . $userdata->display_name . $after;
        }elseif (is_404()) {
            echo $before . __('Error 404 ', 'vbegy') . $after;
        }
        $before     = '<span class="current">';
        $after      = '</span>';
        echo '<div class="clearfix"></div>
        <div class="crumbs">
        <a itemprop="breadcrumb" href="' . $homeLink . '">' . $home . '</a>' . $delimiter . ' ';
        if (is_category() || is_tag() || is_tax()) {
            global $wp_query;
            $term = $wp_query->get_queried_object();
        	$taxonomy = get_taxonomy( $term->taxonomy );
        	if ( ( is_taxonomy_hierarchical( $term->taxonomy ) && $term->parent ) && $parents = breadcrumbs_plus_get_term_parents( $term->parent, $term->taxonomy ) )
        		$item = array_merge( $item, $parents );
        	$item['last'] = $term->name;
            echo $before . '' . single_cat_title('', false) . '' . $after;
        }elseif (is_day()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $delimiter . '';
            echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a>' . $delimiter . '';
            echo $before . get_the_time('d') . $after;
        }elseif (is_month()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $delimiter . '';
            echo $before . get_the_time('F') . $after;
        }elseif (is_year()) {
            echo $before . get_the_time('Y') . $after;
        }elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                if (get_post_type() == 'question') {
	                $question_category = wp_get_post_terms($post->ID,'question-category',array("fields" => "all"));
	                if (isset($question_category[0])) {?>
		                <a href="<?php echo get_term_link($question_category[0]->slug, "question-category");?>"><?php echo $question_category[0]->name?></a>
	                <?php
	                }
	                echo $delimiter;
                }else {
	                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>' . $delimiter . '';
                }
                echo "".$before . get_the_title() . $after;
            }else {
                $cat = get_the_category(); $cat = $cat[0];
                echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                echo $before . get_the_title() . $after;
            }
        }elseif (!is_single() && !is_page() && get_post_type() != 'post') {
            if (is_author()) {
                global $author;
				$userdata = get_userdata($author);
				echo $before . $userdata->display_name . $after;
            }else {
	            $post_type = get_post_type_object(get_post_type());
            	echo $before . (isset($post_type->labels->singular_name)?$post_type->labels->singular_name:__("Error 404","vbegy")) . $after;
            }
        }elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>' . $delimiter . '';
            echo $before . get_the_title() . $after;
        }elseif (is_page() && !$post->post_parent) {
            echo $before . get_the_title() . $after;
        }elseif (is_page() && $post->post_parent) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
            echo $before . get_the_title() . $after;
        }elseif (is_search()) {
            echo $before . __('Search results for ', 'vbegy') . '"' . get_search_query() . '"' . $after;
        }elseif (is_tag()) {
            echo $before . __('Posts tagged ', 'vbegy') . '"' . single_tag_title('', false) . '"' . $after;
        }elseif ( is_author() ) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . $userdata->display_name . $after;
        }elseif (is_404()) {
            echo $before . __('Error 404 ', 'vbegy') . $after;
        }
        if (get_query_var('paged')) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
            echo "<span class='crumbs-span'>/</span><span class='current'>".__('Page', 'vbegy') . ' ' . get_query_var('paged')."</span>";
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }
        echo '</div></div></div></section></div>';
    }
}
/* breadcrumbs_plus_get_term_parents */
function breadcrumbs_plus_get_term_parents( $parent_id = '', $taxonomy = '', $separator = '/' ) {
	$html = array();
	$parents = array();
	if ( empty( $parent_id ) || empty( $taxonomy ) )
		return $parents;
	while ( $parent_id ) {
		$parent = get_term( $parent_id, $taxonomy );
		$parents[] = '<a href="' . get_term_link( $parent, $taxonomy ) . '" title="' . esc_attr( $parent->name ) . '">' . $parent->name . '</a>';
		$parent_id = $parent->parent;
	}
	if ( $parents )
		$parents = array_reverse( $parents );
	return $parents;
}
/* vpanel_show_extra_profile_fields */
add_action( 'show_user_profile', 'vpanel_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'vpanel_show_extra_profile_fields' );
function vpanel_show_extra_profile_fields( $user ) { ?>
	<table class="form-table">
		<tr>
			<th><label for="you_avatar"><?php _e("Your avatar","vbegy")?></label></th>
			<td>
				<input type="text" size="36" class="upload upload_meta regular-text" value="<?php echo esc_attr( get_the_author_meta('you_avatar', $user->ID ) ); ?>" id="you_avatar" name="you_avatar">
				<input id="you_avatar_button" class="upload_image_button button upload-button-2" type="button" value="Upload Image">
			</td>
		</tr>
		<?php if (get_the_author_meta('you_avatar', $user->ID )) {?>
		<tr>
			<th><label><?php _e("Your avatar","vbegy")?></label></th>
			<td>
				<div class="you_avatar"><img alt="" src="<?php echo esc_attr( get_the_author_meta('you_avatar', $user->ID ) ); ?>"></div>
			</td>
		</tr>
		<tr>
			<th><label for="country"><?php _e("Country","vbegy")?></label></th>
			<td>
				<input type="text" name="country" id="country" value="<?php echo esc_attr( get_the_author_meta( 'country', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<?php } ?>
	<h3><?php _e( 'Social Networking', 'vbegy' ) ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="google"><?php _e("Google +","vbegy")?></label></th>
			<td>
				<input type="text" name="google" id="google" value="<?php echo esc_attr( get_the_author_meta( 'google', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="twitter"><?php _e("Twitter","vbegy")?></label></th>
			<td>
				<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="facebook"><?php _e("Facebook","vbegy")?></label></th>
			<td>
				<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="linkedin"><?php _e("linkedin","vbegy")?></label></th>
			<td>
				<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="follow_email"><?php _e("Follow-up email","vbegy")?></label></th>
			<td>
				<input type="text" name="follow_email" id="follow_email" value="<?php echo esc_attr( get_the_author_meta( 'follow_email', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
	</table>
<?php }
/* Save user's meta */
add_action( 'personal_options_update', 'vpanel_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'vpanel_save_extra_profile_fields' );
function vpanel_save_extra_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) return false;
	update_user_meta( $user_id, 'google', esc_attr($_POST['google'] ));
	update_user_meta( $user_id, 'twitter', esc_attr($_POST['twitter']) );
	update_user_meta( $user_id, 'facebook', esc_attr($_POST['facebook']) );
	update_user_meta( $user_id, 'linkedin', esc_attr($_POST['linkedin']) );
	update_user_meta( $user_id, 'follow_email', esc_attr($_POST['follow_email']) );
	if (isset($_POST['you_avatar'])) {
		update_user_meta( $user_id, 'you_avatar', esc_attr($_POST['you_avatar']) );
	}
	update_user_meta( $user_id, 'country', esc_attr($_POST['country']) );
}
/* count_user_posts_by_type */
function count_user_posts_by_type( $userid, $post_type = 'post' ) {
	global $wpdb;
	$where = get_posts_by_author_sql( $post_type, true, $userid );
	$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts $where" );
  	return apply_filters( 'get_usernumposts', $count, $userid );
}
?>