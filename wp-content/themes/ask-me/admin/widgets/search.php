<?php
/* search */
add_action( 'widgets_init', 'widget_search_widget' );
function widget_search_widget() {
	register_widget( 'Widget_Search' );
}
class Widget_Search extends WP_Widget {

	function Widget_Search() {
		$widget_ops = array( 'classname' => 'search-widget'  );
		$control_ops = array( 'id_base' => 'search-widget' );
		$this->WP_Widget( 'search-widget','Ask me - search', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$title	   = apply_filters('widget_title', $instance['title'] );

		echo $before_widget;
			if ( $title )
				echo $before_title.esc_attr($title).$after_title;?>
			<div class="widget_search">
				<form role="search" method="get" class="searchform" action="<?php echo home_url('/'); ?>">
					<input type="text" value="<?php if (get_search_query() != "") {echo the_search_query();}else {_e("Search here ...","vbegy");}?>" onfocus="if(this.value=='<?php _e("Search here ...","vbegy");?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e("Search here ...","vbegy");?>';" name="s">
					<button type="submit" class="search-submit"></button>
				</form>
			</div>
		<?php echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance			   = $old_instance;
		$instance['title']	   = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' => 'search');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title : </label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo (isset($instance['title'])?esc_attr($instance['title']):"");?>" class="widefat" type="text">
		</p>
	<?php
	}
}
?>