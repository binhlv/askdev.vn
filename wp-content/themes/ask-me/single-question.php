<?php get_header();
	if ( have_posts() ) : while ( have_posts() ) : the_post();
		$question_vote = get_post_meta($post->ID,"question_vote",true);
		$question_category = wp_get_post_terms($post->ID,'question-category',array("fields" => "all"));
		$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
		$vbegy_sidebar = rwmb_meta('vbegy_sidebar','select',$post->ID);
		$video_id = rwmb_meta('vbegy_video_question_id',"select",$post->ID);
		$video_type = rwmb_meta('vbegy_video_question_type',"text",$post->ID);
		if ($video_type == 'youtube') {
			$type = "http://www.youtube.com/embed/".$video_id;
		}else if ($video_type == 'vimeo') {
			$type = "http://player.vimeo.com/video/".$video_id;
		}else if ($video_type == 'daily') {
			$type = "http://www.dailymotion.com/swf/video/".$video_id;
		}
		$question_poll = get_post_meta($post->ID,'question_poll',true);
		$question_type = ($question_poll == 1?" question-type-poll":" question-type-normal");
		$the_best_answer = get_post_meta($post->ID,"the_best_answer",true);
		$closed_question = get_post_meta($post->ID,"closed_question",true);
		$question_favorites = get_post_meta($id,'question_favorites',true);
		
		$the_author = get_user_by("login",get_the_author());
		$user_login_id_l = get_user_by("id",$post->post_author);
		if ($post->post_author != 0) {
			$user_profile_page = add_query_arg("u", $user_login_id_l->user_login,get_page_link(vpanel_options('user_profile_page')));
		}
		$get_question_category = get_option("questions_category_".$question_category[0]->term_id);
		$yes_private = 0;
		if (isset($question_category[0]) && isset($get_question_category['private']) && $get_question_category['private'] == "on") {
			if (isset($authordata->ID) && $authordata->ID > 0 && $authordata->ID == get_current_user_id()) {
				$yes_private = 1;
			}
		}else if (isset($question_category[0]) && !isset($get_question_category['private']) && $question_category[0]->parent == 0) {
			$yes_private = 1;
		}
		if (isset($question_category[0]) && $question_category[0]->parent > 0) {
			$get_question_category_parent = get_option("questions_category_".$question_category[0]->parent);
			if (isset($get_question_category_parent[0]) && isset($get_question_category_parent['private']) && $get_question_category_parent['private'] == "on" && isset($authordata->ID) && $authordata->ID > 0 && $authordata->ID == get_current_user_id()) {
				$yes_private = 1;
			}else if (isset($question_category[0]) && isset($get_question_category_parent['private']) && $get_question_category_parent['private'] == "on" && !isset($authordata->ID)) {
				$yes_private = 0;
			}
		}
		if (is_super_admin(get_current_user_id())) {
			$yes_private = 1;
		}
		if ($yes_private == 1) {?>
			<article <?php post_class('question single-question'.$question_type);?> id="post-<?php the_ID();?>" role="article" itemscope="" itemtype="http://schema.org/Article">
				<h2 itemprop="name">
					<?php if ($post->post_author != 0 && $user_login_id_l->ID == get_current_user_id()) {?>
						<span class="question-edit">
							<a href="<?php echo add_query_arg("q", $post->ID,get_page_link(vpanel_options('edit_question')))?>" original-title="<?php _e("Edit the question","vbegy")?>" class="tooltip-n"><i class="icon-edit"></i></a>
						</span>
					<?php }
					if (($post->post_author != 0 && $user_login_id_l->ID == get_current_user_id()) || is_super_admin(get_current_user_id())) {
						if (isset($closed_question) && $closed_question == 1) {?>
							<span class="question-open">
								<a href="#" original-title="<?php _e("Open the question","vbegy")?>" class="tooltip-n"><i class="icon-unlock"></i></a>
							</span>
						<?php }else {?>
							<span class="question-close">
								<a href="#" original-title="<?php _e("Close the question","vbegy")?>" class="tooltip-n"><i class="icon-lock"></i></a>
							</span>
						<?php }
					}
					the_title();?>
				</h2>
				<a class="question-report report_q" href="#"><?php _e("Report","vbegy")?></a>
				<?php if ($question_poll == 1) {?>
					<div class="question-type-main"><i class="icon-signal"></i><?php _e("Poll","vbegy")?></div>
				<?php }else {?>
					<div class="question-type-main"><i class="icon-question-sign"></i><?php _e("Question","vbegy")?></div>
				<?php }?>
				<div class="question-inner">
					<div class="clearfix"></div>
					<div class="question-desc" itemprop="mainContentOfPage">
						<div class="explain-reported">
							<h3><?php _e("Please briefly explain why you feel this question should be reported .","vbegy")?></h3>
							<textarea name="explain-reported"></textarea>
							<div class="clearfix"></div>
							<div class="loader_3"></div>
							<a class="color button small report"><?php _e("Report","vbegy")?></a>
							<a class="color button small dark_button cancel"><?php _e("Cancel","vbegy")?></a>
						</div><!-- End reported -->
						<?php
						if ($question_poll == 1) {?>
							<div class='question_poll_end'>
								<?php
								$question_poll_num = get_post_meta($post->ID,'question_poll_num',true);
								$custom = get_post_custom($post->ID);
								$asks = unserialize($custom["ask"][0]);
								if ($asks) {
									$i = 0;?>
									<div class="poll_1" <?php if (!isset($_COOKIE['question_poll'.$post->ID])) {echo ' style="display:none"';} ?>>
										<div class="progressbar-warp">
											<?php foreach($asks as $ask):$i++;
												if ($question_poll_num != "" or $question_poll_num != 0) {
													$value_poll = round(($ask['value']/$question_poll_num)*100,2);
												}?>
												<span class="progressbar-title"><?php echo stripslashes($ask['title'])?> <?php echo ($question_poll_num == 0?0:$value_poll)?>%<span><?php echo ($ask['value'] != ""?"( ".stripslashes($ask['value'])." ".__("voter","vbegy")." )":"")?></span></span>
												<div class="progressbar">
												    <div class="progressbar-percent <?php echo ($ask['value'] == 0?"poll-result":"")?>" <?php echo ($ask['value'] == 0?"":"style='background-color: #3498db;'")?> attr-percent="<?php echo ($ask['value'] == 0?100:$value_poll)?>"></div>
												</div>
											<?php endforeach;?>
										</div><!-- End progressbar-warp -->
										<?php if (!isset($_COOKIE['question_poll'.$post->ID])) { ?><a href='#' class='color button small poll_polls margin_0'><?php _e("Rating","vbegy")?></a><?php }?>
									</div>
									<div class="clear"></div>
									<div class="poll_2"><div class="loader_3"></div>
										<div class="form-style form-style-3">
											<div class="form-inputs clearfix">
												<?php if (!isset($_COOKIE['question_poll'.$post->ID])) {
													foreach($asks as $ask):$i++;
														?>
														<p>
															<input id="ask[<?php echo $i?>][title]" name="ask_radio" type="radio" value="poll_<?php echo (int)$ask['id']?>" rel="poll_<?php echo stripslashes($ask['title'])?>">
															<label for="ask[<?php echo $i?>][title]"><?php echo stripslashes($ask['title'])?></label>
														</p>
													<?php endforeach;
												}?>
											</div>
										</div>
										<?php if (!isset($_COOKIE['question_poll'.$post->ID])) { ?><a href='#' class='color button small poll_results margin_0'><?php _e("Results","vbegy")?></a><?php }?>
									</div>
								<?php }?>
							</div><!-- End question_poll_end -->
							<div class="clearfix height_20"></div>
							<?php
						}
						$video_description = get_post_meta($post->ID,'video_description',true);
						if (vpanel_options("video_desc_active") == 1 && $video_description == 1) {
							$video_desc = get_post_meta($post->ID,'video_desc',true);
							$video_desc = get_post_meta($post->ID,'video_desc',true);
							$video_id = get_post_meta($post->ID,'video_id',true);
							$video_type = get_post_meta($post->ID,'video_type',true);
							if ($video_id != "") {
								if ($video_type == 'youtube') {
									$type = "http://www.youtube.com/embed/".$video_id;
								}else if ($video_type == 'vimeo') {
									$type = "http://player.vimeo.com/video/".$video_id;
								}else if ($video_type == 'daily') {
									$type = "http://www.dailymotion.com/swf/video/".$video_id;
								}
								if ($vbegy_sidebar == "full") {
							    	$las_video = '<div class="question-video"><iframe height="600" src="'.$type.'"></iframe></div>';
								}else {
							    	$las_video = '<div class="question-video"><iframe height="450" src="'.$type.'"></iframe></div>';
								}
								if (vpanel_options("video_desc") == "before") {
									echo $las_video;
								}
							}
						}
						the_content();
						if (vpanel_options("video_desc") == "after" && vpanel_options("video_desc_active") == 1 && $video_id != "" && $video_description == 1) {
							echo $las_video;
						}
						$comments = get_comments('post_id='.$post->ID);
						if (is_user_logged_in()) {
							$user_login_id2 = get_user_by("id",get_current_user_id());
							$_favorites = get_user_meta(get_current_user_id(),$user_login_id2->user_login."_favorites");
							if (isset($_favorites[0])) {
								if (in_array($post->ID,$_favorites[0])) {?>
									<a class="remove_favorite add_favorite_in color button small margin_0" title="<?php _e("Remove the question of my favorites","vbegy")?>" href="#"><?php _e("Remove the question of my favorites","vbegy")?></a>
								<?php }else {?>
									<a class="add_favorite add_favorite_in color button small margin_0" title="<?php _e("Add a question to Favorites","vbegy")?>" href="#"><?php _e("Add a question to Favorites","vbegy")?></a>
								<?php
								}
							}else {echo '<a class="add_favorite add_favorite_in color button small margin_0" title="'.__("Add a question to Favorites","vbegy").'" href="#">'.__("Add a question to Favorites","vbegy").'</a>';}
							if (empty($comments) && get_current_user_id() == $post->post_author && $post->post_author != 0) {?>
								<div class="form-style form-style-2 form-add-point">
									<p class="clearfix">
										<input id="input-add-point" name="" type="text" placeholder="<?php _e("Question bump points","vbegy")?>">
										<a class="color button small margin_0 f_left" href="#"><?php _e("Bump","vbegy")?></a>
									</p>
								</div>
							<?php }
						}?>
						<div class="loader_2"></div>
						<?php
						$added_file = get_post_meta($post->ID, 'added_file', true);
						if ($added_file != "") {
							echo "<div class='clear'></div><br><a href='".wp_get_attachment_url($added_file)."'>".__("Attachment","vbegy")."</a>";
						}
						?>
						<div class="no_vote_more"></div>
					</div>
					<div class="question-details">
						<?php if (isset($the_best_answer) && $the_best_answer != "" && $comments) {?>
							<span class="question-answered question-answered-done"><i class="icon-ok"></i><?php _e("solved","vbegy")?></span>
						<?php }else if (isset($closed_question) && $closed_question == 1) {?>
							<span class="question-answered question-closed"><i class="icon-lock"></i><?php _e("closed","vbegy")?></span>
						<?php }else if ($the_best_answer == "" && $comments) {?>
							<span class="question-answered"><i class="icon-ok"></i><?php _e("in progress","vbegy")?></span>
						<?php }?>
						<span class="question-favorite"><i class="<?php echo ($question_favorites > 0?"icon-star":"icon-star-empty");?>"></i><?php echo ($question_favorites != ""?$question_favorites:0);?></span>
					</div>
					<?php if (isset($question_category[0])) {?>
					<span class="question-category"><a href="<?php echo get_term_link($question_category[0]->slug, "question-category");?>"><i class="icon-folder-close"></i><?php echo $question_category[0]->name?></a></span>
					<?php }
					if ($post->post_author == 0) {
						$question_username = get_post_meta($post->ID, 'question_username',true);
						$question_email = get_post_meta($post->ID, 'question_email',true);
						?>
						<span class="question-category"><i class="icon-user"></i><?php echo $question_username?></span>
					<?php }?>
					<span class="question-date" datetime="<?php the_time('c'); ?>" itemprop="datePublished"><i class="icon-time"></i>
					<?php echo human_time_diff(get_the_time('U'), current_time('timestamp'));?></span>
					<span class="question-comment"><a href="<?php echo comments_link()?>"><i class="icon-comment"></i><?php echo get_comments_number()?> <?php _e("Answer","vbegy");?></a></span>
					<meta itemprop="interactionCount" content="<?php comments_number( 'UserAnswers: 0', 'UserAnswers: 1', 'UserAnswers: %' ); ?>">
					<span class="question-view"><i class="icon-user"></i><?php echo get_post_meta($post->ID, 'post_stats', true);?> <?php _e("views","vbegy");?></span>
					
					<span class="single-question-vote-result question_vote_result"><?php echo ($question_vote != ""?$question_vote:0)?></span>
					<ul class="single-question-vote">
						<?php if (is_user_logged_in()){?>
							<li><a href="#" id="question_vote_up-<?php echo $post->ID?>" class="single-question-vote-up question_vote_up<?php echo (isset($_COOKIE['question_vote'.$post->ID])?" ".$_COOKIE['question_vote'.$post->ID]."-".$post->ID:"")?> tooltip_s" title="<?php _e("Like","vbegy");?>"><i class="icon-thumbs-up"></i></a></li>
							<li><a href="#" id="question_vote_down-<?php echo $post->ID?>" class="single-question-vote-down question_vote_down<?php echo (isset($_COOKIE['question_vote'.$post->ID])?" ".$_COOKIE['question_vote'.$post->ID]."-".$post->ID:"")?> tooltip_s" title="<?php _e("Dislike","vbegy");?>"><i class="icon-thumbs-down"></i></a></li>
						<?php }else { ?>
							<li><a href="#" class="single-question-vote-up question_vote_up vote_not_user tooltip_s" original-title="<?php _e("Like","vbegy");?>"><i class="icon-thumbs-up"></i></a></li>
							<li><a href="#" class="single-question-vote-down question_vote_down vote_not_user tooltip_s" original-title="<?php _e("Dislike","vbegy");?>"><i class="icon-thumbs-down"></i></a></li>
						<?php }?>
					</ul>
					<div class="clearfix"></div>
				</div>
			</article>
			
			<div class="share-tags page-content">
				<?php
				if ($terms = wp_get_object_terms( $post->ID, 'question_tags' )) :
					echo '<div class="question-tags"><i class="icon-tags"></i>';
						$terms_array = array();
						foreach ($terms as $term) :
							$terms_array[] = '<a href="'.get_term_link($term->slug, 'question_tags').'">'.$term->name.'</a>';
						endforeach;
						echo implode(' , ', $terms_array);
					echo '</div>';
				endif;
				$post_share = vpanel_options("post_share");
				if ($post_share == 1) {?>
					<div class="share-inside-warp">
						<ul>
							<li>
								<a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>" target="_blank">
									<span class="icon_i">
										<span class="icon_square" icon_size="20" span_bg="#3b5997" span_hover="#666">
											<i i_color="#FFF" class="social_icon-facebook"></i>
										</span>
									</span>
								</a>
								<a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>" target="_blank"><?php _e("Facebook","vbegy");?></a>
							</li>
							<li>
								<a href="http://twitter.com/home?status=<?php echo urlencode(get_permalink());?>" target="_blank">
									<span class="icon_i">
										<span class="icon_square" icon_size="20" span_bg="#00baf0" span_hover="#666">
											<i i_color="#FFF" class="social_icon-twitter"></i>
										</span>
									</span>
								</a>
								<a target="_blank" href="http://twitter.com/home?status=<?php echo urlencode(get_permalink());?>"><?php _e("Twitter","vbegy");?></a>
							</li>
							<li>
								<a href="http://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>" target="_blank">
									<span class="icon_i">
										<span class="icon_square" icon_size="20" span_bg="#ca2c24" span_hover="#666">
											<i i_color="#FFF" class="social_icon-gplus"></i>
										</span>
									</span>
								</a>
								<a href="http://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>" target="_blank"><?php _e("Google plus","vbegy");?></a>
							</li>
							<li>
								<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(get_permalink()) ?>&amp;name=<?php echo urlencode(get_the_title()) ?>" target="_blank">
									<span class="icon_i">
										<span class="icon_square" icon_size="20" span_bg="#44546b" span_hover="#666">
											<i i_color="#FFF" class="social_icon-tumblr"></i>
										</span>
									</span>
								</a>
								<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(get_permalink()) ?>&amp;name=<?php echo urlencode(get_the_title()) ?>" target="_blank"><?php _e("Tumblr","vbegy");?></a>
							</li>
							<li>
								<a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php 
									echo urlencode(get_permalink());?>&amp;media=<?php echo urlencode(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>">
									<span class="icon_i">
										<span class="icon_square" icon_size="20" span_bg="#c7151a" span_hover="#666">
											<i i_color="#FFF" class="icon-pinterest"></i>
										</span>
									</span>
								</a>
								<a href="http://pinterest.com/pin/create/button/?url=<?php 
									echo urlencode(get_permalink());?>&amp;media=<?php echo urlencode(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>" target="_blank"><?php _e("Pinterest","vbegy");?></a>
							</li>
						</ul>
						<span class="share-inside-f-arrow"></span>
						<span class="share-inside-l-arrow"></span>
					</div><!-- End share-inside-warp -->
					<div class="share-inside"><i class="icon-share-alt"></i><?php _e("Share","vbegy");?></div>
				<?php }?>
				<div class="clearfix"></div>
			</div><!-- End share-tags -->
			
			<?php
			$vbegy_share_adv_type = rwmb_meta('vbegy_share_adv_type','radio',$post->ID);
			$vbegy_share_adv_code = rwmb_meta('vbegy_share_adv_code','textarea',$post->ID);
			$vbegy_share_adv_href = rwmb_meta('vbegy_share_adv_href','text',$post->ID);
			$vbegy_share_adv_img = rwmb_meta('vbegy_share_adv_img','upload',$post->ID);
			
			if ((is_single() || is_page()) && (($vbegy_share_adv_type == "display_code" && $vbegy_share_adv_code != "") || ($vbegy_share_adv_type == "custom_image" && $vbegy_share_adv_img != ""))) {
				$share_adv_type = $vbegy_share_adv_type;
				$share_adv_code = $vbegy_share_adv_code;
				$share_adv_href = $vbegy_share_adv_href;
				$share_adv_img = $vbegy_share_adv_img;
			}else {
				$share_adv_type = vpanel_options("share_adv_type");
				$share_adv_code = vpanel_options("share_adv_code");
				$share_adv_href = vpanel_options("share_adv_href");
				$share_adv_img = vpanel_options("share_adv_img");
			}
			if (($share_adv_type == "display_code" && $share_adv_code != "") || ($share_adv_type == "custom_image" && $share_adv_img != "")) {
				echo '<div class="clearfix"></div>
				<div class="advertising">';
				if ($share_adv_type == "display_code") {
					echo stripcslashes($share_adv_code);
				}else {
					if ($share_adv_href != "") {
						echo '<a href="'.$share_adv_href.'">';
					}
					echo '<img alt="" src="'.$share_adv_img.'">';
					if ($share_adv_href != "") {
						echo '</a>';
					}
				}
				echo '</div><!-- End advertising -->
				<div class="clearfix"></div>';
			}
			
			$post_author_box = vpanel_options("post_author_box");
			if ($post->post_author != 0 && $post_author_box == 1) {?>
				<div class="about-author clearfix">
				    <div class="author-image">
				    	<a href="<?php echo get_author_posts_url($authordata->ID,$authordata->nickname);?>" original-title="<?php the_author();?>" class="tooltip-n">
				    		<?php 
				    		if (get_the_author_meta('you_avatar', get_the_author_meta('ID'))) {
				    			$you_avatar_img = get_aq_resize_url(esc_attr(get_the_author_meta('you_avatar', get_the_author_meta('ID'))),"full",65,65);
				    			echo "<img alt='".$authordata->display_name."' src='".$you_avatar_img."'>";
				    		}else {
				    			echo get_avatar(get_the_author_meta('user_email'),'65','');
				    		}?>
				    	</a>
				    </div>
				    <div class="author-bio">
				        <h4><?php _e("About the Author","vbegy");?></h4>
				        <?php the_author_meta('description');?>
				    </div>
				</div><!-- End about-author -->
			<?php }?>
			
			<?php
			if (vpanel_options("related_post") == 1) {
				$related_no = vpanel_options('related_number') ? vpanel_options('related_number') : 5;
				global $post;
				$orig_post = $post;
				$categories = wp_get_post_terms($post->ID,'question-category',array("fields" => "ids"));
				$args = array('post_type' => 'question','post__not_in' => array($post->ID),'posts_per_page'=> $related_no , 'tax_query' => array(array('taxonomy' => 'question-category','field' => 'id','terms' => $categories,'operator' => 'IN')));
				$related_query = new wp_query( $args );
				if ($related_query->have_posts()) : ;?>
					<div id="related-posts">
						<h2><?php _e("Related questions","vbegy");?></h2>
						<ul class="related-posts">
							<?php while ( $related_query->have_posts() ) : $related_query->the_post()?>
								<li class="related-item"><h3><a  href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>"><i class="icon-double-angle-right"></i><?php the_title();?></a></h3></li>
							<?php endwhile;?>
						</ul>
					</div><!-- End related-posts -->
				<?php endif;
				$post = $orig_post;
				wp_reset_query();
			}
			
			$post_comments = vpanel_options("post_comments");
			if ($post_comments == 1) {
				comments_template("/question-comments.php");
			}
			
			$post_navigation = vpanel_options("post_navigation");
			if ($post_navigation == 1) {?>
				<div class="post-next-prev clearfix">
				    <p class="prev-post">
				        <?php previous_post_link('%link','<i class="icon-double-angle-left"></i>'.__('&nbsp;Previous question','vbegy')); ?>
				    </p>
				    <p class="next-post">
				    	<?php next_post_link('%link',__('Next question&nbsp;','vbegy').'<i class="icon-double-angle-right"></i>'); ?>
				    </p>
				</div><!-- End post-next-prev -->
			<?php }
		}else {?>
			<article class="question private-question">
				<p class="question-desc"><?php _e("Sorry it a private question .");?></p>
			</article>
		<?php }
	endwhile; endif;
get_footer();?>