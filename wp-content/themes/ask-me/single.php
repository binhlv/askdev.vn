<?php get_header();
	$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
    $vbegy_sidebar = rwmb_meta('vbegy_sidebar','select',$post->ID);
    $vbegy_google = rwmb_meta('vbegy_google',"textarea",$post->ID);
    $video_id = rwmb_meta('vbegy_video_post_id',"select",$post->ID);
    $video_type = rwmb_meta('vbegy_video_post_type',"text",$post->ID);
    $vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
    if ($video_type == 'youtube') {
    	$type = "http://www.youtube.com/embed/".$video_id;
    }else if ($video_type == 'vimeo') {
    	$type = "http://player.vimeo.com/video/".$video_id;
    }else if ($video_type == 'daily') {
    	$type = "http://www.dailymotion.com/swf/video/".$video_id;
    }
	if ( have_posts() ) : while ( have_posts() ) : the_post();?>
		<article <?php post_class('post single-post');?> id="post-<?php the_ID();?>" role="article" itemscope="" itemtype="http://schema.org/Article">
			<div class="post-inner">
				<div class="post-img<?php if ($vbegy_what_post == "image" && !has_post_thumbnail()) {echo " post-img-0";}else if ($vbegy_what_post == "video") {echo " video_embed";}if ($vbegy_sidebar == "full") {echo " post-img-12";}else {echo " post-img-9";}?>">
					<?php include (get_template_directory() . '/includes/head.php');?>
				</div>
	        	<h2 itemprop="name" class="post-title">
	        		<?php if ($vbegy_what_post == "google") {?>
	        			<span class="post-type"><i class="icon-map-marker"></i></span>
	        		<?php }else if ($vbegy_what_post == "video") {?>
	        			<span class="post-type"><i class="icon-play-circle"></i></span>
	        		<?php }else if ($vbegy_what_post == "slideshow") {?>
	        			<span class="post-type"><i class="icon-film"></i></span>
	        		<?php }else {
	        			if (has_post_thumbnail()) {?>
	        		    	<span class="post-type"><i class="icon-picture"></i></span>
	        			<?php }else {?>
	        		    	<span class="post-type"><i class="icon-file-alt"></i></span>
	        			<?php }
	        		}
	        		the_title();?>
	        	</h2>
				<?php $posts_meta = vpanel_options("post_meta");
				if ($posts_meta == 1) {?>
					<div class="post-meta">
					    <span class="meta-author" itemprop="author" rel="author"><i class="icon-user"></i><?php the_author_posts_link();?></span>
					    <span class="meta-date" datetime="<?php the_time('c'); ?>" itemprop="datePublished"><i class="icon-time"></i><?php the_time('F j , Y');?></span>
					    <span class="meta-categories"><i class="icon-suitcase"></i><?php the_category(' , ');?></span>
					    <span class="meta-comment"><i class="icon-comments-alt"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
					    <meta itemprop="interactionCount" content="<?php comments_number( 'UserComments: 0', 'UserComments: 1', 'UserComments: %' ); ?>">
					</div>
				<?php }?>
				<div class="post-content" itemprop="mainContentOfPage">
					<?php the_content();?>
				</div>
				
				<?php  wp_link_pages(array('before' => '<div class="pagination post-pagination">','after' => '</div>','link_before' => '<span>','link_after' => '</span>'));?>
				<div class="clearfix"></div>
				
			</div><!-- End post-inner -->
		</article><!-- End article.post -->
		
		<div class="share-tags page-content">
			<?php if (has_tag()) {?>
				<div class="post-tags"><i class="icon-tags"></i>
					<?php the_tags('',' , ','');?>
				</div>
			<?php }
			$post_share = vpanel_options("post_share");
			if ($post_share == 1) {?>
				<div class="share-inside-warp">
					<ul>
						<li>
							<a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>" target="_blank">
								<span class="icon_i">
									<span class="icon_square" icon_size="20" span_bg="#3b5997" span_hover="#666">
										<i i_color="#FFF" class="social_icon-facebook"></i>
									</span>
								</span>
							</a>
							<a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>" target="_blank"><?php _e("Facebook","vbegy");?></a>
						</li>
						<li>
							<a href="http://twitter.com/home?status=<?php echo urlencode(get_permalink());?>" target="_blank">
								<span class="icon_i">
									<span class="icon_square" icon_size="20" span_bg="#00baf0" span_hover="#666">
										<i i_color="#FFF" class="social_icon-twitter"></i>
									</span>
								</span>
							</a>
							<a target="_blank" href="http://twitter.com/home?status=<?php echo urlencode(get_permalink());?>"><?php _e("Twitter","vbegy");?></a>
						</li>
						<li>
							<a href="http://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>" target="_blank">
								<span class="icon_i">
									<span class="icon_square" icon_size="20" span_bg="#ca2c24" span_hover="#666">
										<i i_color="#FFF" class="social_icon-gplus"></i>
									</span>
								</span>
							</a>
							<a href="http://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>" target="_blank"><?php _e("Google plus","vbegy");?></a>
						</li>
						<li>
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(get_permalink()) ?>&amp;name=<?php echo urlencode(get_the_title()) ?>" target="_blank">
								<span class="icon_i">
									<span class="icon_square" icon_size="20" span_bg="#44546b" span_hover="#666">
										<i i_color="#FFF" class="social_icon-tumblr"></i>
									</span>
								</span>
							</a>
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(get_permalink()) ?>&amp;name=<?php echo urlencode(get_the_title()) ?>" target="_blank"><?php _e("Tumblr","vbegy");?></a>
						</li>
						<li>
							<a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php 
								echo urlencode(get_permalink());?>&amp;media=<?php echo urlencode(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>">
								<span class="icon_i">
									<span class="icon_square" icon_size="20" span_bg="#c7151a" span_hover="#666">
										<i i_color="#FFF" class="icon-pinterest"></i>
									</span>
								</span>
							</a>
							<a href="http://pinterest.com/pin/create/button/?url=<?php 
								echo urlencode(get_permalink());?>&amp;media=<?php echo urlencode(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>" target="_blank"><?php _e("Pinterest","vbegy");?></a>
						</li>
					</ul>
					<span class="share-inside-f-arrow"></span>
					<span class="share-inside-l-arrow"></span>
				</div><!-- End share-inside-warp -->
				<div class="share-inside"><i class="icon-share-alt"></i><?php _e("Share","vbegy");?></div>
			<?php }?>
			<div class="clearfix"></div>
		</div><!-- End share-tags -->
		
		<?php
		$vbegy_share_adv_type = rwmb_meta('vbegy_share_adv_type','radio',$post->ID);
		$vbegy_share_adv_code = rwmb_meta('vbegy_share_adv_code','textarea',$post->ID);
		$vbegy_share_adv_href = rwmb_meta('vbegy_share_adv_href','text',$post->ID);
		$vbegy_share_adv_img = rwmb_meta('vbegy_share_adv_img','upload',$post->ID);
		
		if ((is_single() || is_page()) && (($vbegy_share_adv_type == "display_code" && $vbegy_share_adv_code != "") || ($vbegy_share_adv_type == "custom_image" && $vbegy_share_adv_img != ""))) {
			$share_adv_type = $vbegy_share_adv_type;
			$share_adv_code = $vbegy_share_adv_code;
			$share_adv_href = $vbegy_share_adv_href;
			$share_adv_img = $vbegy_share_adv_img;
		}else {
			$share_adv_type = vpanel_options("share_adv_type");
			$share_adv_code = vpanel_options("share_adv_code");
			$share_adv_href = vpanel_options("share_adv_href");
			$share_adv_img = vpanel_options("share_adv_img");
		}
		if (($share_adv_type == "display_code" && $share_adv_code != "") || ($share_adv_type == "custom_image" && $share_adv_img != "")) {
			echo '<div class="clearfix"></div>
			<div class="advertising">';
			if ($share_adv_type == "display_code") {
				echo stripcslashes($share_adv_code);
			}else {
				if ($share_adv_href != "") {
					echo '<a href="'.$share_adv_href.'">';
				}
				echo '<img alt="" src="'.$share_adv_img.'">';
				if ($share_adv_href != "") {
					echo '</a>';
				}
			}
			echo '</div><!-- End advertising -->
			<div class="clearfix"></div>';
		}
		
		$post_author_box = vpanel_options("post_author_box");
		if ($post_author_box == 1) {?>
			<div class="about-author clearfix">
				<div class="author-image">
					<a href="<?php echo get_author_posts_url($authordata->ID,$authordata->nickname);?>" original-title="<?php the_author();?>" class="tooltip-n">
						<?php 
						if (get_the_author_meta('you_avatar', get_the_author_meta('ID'))) {
							$you_avatar_img = get_aq_resize_url(esc_attr(get_the_author_meta('you_avatar', get_the_author_meta('ID'))),"full",65,65);
							echo "<img alt='".$authordata->display_name."' src='".$you_avatar_img."'>";
						}else {
							echo get_avatar(get_the_author_meta('user_email'),'65','');
						}?>
					</a>
				</div>
			    <div class="author-bio">
			        <h4><?php _e("About the Author","vbegy");?></h4>
			        <?php the_author_meta('description');?>
			    </div>
			</div><!-- End about-author -->
		<?php }
		
		if (vpanel_options("related_post") == 1) {
			$related_no = vpanel_options('related_number') ? vpanel_options('related_number') : 5;
			global $post;
			$orig_post = $post;
			
			$categories = get_the_category($post->ID);
			$category_ids = array();
			foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			$args=array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no , 'category__in'=> $category_ids );
			$related_query = new wp_query( $args );
			if ($related_query->have_posts()) : ;?>
				<div id="related-posts">
					<h2><?php _e("Related Posts","vbegy");?></h2>
					<ul class="related-posts">
						<?php while ( $related_query->have_posts() ) : $related_query->the_post()?>
							<li class="related-item"><h3><a  href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>"><i class="icon-double-angle-right"></i><?php the_title();?></a></h3></li>
						<?php endwhile;?>
					</ul>
				</div><!-- End related-posts -->
			<?php endif;
			$post = $orig_post;
			wp_reset_query();
		}
		
		$post_comments = vpanel_options("post_comments");
		if ($post_comments == 1) {
			comments_template();
		}
		
		$post_navigation = vpanel_options("post_navigation");
		if ($post_navigation == 1) {?>
			<div class="post-next-prev clearfix">
			    <p class="prev-post">
			        <?php previous_post_link('%link','<i class="icon-double-angle-left"></i>'.__('&nbsp;Previous post','vbegy')); ?>
			    </p>
			    <p class="next-post">
			    	<?php next_post_link('%link',__('Next post&nbsp;','vbegy').'<i class="icon-double-angle-right"></i>'); ?>
			    </p>
			</div><!-- End post-next-prev -->
		<?php }
		
	endwhile; endif;
get_footer();?>