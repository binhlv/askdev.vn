<?php
global $blog_style,$vbegy_sidebar;
$posts_meta = vpanel_options("post_meta");
if (have_posts() ) : while (have_posts() ) : the_post();
    $vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
    $vbegy_sidebar = rwmb_meta('vbegy_sidebar','select',$post->ID);
    $vbegy_google = rwmb_meta('vbegy_google',"textarea",$post->ID);
    $video_id = rwmb_meta('vbegy_video_post_id',"select",$post->ID);
    $video_type = rwmb_meta('vbegy_video_post_type',"text",$post->ID);
    $vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
    if ($video_type == 'youtube') {
    	$type = "http://www.youtube.com/embed/".$video_id;
    }else if ($video_type == 'vimeo') {
    	$type = "http://player.vimeo.com/video/".$video_id;
    }else if ($video_type == 'daily') {
    	$type = "http://www.dailymotion.com/swf/video/".$video_id;
    }?>
	<article <?php post_class('post  clearfix '.($blog_style == "blog_2"?"blog_2":"").(is_sticky()?" sticky_post":""));?> role="article" itemscope="" itemtype="http://schema.org/Article">
		<div class="post-inner">
			<?php if ($blog_style != "blog_2") {?>
				<div class="post-img<?php if ($vbegy_what_post == "image" && !has_post_thumbnail()) {echo " post-img-0";}else if ($vbegy_what_post == "video") {echo " video_embed";}if ($vbegy_sidebar == "full") {echo " post-img-12";}else {echo " post-img-9";}?>">
					<?php if (has_post_thumbnail() && $vbegy_what_post == "image") {?><a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php }
    					include (get_template_directory() . '/includes/head.php');?>
					<?php if (has_post_thumbnail()) {?></a><?php }?>
				</div>
	        <?php }?>
	        <h2 itemprop="name" class="post-title">
	        	<?php if ($vbegy_what_post == "google") {?>
    	        	<span class="post-type"><i class="icon-map-marker"></i></span>
	        	<?php }else if ($vbegy_what_post == "video") {?>
	        		<span class="post-type"><i class="icon-play-circle"></i></span>
	        	<?php }else if ($vbegy_what_post == "slideshow") {?>
	        		<span class="post-type"><i class="icon-film"></i></span>
	        	<?php }else {
	        		if (has_post_thumbnail()) {?>
	    	        	<span class="post-type"><i class="icon-picture"></i></span>
	        		<?php }else {?>
	    	        	<span class="post-type"><i class="icon-file-alt"></i></span>
	        		<?php }
	        	}?>
	        	<a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title();?></a>
	        </h2>
	        <?php
	        if ($blog_style == "blog_2") {?>
    	        <div class="post-img<?php if ($vbegy_what_post == "image" && !has_post_thumbnail()) {echo " post-img-0";}else if ($vbegy_what_post == "video") {echo " video_embed";}if ($vbegy_sidebar == "full") {echo " post-img-12";}else {echo " post-img-9";}?>">
    	        	<?php if (has_post_thumbnail() && $vbegy_what_post == "image") {?><a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php }
    	        		include (get_template_directory() . '/includes/head.php');?>
    	        	<?php if (has_post_thumbnail()) {?></a><?php }?>
    	        </div>
	        <?php }
	        if ($posts_meta == 1) {?>
	        	<div class="post-meta">
	        	    <span class="meta-author" itemprop="author" rel="author"><i class="icon-user"></i><?php the_author_posts_link();?></span>
	        	    <span class="meta-date" datetime="<?php the_time('c'); ?>" itemprop="datePublished"><i class="icon-time"></i><?php the_time('F j , Y');?></span>
	        	    <?php if (!is_page()) {?>
		        	    <span class="meta-categories"><i class="icon-suitcase"></i><?php the_category(' , ');?></span>
	        	    <?php }?>
	        	    <span class="meta-comment"><i class="icon-comments-alt"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
	        	    <meta itemprop="interactionCount" content="<?php comments_number( 'UserComments: 0', 'UserComments: 1', 'UserComments: %' ); ?>">
	        	</div>
	        <?php }?>
	        <div class="post-content">
	            <p><?php if ($blog_style == "blog_2") {excerpt(20);}else {excerpt(40);}?></p>
	            <a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark" class="post-read-more button color small"><?php _e("Continue reading","vbegy");?></a>
	        </div><!-- End post-content -->
	    </div><!-- End post-inner -->
	</article><!-- End article.post -->
<?php endwhile;else :?>
	<div class="error_404">
		<div>
			<h2><?php _e("404","vbegy")?></h2>
			<h3><?php _e("Page not Found","vbegy")?></h3>
		</div>
		<div class="clearfix"></div><br>
		<a href="<?php echo esc_url(home_url('/'));?>" class="button large color margin_0"><?php _e("Home Page","vbegy")?></a>
	</div>
<?php endif;?>