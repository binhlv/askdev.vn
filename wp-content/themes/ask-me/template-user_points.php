<?php /* Template name: User Points */
global $user_ID;
if(empty($_GET['u'])){
	wp_redirect(get_bloginfo("url"));
}
$user_login = get_userdata($_GET['u']);
if(empty($user_login)){
	wp_redirect(get_bloginfo("url"));
}
$owner = false;
if($user_ID == $user_login->ID){
	$owner = true;
}
if(vpanel_options("show_point_favorite") == 0 && $owner == false){
	wp_redirect(get_bloginfo("url"));
}
get_header();
	include (get_template_directory() . '/includes/author-head.php');
	?>
	<div class="page-content page-content-user">
		<div class="user-questions">
			<?php $rows_per_page = get_option("posts_per_page");
			$_points = get_user_meta($user_login->ID,$user_login->user_login."_points",true);
			
			for ($points = 1; $points <= $_points; $points++) {
				$point_one[] = get_user_meta($user_login->ID,$user_login->user_login."_points_".$points);
			}
			if (isset($point_one) and is_array($point_one)) {
				$point = array_reverse($point_one);
				
				$current = max(1,get_query_var('paged'));
				$pagination_args = array(
					'base' => @add_query_arg('paged','%#%'),
					'format' => 'page/%#%/?u='.$_GET['u'],
					'total' => ceil(sizeof($point)/$rows_per_page),
					'current' => $current,
					'show_all' => true,
					'prev_text' => '<i class="icon-angle-left"></i>',
					'next_text' => '<i class="icon-angle-right"></i>',
				);
				
				if( !empty($wp_query->query_vars['s']) )
					$pagination_args['add_args'] = array('s'=>get_query_var('s'));
					
				$start = ($current - 1) * $rows_per_page;
				$end = $start + $rows_per_page;
				$end = (sizeof($point) < $end) ? sizeof($point) : $end;
				for ($i=$start;$i < $end ;++$i ) {
					$point_result = $point[$i];?>
					<article class="question user-question user-points">
						<div class="question-content">
							<div class="question-bottom">
								<h3>
								<?php if (!empty($point_result[0][5]) && !empty($point_result[0][6])) {?>
									<a href="<?php echo get_the_permalink($point_result[0][5]).(isset($point_result[0][6])?"#comment-".$point_result[0][6]:"")?>">
								<?php }else if (!empty($point_result[0][5]) && empty($point_result[0][6])) {?>
									<a href="<?php echo get_the_permalink($point_result[0][5])?>">
								<?php }
									echo $point_result[0][4];
								if ((!empty($point_result[0][5]) && !empty($point_result[0][6])) || !empty($point_result[0][5])) {?>
									</a>
								<?php }?>
								</h3>
								<div class="question-user-vote"><i class="icon-thumbs-up"></i></div>
								<div class="question-vote-result"><?php echo $point_result[0][3]?><?php echo $point_result[0][2]?></div>
								<span class="question-date"><i class="icon-time"></i><?php echo $point_result[0][0]."&nbsp;&nbsp;-&nbsp;&nbsp;".$point_result[0][1]?></span>
							</div>
						</div>
					</article>
				<?php }
			}else {echo "<p class='no-item'>".__("There are no points yet .","vbegy")."</p>";}?>
		</div>
	</div>
	<?php if (isset($point_one) &&is_array($point_one) && $pagination_args["total"] > 1) {?>
		<div class="height_30"></div>
		<div class='pagination'><?php echo (paginate_links($pagination_args) != ""?paginate_links($pagination_args):"")?></div>
	<?php }
get_footer();?>