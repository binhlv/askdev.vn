<?php
if (is_author()) {
	$get_query_var = get_query_var("author");
}else {
	$get_query_var = esc_attr($_GET['u']);
}
$user_login = get_userdata($get_query_var);
$you_avatar = get_the_author_meta('you_avatar',$user_login->ID);
$url = get_the_author_meta('url',$user_login->ID);
$country = get_the_author_meta('country',$user_login->ID);
$twitter = get_the_author_meta('twitter',$user_login->ID);
$facebook = get_the_author_meta('facebook',$user_login->ID);
$google = get_the_author_meta('google',$user_login->ID);
$linkedin = get_the_author_meta('linkedin',$user_login->ID);
$follow_email = get_the_author_meta('follow_email',$user_login->ID);

$owner = false;
if($user_ID == $user_login->ID){
	$owner = true;
}

/* visit */
$visit_profile = get_user_meta($user_login->ID,"visit_profile_all",true);
$visit_profile_m = get_user_meta($user_login->ID,"visit_profile_m_".date_i18n('m_Y',current_time('timestamp')),true);
$visit_profile_d = get_user_meta($user_login->ID,"visit_profile_d_".date_i18n('d_m_Y',current_time('timestamp')),true);

if ($visit_profile_d == "" or $visit_profile_d == 0) {
	add_user_meta($user_login->ID,"visit_profile_d_".date_i18n('d_m_Y',current_time('timestamp')),1);
}else {
	update_user_meta($user_login->ID,"visit_profile_d_".date_i18n('d_m_Y',current_time('timestamp')),$visit_profile_d+1);
}

if ($visit_profile_m == "" or $visit_profile_m == 0) {
	add_user_meta($user_login->ID,"visit_profile_m_".date_i18n('m_Y',current_time('timestamp')),1);
}else {
	update_user_meta($user_login->ID,"visit_profile_m_".date_i18n('m_Y',current_time('timestamp')),$visit_profile_m+1);
}

if ($visit_profile == "" or $visit_profile == 0) {
	add_user_meta($user_login->ID,"visit_profile_all",1);
}else {
	update_user_meta($user_login->ID,"visit_profile_all",$visit_profile+1);
}

/* points */
$points = get_user_meta($user_login->ID,"points",true);

/* favorites */
$_favorites = get_user_meta($user_login->ID,$user_login->user_login."_favorites");

/* the_best_answer */
$the_best_answer = get_user_meta($user_login->ID,"the_best_answer",true);

/* add_answer */
$add_answer = get_user_meta($user_login->ID,"add_answer_all",true);
$add_answer_m = get_user_meta($user_login->ID,"add_answer_m_".date_i18n('m_Y',current_time('timestamp')),true);
$add_answer_d = get_user_meta($user_login->ID,"add_answer_d_".date_i18n('d_m_Y',current_time('timestamp')),true);
$add_answer = count(get_comments(array("post_type" => "question","status" => "approve","user_id" => $user_login->ID)));

/* add_questions */
$add_questions = get_user_meta($user_login->ID,"add_questions_all",true);
$add_questions_m = get_user_meta($user_login->ID,"add_questions_m_".date_i18n('m_Y',current_time('timestamp')),true);
$add_questions_d = get_user_meta($user_login->ID,"add_questions_d_".date_i18n('d_m_Y',current_time('timestamp')),true);
$add_questions = count_user_posts_by_type($user_login->ID,"question");

/* following */
$following_me = get_user_meta($user_login->ID,"following_me");
$following_you = get_user_meta($user_login->ID,"following_you");
?>
<div class="row">
	<div class="user-profile">
		<div class="col-md-12">
			<div class="page-content">
				<h2><?php _e("About","vbegy")?> <a href="<?php echo get_author_posts_url($user_login->ID);?>"><?php echo $user_login->display_name?></a></h2>
				<div class="user-profile-img">
					<a original-title="<?php echo $user_login->display_name?>" class="tooltip-n" href="<?php echo get_author_posts_url($user_login->ID);?>">
						<?php if ($you_avatar) {
							$you_avatar_img = get_aq_resize_url(esc_attr($you_avatar),"full",79,79);
							echo "<img alt='".$user_login->display_name."' src='".$you_avatar_img."'>";
						}else {
							echo get_avatar(get_the_author_meta('user_email'),'65','');
						}?>
					</a>
				</div>
				<div class="ul_list ul_list-icon-ok about-user">
					<ul>
						<li><i class="icon-plus"></i><?php _e("Registered","vbegy")?> : <span><?php echo substr($user_login->user_registered, 0, 10); ?></span></li>
						<?php if ($country) {?>
						<li><i class="icon-map-marker"></i><?php _e("Country","vbegy")?> : <span><?php echo $country?></span></li>
						<?php }
						if ($url) {?>
						<li><i class="icon-globe"></i><?php _e("Website","vbegy")?> : <a target="_blank" href="<?php echo $url?>"><?php _e("view","vbegy")?></a></li>
						<?php }?>
					</ul>
				</div>
				<div class="clearfix"></div>
				<p><?php echo $user_login->description?></p>
				<div class="clearfix"></div>
				<?php if($owner == true){?>
					<a class="button color small margin_0" href="<?php echo get_page_link(vpanel_options('user_edit_profile_page'))?>"><?php _e("Edit profile","vbegy")?></a>
				<?php }
				if (is_user_logged_in() and $owner == false) {
					$following_me2 = get_user_meta(get_current_user_id(),"following_me");
					if (!empty($following_me2) and in_array($user_login->ID,$following_me2[0])) {?>
						<a href="#" class="following_not button color small margin_0" rel="<?php echo $user_login->ID?>"><?php _e("Unfollow","vbegy")?></a>
					<?php }else {?>
						<a href="#" class="following_you button color small margin_0" rel="<?php echo $user_login->ID?>"><?php _e("Follow","vbegy")?></a>
				<?php
					}
				}?>
				<div class="clearfix"></div>
				<?php if ($facebook || $twitter || $linkedin || $google || $follow_email) { ?>
					<br>
					<span class="user-follow-me"><?php _e("Follow Me","vbegy")?></span>
					<?php if ($facebook) {?>
					<a href="<?php echo $facebook?>" original-title="<?php _e("Facebook","vbegy")?>" class="tooltip-n">
						<span class="icon_i">
							<span class="icon_square" icon_size="30" span_bg="#3b5997" span_hover="#2f3239">
								<i class="social_icon-facebook"></i>
							</span>
						</span>
					</a>
					<?php }
					if ($twitter) {?>
					<a href="<?php echo $twitter?>" original-title="<?php _e("Twitter","vbegy")?>" class="tooltip-n">
						<span class="icon_i">
							<span class="icon_square" icon_size="30" span_bg="#00baf0" span_hover="#2f3239">
								<i class="social_icon-twitter"></i>
							</span>
						</span>
					</a>
					<?php }
					if ($linkedin) {?>
					<a href="<?php echo $linkedin?>" original-title="<?php _e("Linkedin","vbegy")?>" class="tooltip-n">
						<span class="icon_i">
							<span class="icon_square" icon_size="30" span_bg="#006599" span_hover="#2f3239">
								<i class="social_icon-linkedin"></i>
							</span>
						</span>
					</a>
					<?php }
					if ($google) {?>
					<a href="<?php echo $google?>" original-title="<?php _e("Google plus","vbegy")?>" class="tooltip-n">
						<span class="icon_i">
							<span class="icon_square" icon_size="30" span_bg="#c43c2c" span_hover="#2f3239">
								<i class="social_icon-gplus"></i>
							</span>
						</span>
					</a>
					<?php }
					if ($follow_email) {?>
					<a href="<?php echo $follow_email?>" original-title="<?php _e("Email","vbegy")?>" class="tooltip-n">
						<span class="icon_i">
							<span class="icon_square" icon_size="30" span_bg="#000" span_hover="#2f3239">
								<i class="social_icon-email"></i>
							</span>
						</span>
					</a>
					<?php }
				}?>
			</div><!-- End page-content -->
		</div><!-- End col-md-12 -->
		<div class="col-md-12">
			<div class="page-content page-content-user-profile">
				<div class="user-profile-widget">
					<h2><?php _e("User Stats","vbegy")?></h2>
					<div class="ul_list ul_list-icon-ok">
						<ul>
							<li><i class="icon-question-sign"></i><a href="<?php echo add_query_arg("u", esc_attr($get_query_var),get_page_link(vpanel_options('question_user_page')))?>"><?php _e("Questions","vbegy")?><span> ( <span><?php echo ($add_questions == ""?0:$add_questions)?></span> ) </span></a></li>
							<li><i class="icon-comment"></i><a href="<?php echo add_query_arg("u", esc_attr($get_query_var),get_page_link(vpanel_options('answer_user_page')))?>"><?php _e("Answers","vbegy")?><span> ( <span><?php echo ($add_answer == ""?0:$add_answer)?></span> ) </span></a></li>
							<?php if (vpanel_options("show_point_favorite") == 1 || $owner == true) {?>
								<li><i class="icon-star"></i><a href="<?php echo add_query_arg("u", esc_attr($get_query_var),get_page_link(vpanel_options('favorite_user_page')))?>"><?php _e("Favorite Questions","vbegy")?><span> ( <span><?php echo (isset($_favorites[0])?count($_favorites[0]):0)?></span> ) </span></a></li>
								<li><i class="icon-heart"></i><a href="<?php echo add_query_arg("u", esc_attr($get_query_var),get_page_link(vpanel_options('point_user_page')))?>"><?php _e("Points","vbegy")?><span> ( <span><?php echo ($points == "" ?0:$points)?></span> ) </span></a></li>
							<?php }?>
							<li><i class="icon-file-alt"></i><a href="<?php echo add_query_arg("u", esc_attr($get_query_var),get_page_link(vpanel_options('post_user_page')))?>"><?php _e("Posts","vbegy")?><span> ( <span><?php echo count_user_posts_by_type($user_login->ID,"post")?></span> ) </span></a></li>
							<li><i class="icon-asterisk"></i><?php _e("Best Answers","vbegy")?><span> ( <span><?php echo ($the_best_answer == ""?0:$the_best_answer)?></span> ) </span></li>
							<?php if (vpanel_options("show_point_favorite") == 1 || $owner == true) {?>
								<li class="authors_follow"><i class="icon-user-md"></i><a href="<?php echo add_query_arg("u", esc_attr($get_query_var),get_page_link(vpanel_options('i_follow_user_page')))?>"><?php _e("Authors I Follow","vbegy")?><span> ( <span><?php echo (isset($following_me[0]) && is_array($following_me[0])?count($following_me[0]):0)?></span> ) </span></a></li>
								<li class="followers"><i class="icon-user"></i><a href="<?php echo add_query_arg("u", esc_attr($get_query_var),get_page_link(vpanel_options('followers_user_page')))?>"><?php _e("Followers","vbegy")?><span> ( <span><?php echo (isset($following_you[0]) && is_array($following_you[0])?count($following_you[0]):0)?></span> ) </span></a></li>
							<?php }?>
						</ul>
					</div>
				</div><!-- End user-profile-widget -->
			</div><!-- End page-content -->
		</div><!-- End col-md-12 -->
	</div><!-- End user-profile -->
</div><!-- End row -->
<div class="clearfix"></div>