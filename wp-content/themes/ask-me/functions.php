<?php
if (is_admin() and isset($_GET['activated']) and $pagenow == "themes.php")
wp_redirect('admin.php?page=options');
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/admin/' );

/* Load lang languages */
load_theme_textdomain('vbegy',dirname(__FILE__).'/languages');

/* require files */
require_once get_template_directory() . '/admin/options-framework.php';
require_once get_template_directory() . '/admin/meta-box/meta-box.php';
require_once get_template_directory() . '/admin/meta-box/meta_box.php';
require_once get_template_directory() . '/admin/functions/aq_resizer.php';
require_once get_template_directory() . '/admin/functions/main_functions.php';
require_once get_template_directory() . '/admin/functions/widget_functions.php';
require_once get_template_directory() . '/admin/functions/nav_menu.php';
require_once get_template_directory() . '/admin/functions/register_post.php';
require_once get_template_directory() . '/admin/functions/page_builder.php';
require_once get_template_directory() . '/functions/shortcode_ask.php';
require_once get_template_directory() . '/functions/functions_ask.php';
if (!class_exists('TwitterOAuth',false)) {
	require_once (get_template_directory() . '/includes/twitteroauth/twitteroauth.php');
}

$themename = wp_get_theme();
$themename = preg_replace("/\W/", "_", strtolower($themename) );
define("vpanel_name",$themename);

/* Widgets */
include get_template_directory() . '/admin/widgets/stats.php';
include get_template_directory() . '/admin/widgets/counter.php';
include get_template_directory() . '/admin/widgets/contact.php';
include get_template_directory() . '/admin/widgets/login.php';
include get_template_directory() . '/admin/widgets/highest_points.php';
include get_template_directory() . '/admin/widgets/questions.php';
include get_template_directory() . '/admin/widgets/twitter.php';
include get_template_directory() . '/admin/widgets/flickr.php';
include get_template_directory() . '/admin/widgets/video.php';
include get_template_directory() . '/admin/widgets/subscribe.php';
include get_template_directory() . '/admin/widgets/search.php';
include get_template_directory() . '/admin/widgets/comments.php';
include get_template_directory() . '/admin/widgets/tabs.php';
include get_template_directory() . '/admin/widgets/adv-120x600.php';
include get_template_directory() . '/admin/widgets/adv-234x60.php';
include get_template_directory() . '/admin/widgets/adv-250x250.php';
include get_template_directory() . '/admin/widgets/adv-120x240.php';
include get_template_directory() . '/admin/widgets/adv-125x125.php';

/* vbegy_scripts_styles */
function vbegy_scripts_styles() {
	global $post;
	wp_enqueue_style('v_css', get_stylesheet_uri() .  '', '', null, 'all');
	if (vpanel_options("v_responsive") == 1) {
		wp_enqueue_style('v_responsive', get_template_directory_uri( __FILE__ )."/css/responsive.css");
	}
	$site_skin_all = vpanel_options("site_skin_l");
	if (is_author()) {
		$author_skin_l = vpanel_options("author_skin_l");
		if ($author_skin_l == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}else if (is_category()) {
		$category_id = get_query_var('cat');
		$cat_skin_l = vpanel_options("cat_skin_l".$category_id);
		if ($cat_skin_l == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}else if (is_tax("question-category")) {
		$tax_id = get_term_by('slug',get_query_var('term'),"question-category");
		$tax_id = $tax_id->term_id;
		$cat_skin_l = vpanel_options("cat_skin_l".$tax_id);
		if ($cat_skin_l == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}else if (is_single() || is_page()) {
		$vbegy_site_skin_l = rwmb_meta('vbegy_site_skin_l','radio',$post->ID);
		if ($vbegy_site_skin_l == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}else {
		if ($site_skin_all == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}
	
	if ((is_author() && $author_skin_l == "default") || ((is_single() || is_page()) && $vbegy_site_skin_l == "default") || (is_category() && $cat_skin_l == "default") || (is_tax("question-category") && $cat_skin_l == "default")) {
		if ($site_skin_all == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}
	
	$site_skin = vpanel_options('site_skin');
	if ($site_skin != "default") {
		wp_enqueue_style('skin_'.$site_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$site_skin.".css");
	}else {
		wp_enqueue_style('skin_default', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
	}
	wp_enqueue_script("v_easing", get_template_directory_uri( __FILE__ )."/js/jquery.easing.1.3.min.js",array("jquery"));
	wp_enqueue_script("v_html5", get_template_directory_uri( __FILE__ )."/js/html5.js",array("jquery"));
	wp_enqueue_script("v_modernizr", get_template_directory_uri( __FILE__ )."/js/modernizr.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_twitter", get_template_directory_uri( __FILE__ )."/js/twitter/jquery.tweet.js",array("jquery"));
	wp_enqueue_script("v_jflickrfeed", get_template_directory_uri( __FILE__ )."/js/jflickrfeed.min.js",array("jquery"));
	wp_enqueue_script("v_inview", get_template_directory_uri( __FILE__ )."/js/jquery.inview.min.js",array("jquery"));
	wp_enqueue_script("v_tipsy", get_template_directory_uri( __FILE__ )."/js/jquery.tipsy.js",array("jquery"));
	wp_enqueue_script("v_tabs", get_template_directory_uri( __FILE__ )."/js/tabs.js",array("jquery"));
	wp_enqueue_script("v_flexslider", get_template_directory_uri( __FILE__ )."/js/jquery.flexslider.js",array("jquery"));
	wp_enqueue_script("v_prettyphoto", get_template_directory_uri( __FILE__ )."/js/jquery.prettyPhoto.js",array("jquery"));
	wp_enqueue_script("v_carouFredSel", get_template_directory_uri( __FILE__ )."/js/jquery.carouFredSel-6.2.1-packed.js",array("jquery"));
	wp_enqueue_script("v_scrollTo", get_template_directory_uri( __FILE__ )."/js/jquery.scrollTo.js",array("jquery"));
	wp_enqueue_script("v_nav", get_template_directory_uri( __FILE__ )."/js/jquery.nav.js",array("jquery"));
	wp_enqueue_script("v_tags", get_template_directory_uri( __FILE__ )."/js/tags.js",array("jquery"));
	if (is_rtl()) {
		wp_enqueue_script("v_bxslider", get_template_directory_uri( __FILE__ )."/js/jquery.bxslider.min-ar.js",array("jquery"));
	}else {
		wp_enqueue_script("v_bxslider", get_template_directory_uri( __FILE__ )."/js/jquery.bxslider.min.js",array("jquery"));
	}
	wp_enqueue_script("v_custom", get_template_directory_uri( __FILE__ )."/js/custom.js",array("jquery","jquery-ui-core","jquery-ui-sortable"));
	wp_localize_script("v_custom","template_url",get_template_directory_uri( __FILE__ ));
	wp_localize_script("v_custom","go_to",__("Go to...","vbegy"));
	wp_localize_script("v_custom","ask_error_text",__("Please fill the required field.","vbegy"));
	wp_localize_script("v_custom","ask_error_captcha",__("The captcha is incorrect, please try again.","vbegy"));
	wp_localize_script("v_custom","ask_error_empty",__("Fill out all the required fields.","vbegy"));
	wp_localize_script("v_custom","no_vote_more",__("Sorry, you cannot vote on the same question more than once.","vbegy"));
	wp_localize_script("v_custom","no_vote_user",__("Rating is available to members only.","vbegy"));
	wp_localize_script("v_custom","no_vote_more_comment",__("Sorry, you cannot vote on the same comment more than once.","vbegy"));
	wp_localize_script("v_custom","add_question",get_page_link(vpanel_options('add_question')));
	wp_localize_script("v_custom","v_get_template_directory_uri",get_template_directory_uri());
	wp_localize_script("v_custom","sure_report",__("Are you sure you want to Report?","vbegy"));
	wp_localize_script("v_custom","reported_question",__("Were reported for the question!","vbegy"));
	wp_localize_script("v_custom","choose_best_answer",__("Select as best answer","vbegy"));
	wp_localize_script("v_custom","cancel_best_answer",__("Cancel the best answer","vbegy"));
	wp_localize_script("v_custom","best_answer",__("Best answer","vbegy"));
	wp_localize_script("v_custom","admin_url",admin_url("admin-ajax.php"));
	if (is_rtl()) {
		wp_enqueue_script("v_custom_ar", get_template_directory_uri( __FILE__ )."/js/custom-ar.js",array("jquery"));
	}
	if (is_singular() && comments_open() && get_option('thread_comments'))
		wp_enqueue_script( 'comment-reply');
}
add_action('wp_enqueue_scripts','vbegy_scripts_styles');
/* vbegy_load_theme */
function vbegy_load_theme() {
    /* Default RSS feed links */
    add_theme_support('automatic-feed-links');

    /* Post Thumbnails */
    if ( function_exists( 'add_theme_support' ) ){
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 50, 50, true );
        set_post_thumbnail_size( 60, 60, true );
        set_post_thumbnail_size( 80, 80, true );
    }
    if ( function_exists( 'add_image_size' ) ){
        add_image_size( 'vbegy_img_1', 50, 50, true );
        add_image_size( 'vbegy_img_2', 60, 60, true );
        add_image_size( 'vbegy_img_3', 80, 80, true );
    }
    /* Valid HTML5 */
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
    /* This theme uses its own gallery styles */
    add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'vbegy_load_theme' );

/* wp head */
function vbegy_head(){
    $default_favicon = get_template_directory_uri()."/images/favicon.png";
    echo '<link rel="shortcut icon" href="'.vpanel_options("favicon").'" type="image/x-icon">' ."\n";

    /* Favicon iPhone */
    if (vpanel_options("iphone_icon")) {
        echo '<link rel="apple-touch-icon-precomposed" href="'.vpanel_options("iphone_icon").'">' ."\n";
    }

    /* Favicon iPhone 4 Retina display */
    if (vpanel_options("iphone_icon_retina")) {
        echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'.vpanel_options("iphone_icon_retina").'">' ."\n";
    }

    /* Favicon iPad */
    if (vpanel_options("ipad_icon")) {
        echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'.vpanel_options("ipad_icon").'">' ."\n";
    }

    /* Favicon iPad Retina display */
    if (vpanel_options("ipad_icon_retina")) {
        echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'.vpanel_options("ipad_icon_retina").'">' ."\n";
    }

    /* Seo */
    $the_seo = stripslashes(vpanel_options("the_keywords"));

    if(vpanel_options("seo_active") == 1){
        if( is_home() || is_front_page() ){
            echo '<meta name="description" content="'. get_bloginfo('description') .'">' ."\n";
            echo "<meta name='keywords' content='".$the_seo."'>" ."\n";
        }else if (is_single() || is_page()) {
	        if ( have_posts() ) : while ( have_posts() ) : the_post();?>
	        	<meta name="description" content="<?php the_excerpt_rss(); ?>">
	            <?php $posttags = get_the_tags();
	                if ($posttags) {
	                    $the_tags_post = '';
	                    foreach ($posttags as $tag) {
	                        $the_tags_post .= $tag->name . ',';
	                    }
	                    echo '<meta name="keywords" content="' . trim($the_tags_post, ',') . '">' ."\n";
	                }
	        endwhile;endif;wp_reset_query();
        }
    }
    
	?>
	<style type="text/css"> 
	<?php echo "\n";
	$vbegy_layout = "";
	if (is_single() || is_page()) {
		global $post;
		$vbegy_layout = rwmb_meta('vbegy_layout','radio',$post->ID);
		$primary_color_p = rwmb_meta('vbegy_primary_color','color',$post->ID);
		$secondary_color_p = rwmb_meta('vbegy_secondary_color','color',$post->ID);
		$vbegy_skin = rwmb_meta('vbegy_skin','radio',$post->ID);
	}
	$cat_layout = "";
	if (is_category()) {
		$category_id = get_query_var('cat');
		$cat_layout = vpanel_options('cat_layout'.$category_id);
		$cat_background_type = vpanel_options('cat_background_type'.$category_id);
		$cat_background_color = vpanel_options('cat_background_color'.$category_id);
		$cat_background_pattern = vpanel_options('cat_background_pattern'.$category_id);
		$cat_custom_background = vpanel_options('cat_custom_background'.$category_id);
		$cat_full_screen_background = vpanel_options('cat_full_screen_background'.$category_id);
		$cat_skin = vpanel_options('cat_skin'.$category_id);
		$primary_color_c = vpanel_options('primary_color'.$category_id);
		$secondary_color_c = vpanel_options('secondary_color'.$category_id);
	}
	if (is_tax("question-category")) {
		$tax_id = get_term_by('slug',get_query_var('term'),"question-category");
		$tax_id = $tax_id->term_id;
		$cat_layout = vpanel_options('cat_layout'.$tax_id);
		$cat_background_type = vpanel_options('cat_background_type'.$tax_id);
		$cat_background_color = vpanel_options('cat_background_color'.$tax_id);
		$cat_background_pattern = vpanel_options('cat_background_pattern'.$tax_id);
		$cat_custom_background = vpanel_options('cat_custom_background'.$tax_id);
		$cat_full_screen_background = vpanel_options('cat_full_screen_background'.$tax_id);
		$cat_skin = vpanel_options('cat_skin'.$tax_id);
		$primary_color_c = vpanel_options('primary_color'.$tax_id);
		$secondary_color_c = vpanel_options('secondary_color'.$tax_id);
	}
	$author_layout = "";
	if (is_author()) {
		$author_layout = vpanel_options('author_layout');
		$author_background_type = vpanel_options('author_background_type');
		$author_background_color = vpanel_options('author_background_color');
		$author_background_pattern = vpanel_options('author_background_pattern');
		$author_custom_background = vpanel_options('author_custom_background');
		$author_full_screen_background = vpanel_options('author_full_screen_background');
		$vbegy_skin = vpanel_options('author_skin');
		$primary_color_a = vpanel_options('author_primary_color');
		$secondary_color_a = vpanel_options('author_secondary_color');
	}
	
	if (is_category() && $cat_layout != "default") {
		if ($cat_layout != "full") {
			$custom_background = $cat_custom_background;
			if ($cat_full_screen_background == 1 && $cat_background_type != "patterns") {?>
				.background-cover {
					<?php if (!empty($cat_background_color)) {?>
					background-color:<?php echo $cat_background_color ?>;
					<?php }?>
					background-image : url('<?php echo $custom_background["image"]?>') ;<?php echo "\n"; ?>
					filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale');<?php echo "\n"; ?>
					-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale')";<?php echo "\n";?>
				}
			<?php }else {?>
				body {
					background:
					<?php if ($cat_background_type == "patterns") {
						if ($cat_background_pattern != "default") {?>
						<?php echo $cat_background_color?> url(<?php echo get_template_directory_uri()?>/images/patterns/<?php echo $cat_background_pattern?>.png) repeat;
						<?php }?>
					<?php }else {
					}
					if (!empty($custom_background)) {
					if ($cat_full_screen_background != 1) {?>
					<?php echo $custom_background["color"]?> url(<?php echo $custom_background["image"]?>) <?php echo $custom_background["repeat"]?> <?php echo $custom_background["position"]?> <?php echo $custom_background["attachment"]?>;
					<?php }}?>
				}
			<?php }
		}
	}else if (is_tax("question-category") && $cat_layout != "default") {
		if ($cat_layout != "full") {
			$custom_background = $cat_custom_background;
			if ($cat_full_screen_background == 1 && $cat_background_type != "patterns") {?>
				.background-cover {
					<?php if (!empty($cat_background_color)) {?>
					background-color:<?php echo $cat_background_color ?>;
					<?php }?>
					background-image : url('<?php echo $custom_background["image"]?>') ;<?php echo "\n"; ?>
					filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale');<?php echo "\n"; ?>
					-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale')";<?php echo "\n";?>
				}
			<?php }else {?>
				body {
					background:
					<?php if ($cat_background_type == "patterns") {
						if ($cat_background_pattern != "default") {?>
						<?php echo $cat_background_color?> url(<?php echo get_template_directory_uri()?>/images/patterns/<?php echo $cat_background_pattern?>.png) repeat;
						<?php }?>
					<?php }else {
					}
					if (!empty($custom_background)) {
					if ($cat_full_screen_background != 1) {?>
					<?php echo $custom_background["color"]?> url(<?php echo $custom_background["image"]?>) <?php echo $custom_background["repeat"]?> <?php echo $custom_background["position"]?> <?php echo $custom_background["attachment"]?>;
					<?php }}?>
				}
			<?php }
		}
	}else if (is_author() && $author_layout != "default") {
		if ($author_layout != "full") {
			$custom_background = $author_custom_background;
			if ($author_full_screen_background == 1 && $author_background_type != "patterns") {?>
				.background-cover {
					<?php if (!empty($author_background_color)) {?>
					background-color:<?php echo $author_background_color ?>;
					<?php }?>
					background-image : url('<?php echo $custom_background["image"]?>') ;<?php echo "\n"; ?>
					filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale');<?php echo "\n"; ?>
					-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale')";<?php echo "\n";?>
				}
			<?php }else {?>
				body {
					background:
					<?php if ($author_background_type == "patterns") {
						if ($author_background_pattern != "default") {?>
						<?php echo $author_background_color?> url(<?php echo get_template_directory_uri()?>/images/patterns/<?php echo $author_background_pattern?>.png) repeat;
						<?php }?>
					<?php }else {
					}
					if (!empty($custom_background)) {
					if ($author_full_screen_background != 1) {?>
					<?php echo $custom_background["color"]?> url(<?php echo $custom_background["image"]?>) <?php echo $custom_background["repeat"]?> <?php echo $custom_background["position"]?> <?php echo $custom_background["attachment"]?>;
					<?php }}?>
				}
			<?php }
		}
	}else if ((is_single() || is_page()) && $vbegy_layout != "default"):
		if ($vbegy_layout == "fixed" || $vbegy_layout == "fixed_2"):
			$background_img = rwmb_meta('vbegy_background_img','upload',$post->ID);
			$background_color = rwmb_meta('vbegy_background_color','color',$post->ID);
			$background_repeat = rwmb_meta('vbegy_background_repeat','select',$post->ID);
			$background_fixed = rwmb_meta('vbegy_background_fixed','select',$post->ID);
			$background_position_x = rwmb_meta('vbegy_background_position_x','select',$post->ID);
			$background_position_y = rwmb_meta('vbegy_background_position_y','select',$post->ID);
			if ($background_color || $background_img):
				if (rwmb_meta('vbegy_background_full','checkbox',$post->ID) == 1): ?>
					.background-cover{<?php echo "\n"; ?>
						<?php if (!empty($background_color)) {?>
						background-color:<?php echo $background_color ?>;
						<?php }?>
						background-image : url('<?php echo $background_img ?>') ;<?php echo "\n"; ?>
						filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $background_img ?>',sizingMethod='scale');<?php echo "\n"; ?>
						-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $background_img ?>',sizingMethod='scale')";<?php echo "\n"; ?>
					}
				<?php else: ?>
					body{background:<?php echo $background_color ?> url('<?php echo $background_img ?>') <?php echo $background_repeat ?> <?php echo $background_fixed ?> <?php echo $background_position_x ?> <?php echo $background_position_y ?>;}<?php echo "\n"; ?>
				<?php endif;
			endif;
		endif;
	else:
		if (vpanel_options("home_layout") != "full") {
			$custom_background = vpanel_options("custom_background");
			if (vpanel_options("full_screen_background") == 1 && vpanel_options("background_type") != "patterns") {?>
				.background-cover {
					<?php $background_color_s = vpanel_options("background_color");
					if (!empty($background_color_s)) {?>
					background-color:<?php echo $background_color_s ?>;
					<?php }?>
					background-image : url('<?php echo $custom_background["image"]?>') ;<?php echo "\n"; ?>
					filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale');<?php echo "\n"; ?>
					-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $custom_background["image"]?>',sizingMethod='scale')";<?php echo "\n";?>
				}
			<?php }else {?>
				body {
					background:
					<?php if (vpanel_options("background_type") == "patterns") {
						if (vpanel_options("background_pattern") != "default") {?>
						<?php echo vpanel_options("background_color")?> url(<?php echo get_template_directory_uri()?>/images/patterns/<?php echo vpanel_options("background_pattern")?>.png) repeat;
						<?php }?>
					<?php }else {
					}
					if (!empty($custom_background)) {
					if (vpanel_options("full_screen_background") != 1) {?>
					<?php echo $custom_background["color"]?> url(<?php echo $custom_background["image"]?>) <?php echo $custom_background["repeat"]?> <?php echo $custom_background["position"]?> <?php echo $custom_background["attachment"]?>;
					<?php }}?>
				}
			<?php }
		}
	endif;
	if ((is_single() || is_page()) && ($primary_color_p == "" && $secondary_color_p == "")) {
		if ($vbegy_skin != "default_color") {
			if ($vbegy_skin == "default") {
				wp_enqueue_style('skin_skins', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
			}else if (!empty($vbegy_skin)) {
				wp_enqueue_style('skin_'.$vbegy_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$vbegy_skin.".css");
			}
		}else {
			$primary_color = vpanel_options("primary_color");
			$secondary_color = vpanel_options("secondary_color");
			if ($primary_color != "" && $secondary_color != "") :
				all_css_color($primary_color,$secondary_color);
			endif;
		}
	}else if ((is_single() || is_page()) && ($primary_color_p != "" && $secondary_color_p != "")) {
		all_css_color($primary_color_p,$secondary_color_p);
	}else if (is_category() && ($primary_color_c == "" && $secondary_color_c == "")) {
		if ($cat_skin != "default_color") {
			if ($cat_skin == "default") {
				wp_enqueue_style('skin_skins', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
			}else if (!empty($cat_skin)) {
				wp_enqueue_style('skin_'.$cat_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$cat_skin.".css");
			}
		}else {
			$primary_color = vpanel_options("primary_color");
			$secondary_color = vpanel_options("secondary_color");
			if ($primary_color != "" && $secondary_color != "") :
				all_css_color($primary_color,$secondary_color);
			endif;
		}
	}else if (is_category() && ($primary_color_c != "" && $secondary_color_c != "")) {
		all_css_color($primary_color_c,$secondary_color_c);
	}else if (is_tax("question-category") && ($primary_color_c == "" && $secondary_color_c == "")) {
		if ($cat_skin != "default_color") {
			if ($cat_skin == "default") {
				wp_enqueue_style('skin_skins', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
			}else if (!empty($cat_skin)) {
				wp_enqueue_style('skin_'.$cat_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$cat_skin.".css");
			}
		}else {
			$primary_color = vpanel_options("primary_color");
			$secondary_color = vpanel_options("secondary_color");
			if ($primary_color != "" && $secondary_color != "") :
				all_css_color($primary_color,$secondary_color);
			endif;
		}
	}else if (is_tax("question-category") && ($primary_color_c != "" && $secondary_color_c != "")) {
		all_css_color($primary_color_c,$secondary_color_c);
	}else if (is_author() && ($primary_color_a == "" && $secondary_color_a == "")) {
		if ($vbegy_skin != "default_color") {
			if ($vbegy_skin == "default") {
				wp_enqueue_style('skin_skins', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
			}else if (!empty($vbegy_skin)) {
				wp_enqueue_style('skin_'.$vbegy_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$vbegy_skin.".css");
			}
		}else {
			$primary_color = vpanel_options("primary_color");
			$secondary_color = vpanel_options("secondary_color");
			if ($primary_color != "" && $secondary_color != "") :
				all_css_color($primary_color,$secondary_color);
			endif;
		}
	}else if (is_author() && ($primary_color_a != "" && $secondary_color_a != "")) {
		all_css_color($primary_color_a,$secondary_color_a);
	}else {
		$primary_color = vpanel_options("primary_color");
		$secondary_color = vpanel_options("secondary_color");
		if ($primary_color != "" && $secondary_color != "") :
			all_css_color($primary_color,$secondary_color);
		endif;
	}
	
    /* custom_css */
	if(vpanel_options("custom_css")) {
	    echo stripslashes(vpanel_options("custom_css"));
	}
	?>
	</style> 
	<?php
    
    /* head_code */
    if(vpanel_options("head_code")) {
        echo stripslashes(vpanel_options("head_code"));
    }

}
add_action('wp_head', 'vbegy_head');

/* wp login head */
function vbegy_login_logo() {
    if (vpanel_options("login_logo") != "") {
        echo '<style type="text/css"> h1 a {  background-image:url('.vpanel_options("login_logo").')  !important; background-size: auto !important; } </style>';
	}
}
add_action('login_head',  'vbegy_login_logo');

/* all_css_color */
function all_css_color($color_1,$color_2) {
	?>
	::-moz-selection {
	    background: <?php echo $color_1;?>;
	}
	::selection {
	    background: <?php echo $color_1;?>;
	}
	.more:hover,.button.color,.button.black:hover,.go-up,.widget_portfolio .portfolio-widget-item:hover .portfolio_img:before,.popular_posts .popular_img:hover a:before,.widget_flickr a:hover:before,.widget_highest_points .author-img a:hover:before,.question-author-img:hover span,.pagination a:hover,.pagination span:hover,.pagination span.current,.about-author .author-image a:hover:before,.question-comments a,.flex-direction-nav li a:hover,.button.dark_button.color:hover,.table-style-2 thead th,.progressbar-percent,.carousel-arrow a:hover,.box_icon:hover .icon_circle,.box_icon:hover .icon_soft_r,.box_icon:hover .icon_square,.bg_default,.box_warp_colored,.box_warp_hover:hover,.post .boxedtitle i,.single-question-title i,.question-type,.post-type,.social_icon a,.page-content .boxedtitle,.main-content .boxedtitle,.flex-caption h2,.flex-control-nav li a.flex-active,.bxslider-overlay:before,.navigation .header-menu ul li ul li:hover > a,.navigation .header-menu ul li ul li.current_page_item > a,#header-top,.navigation > .header-menu > ul > li:hover > a,.navigation > .header-menu > ul > li.current_page_item > a,.ask-me,.breadcrumbs,#footer-bottom .social_icons ul li a:hover,.tagcloud a:hover,input[type="checkbox"],.login-password a:hover,.tab a.current,.question-type-main,.question-report:hover,.load-questions,.del-poll-li:hover,.styled-select::before,.fileinputs span,.post .post-type,.divider span,.widget_menu li.current_page_item a,.accordion .accordion-title.active a,.tab-inner-warp,.navigation_mobile_click:before,.user-profile-img a:hover:before,.post-pagination > span,#footer.footer_dark .tagcloud a:hover {
		 background-color: <?php echo $color_1;?>;
	}
	p a,li a, a:hover,.button.normal:hover,span.color,#footer a:hover,.widget a:hover,.question h3 a:hover,.boxedtitle h1 a:hover,.boxedtitle h2 a:hover,.boxedtitle h3 a:hover,.boxedtitle h4 a:hover,.boxedtitle h5 a:hover,.boxedtitle h6 a:hover,.box_icon:hover span i,.color_default,.navigation_mobile > div > ul a:hover,.navigation_mobile > div > ul li ul li:hover:before,.post .post-meta .meta-author a:hover,.post .post-meta .meta-categories a:hover,.post .post-meta .meta-comment a:hover,.question h2 a:hover,.question-category a:hover,.question-reply:hover i,.question-category a:hover i,.question-comment a:hover,.question-comment a:hover i,.question-reply:hover,.post .post-meta .meta-author:hover a,.post .post-meta .meta-author:hover i,.post .post-meta .meta-categories:hover i,.post .post-meta .meta-comment:hover a,.post .post-meta .meta-comment:hover i,.post-title a:hover,.question-tags a,.question .question-type,.comment-author a:hover,.comment-reply:hover,.user-profile-widget li a:hover,.taglist .tag a.delete:before,.form-style p span.color,.post-tags,.post-tags a,.related-posts li a:hover,#footer.footer_light_top .related-posts li a:hover,.share-inside,.share-inside-warp ul li a:hover,.user-points .question-vote-result,.navigation > .header-menu > ul > li > a > .menu-nav-arrow,#footer-bottom a,.widget h3.widget_title,#footer .related-item span,.widget_twitter ul li:before,#footer .widget_twitter .tweet_time a,.widget_highest_points li h6 a,#footer .widget_contact ul li span,.rememberme label,.login-text i,.subscribe-text i,.widget_search .search-submit,.login-password i,.question-tags,.question-tags i,.panel-pop h2,input[type="text"],input[type="password"],input[type="email"],textarea,select,.panel-pop p,.main-content .page-content .boxedtitle.page-title h2,.fakefile button,.login p,.login h2,.contact-us h2,.share-inside i,#related-posts h2,.comment-reply,.post-title,.post-title a,.user-profile h2,.user-profile h2 a,.stats-head,.block-stats-1,.block-stats-2,.block-stats-3,.block-stats-4,.user-question h3 a,.icon_shortcode .ul_icons li,.testimonial-client span,.box_icon h1,.box_icon h2,.box_icon h3,.box_icon h4,.box_icon h5,.box_icon h6,.widget_contact ul li i,#footer.footer_light_top .widget a:hover,#header .logo h2 a:hover,.widget_tabs.tabs-warp .tabs li a,#footer .widget .widget_highest_points a,#footer .related-item h3 a:hover,#footer.footer_dark .widget .widget_comments a:hover,#footer .widget_tabs.tabs-warp .tabs li a,.dark_skin .sidebar .widget a:hover,.user-points h3 {
		 color: <?php echo $color_1;?>;
	}
	.loader_html,input[type="text"]:focus,input[type="password"]:focus,input[type="email"]:focus,textarea:focus,.box_icon .form-style textarea:focus,.social_icon a,#footer-bottom .social_icons ul li a:hover,.widget_login input[type="text"],.widget_search input[type="text"],.subscribe_widget input[type="text"],.widget_login input[type="password"],.panel_light.login-panel input[type="text"],.panel_light.login-panel input[type="password"],#footer.footer_dark .tagcloud a:hover,#footer.footer_dark .widget_search input[type="text"]:focus,#footer.footer_dark .subscribe_widget input[type="text"]:focus,#footer.footer_dark .widget_login input[type="text"]:focus,#footer.footer_dark .widget_login input[type="password"]:focus,.dark_skin .sidebar .widget_search input[type="text"]:focus,.dark_skin .sidebar .subscribe_widget input[type="text"]:focus,.dark_skin .sidebar .widget_login input[type="text"]:focus,.dark_skin .sidebar .widget_login input[type="password"]:focus {
		border-color: <?php echo $color_1;?>;
	}
	.tabs {
		border-bottom-color: <?php echo $color_1;?>;
	}
	.tab a.current {
		border-top-color: <?php echo $color_1;?>;
	}
	.tabs-vertical .tab a.current,blockquote {
		border-right-color: <?php echo $color_1;?>;
	}
	blockquote {
		border-left-color: <?php echo $color_1;?>;
	}
	.box_warp_hover:hover,.box_warp_colored {
		border-color: <?php echo $color_2;?>;
	}
	#header .logo span {
		color: <?php echo $color_2;?>;
	}
	<?php
	$color_1_rgb = hex2rgb($color_1);
	if (isset($color_1_rgb) && is_array($color_1_rgb)) {
	?>
	.ask-me .col-md-9 p textarea,.widget_login input[type="text"],.widget_search input[type="text"],.subscribe_widget input[type="text"],.widget_login input[type="password"],.panel_light.login-panel input[type="text"],.panel_light.login-panel input[type="password"],blockquote,.qoute {
		background: rgba(<?php echo implode(",",$color_1_rgb);?>,0.20);
	}
	<?php }
}

/* Content Width */
if (!isset( $content_width ))
	$content_width = 785;

/* vpanel_feed_request */
function vpanel_feed_request ($qv) {
	if (isset($qv['feed']) && !isset($qv['post_type']))
		$qv['post_type'] = array('post', 'question');
	return $qv;
}
add_filter('request', 'vpanel_feed_request');
/* Save default options */
$default_options = array(
	"v_responsive" => 1,
	"seo_active" => 1,
	"login_panel" => 1,
	"top_panel_skin" => "panel_dark",
	"top_menu" => 1,
	"logo_position" => "left_logo",
	"header_skin" => "header_dark",
	"logo_display" => "custom_image",
	"logo_img" => get_template_directory_uri( __FILE__ )."/images/logo.png",
	"breadcrumbs" => 1,
	"index_top_box" => 1,
	"index_top_box_layout" => 1,
	"index_title" => "Welcome to Ask me",
	"index_content" => "Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque.",
	"index_about" => "About Us",
	"index_about_h" => "#",
	"index_join" => "Join Now",
	"index_join_h" => "#",
	"ask_question_no_register" => 0,
	"attachment_question" => 1,
	"attachment_answer" => 1,
	"recently_answered_num" => 30,
	"questions_publish" => "publish",
	"question_points" => 3,
	"question_points_active" => 0,
	"video_desc_active" => 1,
	"video_desc" => "after",
	"send_email_new_question" => 0,
	"confirm_email" => 0,
	"the_captcha" => 0,
	"show_captcha_answer" => 1,
	"captcha_style" => "question_answer",
	"captcha_question" => "What is the capital of Egypt ?",
	"captcha_answer" => "Cairo",
	"profile_picture" => 0,
	"profile_picture_required" => 0,
	"register_content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravdio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequa. Vivamus vulputate posuere nisl quis consequat.",
	"show_point_favorite" => 1,
	"point_best_answer" => 5,
	"point_add_comment" => 2,
	"point_rating_answer" => 1,
	"point_following_me" => 1,
	"author_sidebar_layout" => "default",
	"author_sidebar" => "default",
	"author_layout" => "default",
	"author_template" => "default",
	"author_skin_l" => "default",
	"author_skin" => "default_color",
	"home_display" => "blog_1",
	"post_meta" => 1,
	"post_share" => 1,
	"post_author_box" => 1,
	"related_post" => 1,
	"related_number" => 5,
	"post_comments" => 1,
	"post_comments_user" => 0,
	"post_navigation" => 1,
	"sidebar_layout" => "default",
	"sidebar_home" => "default",
	"else_sidebar" => "default",
	"home_layout" => "full",
	"home_template" => "grid_1200",
	"site_skin_l" => "site_light",
	"site_skin" => "default",
	"footer_skin" => "footer_dark",
	"footer_layout" => "footer_no",
	"footer_copyrights" => "Copyright 2014 Ask me | <a href=\"#\">By 2code</a>",
	"social_icon_f" => 1,
	"twitter_icon_f" => "#",
	"facebook_icon_f" => "#",
	"gplus_icon_f" => "#",
	"youtube_icon_f" => "#",
	"skype_icon_f" => "#",
	"flickr_icon_f" => "#",
	"rss_icon_f" => 1,
);

if (!get_option("vpanel_".vpanel_name)) {
	add_option("vpanel_".vpanel_name,$default_options);
}