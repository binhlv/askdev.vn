<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'my_askdev_vn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P]`C %#kwkX2;)8i-L+|pnWt]^rNOrbBAQYj;TtjId)4lhCb0:*9*f.&l5r_os?{');
define('SECURE_AUTH_KEY',  'bQ;]`QL.lp]-6l`Pb2n^Z~<hE|cL@.j[|Ro)FnBi(KdyogC*TV^da!n3,+`F./+r');
define('LOGGED_IN_KEY',    'NpJGrrrI|a7o,7#`G+bQ** :%7d?ur 6OXp+K-%=c*=H2=iqZ=x&m$i3:HYU4^HT');
define('NONCE_KEY',        '!|KC1KsWIt[Xshdr^]H<FX(Jk,-`@jY[kMG}LmPIY2w`([C4+w-4vkem0atJ]YO{');
define('AUTH_SALT',        'tS*%y#fdOn]REo4HcjxwENH4ekr[OqU({KT[.Cr1Y}s}!J4/Lz,QRd/*o6IDqt13');
define('SECURE_AUTH_SALT', '3|Z;O<lGEUBQFj/&EX/}@+X0$bFtB9,G+>XPvwzRPp5!C=b:hYt-eBcE*{.0I{D,');
define('LOGGED_IN_SALT',   '5-u-rb+!3XLbwG~]YT+.sK3k]Z^ ohKpW.0hqZw=~7;R,|)Zp_u;zf{7CSI-o83+');
define('NONCE_SALT',       '/9i,pg<}@k[HAk^A-}=<dk,2e[-uKGcP]9~q^K?Irdm{e//Nd^?;}e&7)1=We|Hl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
