<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 28/11/2015
 * Time: 1:50 PM
 */
include('simplehtmldom_1_5/simple_html_dom.php');
include('../wp-load.php');

//include('../wp-content/themes/qaengine-v1.51/includes/questions.php');

class GetData
{
    public $name;
    public $args;
    public $taxonomy_args;
    public $meta_data;
    public function __construct()
    {
        $this->meta_data = apply_filters('question_meta_fields', array(
            'et_vote_count',
            'et_view_count',
            'et_answers_count',
            'et_users_follow',
            'et_answer_authors',
            'et_last_author',
            'et_vote_up_authors',
            'et_vote_down_authors',
            'et_best_answer'
        ));
        $this->taxonomy_args =  array(
            'question_category' => array(
                'hierarchical'      => true,
                'labels'            => array(
                    'name'              => __( 'Question Categories', ET_DOMAIN ),
                    'singular_name'     => __( 'Category', ET_DOMAIN ),
                    'search_items'      => __( 'Search Categories', ET_DOMAIN ),
                    'all_items'         => __( 'All Categories', ET_DOMAIN ),
                    'parent_item'       => __( 'Parent Category', ET_DOMAIN ),
                    'parent_item_colon' => __( 'Parent Category:', ET_DOMAIN ),
                    'edit_item'         => __( 'Edit Category' , ET_DOMAIN),
                    'update_item'       => __( 'Update Category', ET_DOMAIN ),
                    'add_new_item'      => __( 'Add New Category' , ET_DOMAIN),
                    'new_item_name'     => __( 'New Category Name', ET_DOMAIN ),
                    'menu_name'         => __( 'Category' , ET_DOMAIN),
                ),
                'show_ui'           => true,
                'show_admin_column' => true,
                'query_var'         => true,
                'rewrite' 			=> array( 'slug' => ae_get_option('category_slug', 'question-category')),
            ),
            'qa_tag'  => array(
                'hierarchical'          => false,
                'labels'                => array(
                    'name'                       => _x( 'Tags', 'taxonomy general name' ),
                    'singular_name'              => _x( 'Tag', 'taxonomy singular name' ),
                    'search_items'               => __( 'Search Tags',ET_DOMAIN ),
                    'popular_items'              => __( 'Popular Tags',ET_DOMAIN ),
                    'all_items'                  => __( 'All Tags',ET_DOMAIN ),
                    'parent_item'                => null,
                    'parent_item_colon'          => null,
                    'edit_item'                  => __( 'Edit Tag',ET_DOMAIN ),
                    'update_item'                => __( 'Update Tag',ET_DOMAIN ),
                    'add_new_item'               => __( 'Add New Tag',ET_DOMAIN ),
                    'new_item_name'              => __( 'New Tag Name',ET_DOMAIN ),
                    'separate_items_with_commas' => __( 'Separate tags with commas',ET_DOMAIN ),
                    'add_or_remove_items'        => __( 'Add or remove tags',ET_DOMAIN ),
                    'choose_from_most_used'      => __( 'Choose from the most used tags',ET_DOMAIN ),
                    'not_found'                  => __( 'No tags found.',ET_DOMAIN ),
                    'menu_name'                  => __( 'Tags',ET_DOMAIN ),
                ),
                'show_ui'               => true,
                'show_admin_column'     => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var'             => true,
                'rewrite'               => array( 'slug' => ae_get_option('tag_slug', 'qa-tag') ),
            )
        );
    }
    function  run()
    {
        global $wpdb, $argv;
        // get DOM from URL or file
        $url = 'http://stackoverflow.com';

        if(!isset($argv[1])){
            $this->usageHelp();
            die();
        }
        $start = $argv[1];
        $number = 5;
        for ($end = ($number * $start); $end > (($start - 1) * $number); $end--) {
            $html = file_get_html('http://stackoverflow.com/questions?page=' . $end . '&sort=frequent');
            // find all link question
            foreach ($html->find('a.question-hyperlink') as $e) {

                $args = array(
                    'post_type' => 'question',
                    'meta_key' => 'link_internet',
                    'meta_value' => $url . $e->href
                );
                $getPosts = get_posts($args);

                if (!count($getPosts)) {
                    //get content a question
                    $htmlQuestion = file_get_html($url . $e->href);
                    $title = $e->innertext;
                    if ($htmlQuestion) {
                        $question = $htmlQuestion->find('div.question table tr td div.post-text', 0)->innertext;
                        $tags = array();
                        foreach ($htmlQuestion->find('div.question table tr td div.post-taglist a.post-tag') as $tag) {
                            $tags[] = preg_replace("/<img[^>]+\>/i", "", $tag->innertext);
                        }

                        $answerContent = '';
                        $i = 0;
                        foreach ($htmlQuestion->find('div#answers div.answer') as $answer) {
                            $content = str_get_html($answer->innertext);
                            $asC = $content->find('table tr td div.post-text', 0)->innertext;
                            if ($i == 0 || $content->find('span.vote-accepted-on')) {
                                $answerContent = $asC;
                            }
                            $i++;
                        }
                        $aCats = array(
                            '18' => array('c++', 'c', 'php', 'java', 'Android', 'iOS', 'Perl', 'Objective-C', 'JavaScript', 'Visual Basic', 'c#', 'Python', 'Ruby', 'CSS', 'R', 'pascal', 'nodejs', 'Asp.Net', 'HTML5', 'MVC', 'iis', 'linq', 'xmarin', 'string', 'int', 'varchar', 'multithreading', 'excel', 'image', 'facebook'),//ngon ngu lap trình
                            '35' => array('joomla', 'drupal', 'wordrpess', 'magento', 'ExpressionEngine', 'Radiant', 'Cushy', 'cs.cart', 'opencart', 'Xenforo', 'Nodebb', 'phpbb', 'vBulletin'),//cms
                            '44' => array('PHPStorm', 'PhpDesigner', 'Eclipse', 'VisualStudio', 'Notepad++', 'Sublime', 'github', 'bitbucket', 'codeproject', 'redmine', 'zoho'),//Tool
                            '42' => array('Mysql', 'Mongodb', 'SqlServer', 'Agile', 'PostgreSQL', 'Oracle', 'Firebird'),//database
                            '11' => array('zend 1', 'zend 2', 'cakePHP', 'Yii', 'Phalcon', 'Laravel', 'Symfony 1', 'Symfony2', 'Code Igniter', 'Aura', 'Flight', 'Kohana', 'PHP-MVC', 'fuelphp', 'Slim', 'AngularJs', 'devpress', 'Silverlight', 'Backbone', 'ExtJS', 'Agility', 'Choco', 'Sammy', 'eyeballs', 'ActiveJS', 'Spine', 'qooxdoo', 'Luna', 'JavaScriptMVC', 'Cappuccino', 'Sproutcore', 'Bootstrap'),//framework
                            '27' => array('Linux', 'Centos', 'Window', 'Memcache', 'APC', 'ElasticSearch', 'redis', 'puppet', 'ansible', 'chef', 'git', 'AWS', 'database', 'SVN', 'elk', 'cache', 'nginx', 'apache', 'xampp', 'wamp', 'rabbitmq', 'phpmyadmin', 'firewall', 'bash', 'web-service'),//System
                            '48' => array('Photoshop', 'Dreamweaver', '3D Max', 'Vray', 'Autocad', 'Premiere Pro', 'After Effect', 'Illustrator', 'CorelDRAW,InDesign', 'Flash')
                        );
                        $cat = 18;
                        foreach ($tags as $tag) {
                            foreach ($aCats as $key => $val) {
                                $a = array_map('strtolower', $val);
                                if (in_array(strtolower($tag), $a)) {
                                    $cat = $key;
                                    break;
                                }
                            }
                        }
                        $cats = array(
                            'qa_tag' => $tags,
                            'question_category' => $cat
                        );
                        $uQ = rand(6, 10);
                        $result = $this->insert_question($title, $question, $cats, "publish", $uQ);

                        if ($result) {
                            $uA = rand(2, 5);
                            update_post_meta($result, 'link_internet', $url . $e->href);
                            $this->insert_answer($result, $answerContent, $uA, $result);
                        };
                    }
                }

            }
        }
    }

function insert_question($title, $content, $cats, $status = "publish", $author = 0)
{


    if (empty($cats)) {
        echo('Category must not empty');
        return null;
    }
    $data = array(
        'post_title' => $title,
        'post_content' => $content,
        'post_type' => 'question',
        'post_author' => $author,
        'post_status' => $status,
        'tax_input' => $cats,
        'et_updated_date' => current_time('mysql'),
    );

    $question_id = $this->_insert($data);

    //update question count
    $count = $this->__et_count_user_posts($author, 'question');
    update_user_meta($author, 'et_question_count', $count);

    //update following questions
    $follow_questions = (array)get_user_meta($author, 'qa_following_questions', true);
    if (!in_array($question_id, $follow_questions))
        array_push($follow_questions, $question_id);
    $follow_questions = array_unique(array_filter($follow_questions));
    update_user_meta($author, 'qa_following_questions', $follow_questions);

    return $question_id;
}

function _insert($args)
{
    global $current_user;


    if (isset($args['author']) && !empty($args['author'])) $args['post_author'] = $args['author'];

    if (empty($args['post_author'])) return new WP_Error('missing_author', __('Missing Author'));


    $data = $this->trip_meta($args);

    $result = wp_insert_post($data['args'], true);
    /**
     * update custom field and tax
     */
    $this->update_custom_field($result, $data, $args);

    return $result;
}

function trip_meta($data)
{
    // trip meta datas
    $args = $data;
    $meta = array();
    foreach ($args as $key => $value) {
        if (in_array($key, $this->meta_data)) {
            $meta[$key] = $value;
            unset($args[$key]);
        }
    }

    return array(
        'args' => $args,
        'meta' => $meta
    );
}

function update_custom_field($result, $data, $args)
{
    if ( $result){
        foreach ($this->taxonomy_args as $tax_name => $tax_args) {
            if ( isset($args['tax_input'][$tax_name]) ){
                if($tax_name == "question_category" && !term_exists( $args['tax_input'][$tax_name], 'question_category' )){
                    die(__('Category is not existed!'));
                }
                $terms = wp_set_object_terms( $result, $args['tax_input'][$tax_name], $tax_name );
            }
        }
        foreach ($data['meta'] as $key => $value) {
            update_post_meta($result, $key, $value);
        }
    }
}

function insert_answer($question_id, $content, $author = false, $answer_id = 0)
{

    $question = get_post($question_id);

    $content = preg_replace('/\[quote\].*(<br\s*\/?>\s*).*\[\/quote\]/', '', $content);
    $data = array(
        'post_title' => 'RE: ' . $question->post_title,
        'post_content' => $content,
        'post_parent' => $question_id,
        'author' => $author,
        'post_type' => 'answer',
        'post_status' => ae_get_option('pending_answers') && !(current_user_can('manage_options') || qa_user_can('approve_answer')) ? 'pending' : 'publish',
        'et_answer_parent' => $answer_id
    );

    $result = $this->_insert($data);

    // if item is inserted successfully, update statistic
    if ($result) {
        //update user answers count
        $count = $this->__et_count_user_posts($author, 'answer');
        update_user_meta($author, 'et_answer_count', $count);

        //update user following questions
        $follow_questions = (array)get_user_meta($author, 'qa_following_questions', true);
        if (!in_array($question_id, $follow_questions))
            array_push($follow_questions, $question_id);
        $follow_questions = array_unique(array_filter($follow_questions));
        update_user_meta($author, 'qa_following_questions', $follow_questions);

        // update question's update date
        update_post_meta($question_id, 'et_updated_date', current_time('mysql'));

        // update last update author
        update_post_meta($question_id, 'et_last_author', $author);

        // update answer_authors
        $answer_authors = get_post_meta($question_id, 'et_answer_authors', true);
        $answer_authors = is_array($answer_authors) ? $answer_authors : array();
        if (!in_array($author, $answer_authors)) {
            $answer_authors[] = $author;
            update_post_meta($question_id, 'et_answer_authors', $answer_authors);
        }
        // update answer author for answer
        if ($answer_id) {
            $answer_authors = get_post_meta($answer_id, 'et_answer_authors', true);
            $answer_authors = is_array($answer_authors) ? $answer_authors : array();
            if (!in_array($author, $answer_authors)) {
                $answer_authors[] = $author;
                update_post_meta($answer_id, 'et_answer_authors', $answer_authors);
            }
        }
        $this->count_comments($answer_id);

    }
    return $result;
}

function count_comments($parent)
{
    global $wpdb;

    $sql = "SELECT count(*) FROM {$wpdb->posts}
					INNER JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id AND {$wpdb->postmeta}.meta_key = 'et_answer_parent'
					WHERE {$wpdb->postmeta}.meta_value = $parent AND {$wpdb->posts}.post_type = 'answer' AND {$wpdb->posts}.post_status = 'publish' ";

    $count = $wpdb->get_var($sql);

    // save
    update_post_meta($parent, 'et_answers_count', (int)$count);

    return $count;
}

function __et_count_user_posts($user_id, $post_type = "question")
{
    global $wpdb;
    $sql = "SELECT COUNT(post.ID)
				FROM {$wpdb->posts} as post
				WHERE post.post_type = '" . $post_type . "'
					AND ( post.post_status = 'publish' OR post.post_status = 'pending' )
					AND post.post_author = " . $user_id;
    return $wpdb->get_var($sql);
}
    public function usageHelp()
    {
        echo <<<USAGE
            Usage:  php -f index.php [page_number]

              page             Curl page
              help              This help

USAGE;
    }
}
$shell = new GetData();
$shell->run();