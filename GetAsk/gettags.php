<?php
// example of how to use basic selector to retrieve HTML contents
include('simplehtmldom_1_5/simple_html_dom.php');
 
$number = 100;
// find all link
$doc = new DOMDocument( );
for($number; $number>=1; $number--){
	$html = file_get_html('http://stackoverflow.com/tags?page='.$number.'&tab=popular');
	foreach($html->find('a.post-tag') as $e) {
		$content = preg_replace("/<img[^>]+\>/i", "", $e->innertext);     
		$ele = $doc->createElement( 'category' );
		$ele->setAttribute( "domain", "qa_tag" );
		$ele->setAttribute( "nicename", $content );
		$ele->nodeValue = '<![CDATA['.$content.']]>';
		$doc->appendChild( $ele );
	}
}
$doc->save('MyXmlFile.xml');
?>